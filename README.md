# Solar Finance Calculator

This program was written to generate the pricing of a solar panel system to multiple state markets. This tool was built while working for a solar company to be able to assist sales reps and the loan pricing process.

Occasionally, sales reps would be able to get a lower price on the per watt cost (PWC) of a solar panel system. This tool would then show the price out to several place at $0.05 drops from the maximum PWC of the reigion till it reaches the minimum.

The tool at this point is probably no longer useful for determining solar pricing. However, it is still useful to me as a reference for future projects utilizing C# forms.

There were no special usings for this program.

# In the end this tool accomplishes these tasks:

- Price out a solar panel system by its PWC from a maximum to a minimum at $0.05 intervals.
- Keep accuracy for proposal specialists.
- Makes a common calculation no longer needed to be repeated for each time a price change is requested.

# Reflection
In hindsight, this entire program could have been done in Excel. However, Excel is not always a favorable program to keep open all day. Not every machine in a company has it. The best solution for the entire team was to make an .exe that could be shared quickly to everyone who needs it and would be able to run quickly with a low learning curve.