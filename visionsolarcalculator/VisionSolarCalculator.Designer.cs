﻿namespace visionsolarcalculator
{
    partial class VisionSolarCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisionSolarCalculator));
            this.tabAnnouncements = new System.Windows.Forms.TabControl();
            this.tabUtah = new System.Windows.Forms.TabPage();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.utahChkBtnCash = new System.Windows.Forms.CheckBox();
            this.btnClearUT = new System.Windows.Forms.Button();
            this.textBoxEmailTemplateUT = new System.Windows.Forms.TextBox();
            this.textBoxKwTotalUT = new System.Windows.Forms.TextBox();
            this.textBoxArray4KwUT = new System.Windows.Forms.TextBox();
            this.textBoxArray1KwUT = new System.Windows.Forms.TextBox();
            this.textBoxArray2KwUT = new System.Windows.Forms.TextBox();
            this.textBoxArray3KwUT = new System.Windows.Forms.TextBox();
            this.lblNinthWatt = new System.Windows.Forms.Label();
            this.lblSixthWatt = new System.Windows.Forms.Label();
            this.lblEighthWatt = new System.Windows.Forms.Label();
            this.lblFifthWatt = new System.Windows.Forms.Label();
            this.lblSeventhWatt = new System.Windows.Forms.Label();
            this.lblFourthWatt = new System.Windows.Forms.Label();
            this.lblThirdWatt = new System.Windows.Forms.Label();
            this.lblSecondWatt = new System.Windows.Forms.Label();
            this.lblFirstWatt = new System.Windows.Forms.Label();
            this.textBox340UT = new System.Windows.Forms.TextBox();
            this.textBox355UT = new System.Windows.Forms.TextBox();
            this.textBox350UT = new System.Windows.Forms.TextBox();
            this.textBox380UT = new System.Windows.Forms.TextBox();
            this.textBox345UT = new System.Windows.Forms.TextBox();
            this.textBox365UT = new System.Windows.Forms.TextBox();
            this.textBox360UT = new System.Windows.Forms.TextBox();
            this.textBox375UT = new System.Windows.Forms.TextBox();
            this.textBox370UT = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxPerWattCostUT = new System.Windows.Forms.TextBox();
            this.btnCalculateUtah = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxTotalUtah = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxArray1Utah = new System.Windows.Forms.TextBox();
            this.textBoxArray2Utah = new System.Windows.Forms.TextBox();
            this.textBoxArray3Utah = new System.Windows.Forms.TextBox();
            this.textBoxArray4Utah = new System.Windows.Forms.TextBox();
            this.tabSC = new System.Windows.Forms.TabPage();
            this.webBrowser2 = new System.Windows.Forms.WebBrowser();
            this.southCarolinaChkCashBtn = new System.Windows.Forms.CheckBox();
            this.btnClearSC = new System.Windows.Forms.Button();
            this.textBoxEmailTemplateSC = new System.Windows.Forms.TextBox();
            this.textBoxTotalKWSC = new System.Windows.Forms.TextBox();
            this.textBoxArrayKWSC4 = new System.Windows.Forms.TextBox();
            this.textBoxArrayKWSC1 = new System.Windows.Forms.TextBox();
            this.textBoxArrayKWSC2 = new System.Windows.Forms.TextBox();
            this.textBoxArrayKWSC3 = new System.Windows.Forms.TextBox();
            this.lblPWCSC9 = new System.Windows.Forms.Label();
            this.lblPWCSC8 = new System.Windows.Forms.Label();
            this.lblPWCSC7 = new System.Windows.Forms.Label();
            this.lblPWCSC6 = new System.Windows.Forms.Label();
            this.lblPWCSC5 = new System.Windows.Forms.Label();
            this.lblPWCSC4 = new System.Windows.Forms.Label();
            this.lblPWCSC3 = new System.Windows.Forms.Label();
            this.lblPWCSC2 = new System.Windows.Forms.Label();
            this.lblPWCSC1 = new System.Windows.Forms.Label();
            this.textBoxPWCDOWNSC9 = new System.Windows.Forms.TextBox();
            this.textBoxPWCDOWNSC8 = new System.Windows.Forms.TextBox();
            this.textBoxPWCDOWNSC7 = new System.Windows.Forms.TextBox();
            this.textBoxPWCDOWNSC6 = new System.Windows.Forms.TextBox();
            this.textBoxPWCDOWNSC1 = new System.Windows.Forms.TextBox();
            this.textBoxPWCDOWNSC4 = new System.Windows.Forms.TextBox();
            this.textBoxPWCDOWNSC5 = new System.Windows.Forms.TextBox();
            this.textBoxPWCDOWNSC2 = new System.Windows.Forms.TextBox();
            this.textBoxPWCDOWNSC3 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxPWCSC = new System.Windows.Forms.TextBox();
            this.btnCalculateSC = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxTotalPriceSC = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxArraySC1 = new System.Windows.Forms.TextBox();
            this.textBoxArraySC2 = new System.Windows.Forms.TextBox();
            this.textBoxArraySC3 = new System.Windows.Forms.TextBox();
            this.textBoxArraySC4 = new System.Windows.Forms.TextBox();
            this.tabCalifornia = new System.Windows.Forms.TabPage();
            this.webBrowser3 = new System.Windows.Forms.WebBrowser();
            this.californiaChkCashBtn = new System.Windows.Forms.CheckBox();
            this.btnCAClear = new System.Windows.Forms.Button();
            this.txtCAEmailTemplate = new System.Windows.Forms.TextBox();
            this.txtCAKWTotal = new System.Windows.Forms.TextBox();
            this.txtCAKWArray4 = new System.Windows.Forms.TextBox();
            this.txtCAKWArray1 = new System.Windows.Forms.TextBox();
            this.txtCAKWArray2 = new System.Windows.Forms.TextBox();
            this.txtCAKWArray3 = new System.Windows.Forms.TextBox();
            this.lblCAPWCDown9 = new System.Windows.Forms.Label();
            this.lblCAPWCDown8 = new System.Windows.Forms.Label();
            this.lblCAPWCDown7 = new System.Windows.Forms.Label();
            this.lblCAPWCDown6 = new System.Windows.Forms.Label();
            this.lblCAPWCDown5 = new System.Windows.Forms.Label();
            this.lblCAPWCDown4 = new System.Windows.Forms.Label();
            this.lblCAPWCDown3 = new System.Windows.Forms.Label();
            this.lblCAPWCDown2 = new System.Windows.Forms.Label();
            this.lblCAPWCDown1 = new System.Windows.Forms.Label();
            this.txtCATotalSystemCost10 = new System.Windows.Forms.TextBox();
            this.txtCATotalSystemCost9 = new System.Windows.Forms.TextBox();
            this.txtCATotalSystemCost8 = new System.Windows.Forms.TextBox();
            this.txtCATotalSystemCost7 = new System.Windows.Forms.TextBox();
            this.txtCATotalSystemCost2 = new System.Windows.Forms.TextBox();
            this.txtCATotalSystemCost5 = new System.Windows.Forms.TextBox();
            this.txtCATotalSystemCost6 = new System.Windows.Forms.TextBox();
            this.txtCATotalSystemCost3 = new System.Windows.Forms.TextBox();
            this.txtCATotalSystemCost4 = new System.Windows.Forms.TextBox();
            this.lblCAPWC = new System.Windows.Forms.Label();
            this.txtCAPWC = new System.Windows.Forms.TextBox();
            this.btnCACalculate = new System.Windows.Forms.Button();
            this.lblCATotalSystemCost = new System.Windows.Forms.Label();
            this.txtCATotalSystemCost1 = new System.Windows.Forms.TextBox();
            this.lblCAArray4 = new System.Windows.Forms.Label();
            this.lblCAArray3 = new System.Windows.Forms.Label();
            this.lblCAArray2 = new System.Windows.Forms.Label();
            this.lblCAArray1 = new System.Windows.Forms.Label();
            this.txtCAArray1 = new System.Windows.Forms.TextBox();
            this.txtCAArray2 = new System.Windows.Forms.TextBox();
            this.txtCAArray3 = new System.Windows.Forms.TextBox();
            this.txtCAArray4 = new System.Windows.Forms.TextBox();
            this.tabTexasAustin = new System.Windows.Forms.TabPage();
            this.webBrowser4 = new System.Windows.Forms.WebBrowser();
            this.austinChkCashBtn = new System.Windows.Forms.CheckBox();
            this.btnAustinClear = new System.Windows.Forms.Button();
            this.austinTxtEmailTemplate = new System.Windows.Forms.TextBox();
            this.austinTxtKwTotal = new System.Windows.Forms.TextBox();
            this.austinTxtKwArray4 = new System.Windows.Forms.TextBox();
            this.austinTxtKwArray1 = new System.Windows.Forms.TextBox();
            this.austinTxtKwArray2 = new System.Windows.Forms.TextBox();
            this.austinTxtKwArray3 = new System.Windows.Forms.TextBox();
            this.lblAustinPWCDown9 = new System.Windows.Forms.Label();
            this.lblAustinPWCDown8 = new System.Windows.Forms.Label();
            this.lblAustinPWCDown7 = new System.Windows.Forms.Label();
            this.lblAustinPWCDown6 = new System.Windows.Forms.Label();
            this.lblAustinPWCDown5 = new System.Windows.Forms.Label();
            this.lblAustinPWCDown4 = new System.Windows.Forms.Label();
            this.lblAustinPWCDown3 = new System.Windows.Forms.Label();
            this.lblAustinPWCDown2 = new System.Windows.Forms.Label();
            this.lblAustinPWCDown1 = new System.Windows.Forms.Label();
            this.austinTxtTotalSystemCost10 = new System.Windows.Forms.TextBox();
            this.austinTxtTotalSystemCost9 = new System.Windows.Forms.TextBox();
            this.austinTxtTotalSystemCost8 = new System.Windows.Forms.TextBox();
            this.austinTxtTotalSystemCost7 = new System.Windows.Forms.TextBox();
            this.austinTxtTotalSystemCost2 = new System.Windows.Forms.TextBox();
            this.austinTxtTotalSystemCost5 = new System.Windows.Forms.TextBox();
            this.austinTxtTotalSystemCost6 = new System.Windows.Forms.TextBox();
            this.austinTxtTotalSystemCost3 = new System.Windows.Forms.TextBox();
            this.austinTxtTotalSystemCost4 = new System.Windows.Forms.TextBox();
            this.lblAustinPWC = new System.Windows.Forms.Label();
            this.austinTxtPWC1 = new System.Windows.Forms.TextBox();
            this.btnAustinCalc = new System.Windows.Forms.Button();
            this.lblAustinTotalSystemCost = new System.Windows.Forms.Label();
            this.austinTxtTotalSystemCost1 = new System.Windows.Forms.TextBox();
            this.lblAustinArray4 = new System.Windows.Forms.Label();
            this.lblAustinArray3 = new System.Windows.Forms.Label();
            this.lblAustinArray2 = new System.Windows.Forms.Label();
            this.lblAustinArray1 = new System.Windows.Forms.Label();
            this.austinTxtArray1 = new System.Windows.Forms.TextBox();
            this.austinTxtArray2 = new System.Windows.Forms.TextBox();
            this.austinTxtArray3 = new System.Windows.Forms.TextBox();
            this.austinTxtArray4 = new System.Windows.Forms.TextBox();
            this.tabTexasSanAntonio = new System.Windows.Forms.TabPage();
            this.webBrowser5 = new System.Windows.Forms.WebBrowser();
            this.sanAntonioChkCashBtn = new System.Windows.Forms.CheckBox();
            this.btnSanAntonioClear = new System.Windows.Forms.Button();
            this.sanAntonioTxtEmailInformation = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtKWTotal = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtKWArray4 = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtKWArray1 = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtKWArray2 = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtKWArray3 = new System.Windows.Forms.TextBox();
            this.sanAntonioLblPwcDown9 = new System.Windows.Forms.Label();
            this.sanAntonioLblPwcDown8 = new System.Windows.Forms.Label();
            this.sanAntonioLblPwcDown7 = new System.Windows.Forms.Label();
            this.sanAntonioLblPwcDown6 = new System.Windows.Forms.Label();
            this.sanAntonioLblPwcDown5 = new System.Windows.Forms.Label();
            this.sanAntonioLblPwcDown4 = new System.Windows.Forms.Label();
            this.sanAntonioLblPwcDown3 = new System.Windows.Forms.Label();
            this.sanAntonioLblPwcDown2 = new System.Windows.Forms.Label();
            this.sanAntonioLblPwcDown1 = new System.Windows.Forms.Label();
            this.sanAntonioTxtTotalSystemCost10 = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtTotalSystemCost9 = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtTotalSystemCost8 = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtTotalSystemCost7 = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtTotalSystemCost2 = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtTotalSystemCost5 = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtTotalSystemCost6 = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtTotalSystemCost3 = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtTotalSystemCost4 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.sanAntonioTxtPWC = new System.Windows.Forms.TextBox();
            this.btnSanAntonioCalc = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.sanAntonioTxtTotalSystemCost1 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.sanAntonioTxtArray1 = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtArray2 = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtArray3 = new System.Windows.Forms.TextBox();
            this.sanAntonioTxtArray4 = new System.Windows.Forms.TextBox();
            this.tabVegas = new System.Windows.Forms.TabPage();
            this.webBrowser6 = new System.Windows.Forms.WebBrowser();
            this.nvChkCash = new System.Windows.Forms.CheckBox();
            this.btnClearNV = new System.Windows.Forms.Button();
            this.nvEmailTemplateTxt = new System.Windows.Forms.TextBox();
            this.nvTotalKwTxt = new System.Windows.Forms.TextBox();
            this.nvkWArray4Txt = new System.Windows.Forms.TextBox();
            this.nvkWArray1Txt = new System.Windows.Forms.TextBox();
            this.nvkWArray2Txt = new System.Windows.Forms.TextBox();
            this.nvkWArray3Txt = new System.Windows.Forms.TextBox();
            this.nvLblDown9 = new System.Windows.Forms.Label();
            this.nvLblDown8 = new System.Windows.Forms.Label();
            this.nvLblDown7 = new System.Windows.Forms.Label();
            this.nvLblDown6 = new System.Windows.Forms.Label();
            this.nvLblDown5 = new System.Windows.Forms.Label();
            this.nvLblDown4 = new System.Windows.Forms.Label();
            this.nvLblDown3 = new System.Windows.Forms.Label();
            this.nvLblDown2 = new System.Windows.Forms.Label();
            this.nvLblDown1 = new System.Windows.Forms.Label();
            this.nvTxtDown9 = new System.Windows.Forms.TextBox();
            this.nvTxtDown8 = new System.Windows.Forms.TextBox();
            this.nvTxtDown7 = new System.Windows.Forms.TextBox();
            this.nvTxtDown6 = new System.Windows.Forms.TextBox();
            this.nvTxtDown1 = new System.Windows.Forms.TextBox();
            this.nvTxtDown4 = new System.Windows.Forms.TextBox();
            this.nvTxtDown5 = new System.Windows.Forms.TextBox();
            this.nvTxtDown2 = new System.Windows.Forms.TextBox();
            this.nvTxtDown3 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.nvPwcTxt = new System.Windows.Forms.TextBox();
            this.btnCalcNV = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.nvTotalSystemCostTxt = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.nvArray1Txt = new System.Windows.Forms.TextBox();
            this.nvArray2Txt = new System.Windows.Forms.TextBox();
            this.nvArray3Txt = new System.Windows.Forms.TextBox();
            this.nvArray4Txt = new System.Windows.Forms.TextBox();
            this.tabAnnoucements = new System.Windows.Forms.TabPage();
            this.webBrowser8 = new System.Windows.Forms.WebBrowser();
            this.webBrowser7 = new System.Windows.Forms.WebBrowser();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tabAnnouncements.SuspendLayout();
            this.tabUtah.SuspendLayout();
            this.tabSC.SuspendLayout();
            this.tabCalifornia.SuspendLayout();
            this.tabTexasAustin.SuspendLayout();
            this.tabTexasSanAntonio.SuspendLayout();
            this.tabVegas.SuspendLayout();
            this.tabAnnoucements.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabAnnouncements
            // 
            this.tabAnnouncements.Controls.Add(this.tabUtah);
            this.tabAnnouncements.Controls.Add(this.tabSC);
            this.tabAnnouncements.Controls.Add(this.tabCalifornia);
            this.tabAnnouncements.Controls.Add(this.tabTexasAustin);
            this.tabAnnouncements.Controls.Add(this.tabTexasSanAntonio);
            this.tabAnnouncements.Controls.Add(this.tabVegas);
            this.tabAnnouncements.Controls.Add(this.tabAnnoucements);
            this.tabAnnouncements.Location = new System.Drawing.Point(12, 27);
            this.tabAnnouncements.Name = "tabAnnouncements";
            this.tabAnnouncements.SelectedIndex = 0;
            this.tabAnnouncements.Size = new System.Drawing.Size(1018, 498);
            this.tabAnnouncements.TabIndex = 1;
            this.tabAnnouncements.TabStop = false;
            this.tabAnnouncements.Click += new System.EventHandler(this.tabControl1_Click);
            // 
            // tabUtah
            // 
            this.tabUtah.Controls.Add(this.webBrowser1);
            this.tabUtah.Controls.Add(this.utahChkBtnCash);
            this.tabUtah.Controls.Add(this.btnClearUT);
            this.tabUtah.Controls.Add(this.textBoxEmailTemplateUT);
            this.tabUtah.Controls.Add(this.textBoxKwTotalUT);
            this.tabUtah.Controls.Add(this.textBoxArray4KwUT);
            this.tabUtah.Controls.Add(this.textBoxArray1KwUT);
            this.tabUtah.Controls.Add(this.textBoxArray2KwUT);
            this.tabUtah.Controls.Add(this.textBoxArray3KwUT);
            this.tabUtah.Controls.Add(this.lblNinthWatt);
            this.tabUtah.Controls.Add(this.lblSixthWatt);
            this.tabUtah.Controls.Add(this.lblEighthWatt);
            this.tabUtah.Controls.Add(this.lblFifthWatt);
            this.tabUtah.Controls.Add(this.lblSeventhWatt);
            this.tabUtah.Controls.Add(this.lblFourthWatt);
            this.tabUtah.Controls.Add(this.lblThirdWatt);
            this.tabUtah.Controls.Add(this.lblSecondWatt);
            this.tabUtah.Controls.Add(this.lblFirstWatt);
            this.tabUtah.Controls.Add(this.textBox340UT);
            this.tabUtah.Controls.Add(this.textBox355UT);
            this.tabUtah.Controls.Add(this.textBox350UT);
            this.tabUtah.Controls.Add(this.textBox380UT);
            this.tabUtah.Controls.Add(this.textBox345UT);
            this.tabUtah.Controls.Add(this.textBox365UT);
            this.tabUtah.Controls.Add(this.textBox360UT);
            this.tabUtah.Controls.Add(this.textBox375UT);
            this.tabUtah.Controls.Add(this.textBox370UT);
            this.tabUtah.Controls.Add(this.label6);
            this.tabUtah.Controls.Add(this.textBoxPerWattCostUT);
            this.tabUtah.Controls.Add(this.btnCalculateUtah);
            this.tabUtah.Controls.Add(this.label5);
            this.tabUtah.Controls.Add(this.textBoxTotalUtah);
            this.tabUtah.Controls.Add(this.label4);
            this.tabUtah.Controls.Add(this.label3);
            this.tabUtah.Controls.Add(this.label2);
            this.tabUtah.Controls.Add(this.label1);
            this.tabUtah.Controls.Add(this.textBoxArray1Utah);
            this.tabUtah.Controls.Add(this.textBoxArray2Utah);
            this.tabUtah.Controls.Add(this.textBoxArray3Utah);
            this.tabUtah.Controls.Add(this.textBoxArray4Utah);
            this.tabUtah.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabUtah.Location = new System.Drawing.Point(4, 22);
            this.tabUtah.Name = "tabUtah";
            this.tabUtah.Padding = new System.Windows.Forms.Padding(3);
            this.tabUtah.Size = new System.Drawing.Size(1010, 472);
            this.tabUtah.TabIndex = 0;
            this.tabUtah.Text = "Utah";
            this.tabUtah.UseVisualStyleBackColor = true;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(3, 3);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(475, 460);
            this.webBrowser1.TabIndex = 19;
            this.webBrowser1.Url = new System.Uri(resources.GetString("webBrowser1.Url"), System.UriKind.Absolute);
            // 
            // utahChkBtnCash
            // 
            this.utahChkBtnCash.AutoSize = true;
            this.utahChkBtnCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.utahChkBtnCash.Location = new System.Drawing.Point(484, 138);
            this.utahChkBtnCash.Name = "utahChkBtnCash";
            this.utahChkBtnCash.Size = new System.Drawing.Size(50, 17);
            this.utahChkBtnCash.TabIndex = 18;
            this.utahChkBtnCash.Text = "Cash";
            this.utahChkBtnCash.UseVisualStyleBackColor = true;
            // 
            // btnClearUT
            // 
            this.btnClearUT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearUT.Location = new System.Drawing.Point(590, 228);
            this.btnClearUT.Name = "btnClearUT";
            this.btnClearUT.Size = new System.Drawing.Size(75, 23);
            this.btnClearUT.TabIndex = 6;
            this.btnClearUT.Text = "Clear";
            this.btnClearUT.UseVisualStyleBackColor = true;
            this.btnClearUT.Click += new System.EventHandler(this.btnClearUT_Click);
            // 
            // textBoxEmailTemplateUT
            // 
            this.textBoxEmailTemplateUT.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEmailTemplateUT.Location = new System.Drawing.Point(802, 3);
            this.textBoxEmailTemplateUT.Multiline = true;
            this.textBoxEmailTemplateUT.Name = "textBoxEmailTemplateUT";
            this.textBoxEmailTemplateUT.ReadOnly = true;
            this.textBoxEmailTemplateUT.Size = new System.Drawing.Size(205, 460);
            this.textBoxEmailTemplateUT.TabIndex = 17;
            this.textBoxEmailTemplateUT.TabStop = false;
            this.textBoxEmailTemplateUT.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBoxEmailTemplateUT_MouseClick);
            // 
            // textBoxKwTotalUT
            // 
            this.textBoxKwTotalUT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxKwTotalUT.Location = new System.Drawing.Point(696, 200);
            this.textBoxKwTotalUT.Name = "textBoxKwTotalUT";
            this.textBoxKwTotalUT.ReadOnly = true;
            this.textBoxKwTotalUT.Size = new System.Drawing.Size(100, 20);
            this.textBoxKwTotalUT.TabIndex = 16;
            this.textBoxKwTotalUT.TabStop = false;
            // 
            // textBoxArray4KwUT
            // 
            this.textBoxArray4KwUT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArray4KwUT.Location = new System.Drawing.Point(696, 86);
            this.textBoxArray4KwUT.Name = "textBoxArray4KwUT";
            this.textBoxArray4KwUT.ReadOnly = true;
            this.textBoxArray4KwUT.Size = new System.Drawing.Size(100, 20);
            this.textBoxArray4KwUT.TabIndex = 0;
            this.textBoxArray4KwUT.TabStop = false;
            // 
            // textBoxArray1KwUT
            // 
            this.textBoxArray1KwUT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArray1KwUT.Location = new System.Drawing.Point(696, 8);
            this.textBoxArray1KwUT.Name = "textBoxArray1KwUT";
            this.textBoxArray1KwUT.ReadOnly = true;
            this.textBoxArray1KwUT.Size = new System.Drawing.Size(100, 20);
            this.textBoxArray1KwUT.TabIndex = 0;
            this.textBoxArray1KwUT.TabStop = false;
            // 
            // textBoxArray2KwUT
            // 
            this.textBoxArray2KwUT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArray2KwUT.Location = new System.Drawing.Point(696, 34);
            this.textBoxArray2KwUT.Name = "textBoxArray2KwUT";
            this.textBoxArray2KwUT.ReadOnly = true;
            this.textBoxArray2KwUT.Size = new System.Drawing.Size(100, 20);
            this.textBoxArray2KwUT.TabIndex = 0;
            this.textBoxArray2KwUT.TabStop = false;
            // 
            // textBoxArray3KwUT
            // 
            this.textBoxArray3KwUT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArray3KwUT.Location = new System.Drawing.Point(696, 60);
            this.textBoxArray3KwUT.Name = "textBoxArray3KwUT";
            this.textBoxArray3KwUT.ReadOnly = true;
            this.textBoxArray3KwUT.Size = new System.Drawing.Size(100, 20);
            this.textBoxArray3KwUT.TabIndex = 0;
            this.textBoxArray3KwUT.TabStop = false;
            // 
            // lblNinthWatt
            // 
            this.lblNinthWatt.AutoSize = true;
            this.lblNinthWatt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNinthWatt.Location = new System.Drawing.Point(696, 427);
            this.lblNinthWatt.Name = "lblNinthWatt";
            this.lblNinthWatt.Size = new System.Drawing.Size(22, 13);
            this.lblNinthWatt.TabIndex = 14;
            this.lblNinthWatt.Text = "0.0";
            // 
            // lblSixthWatt
            // 
            this.lblSixthWatt.AutoSize = true;
            this.lblSixthWatt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSixthWatt.Location = new System.Drawing.Point(696, 375);
            this.lblSixthWatt.Name = "lblSixthWatt";
            this.lblSixthWatt.Size = new System.Drawing.Size(22, 13);
            this.lblSixthWatt.TabIndex = 14;
            this.lblSixthWatt.Text = "0.0";
            // 
            // lblEighthWatt
            // 
            this.lblEighthWatt.AutoSize = true;
            this.lblEighthWatt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEighthWatt.Location = new System.Drawing.Point(590, 427);
            this.lblEighthWatt.Name = "lblEighthWatt";
            this.lblEighthWatt.Size = new System.Drawing.Size(22, 13);
            this.lblEighthWatt.TabIndex = 13;
            this.lblEighthWatt.Text = "0.0";
            // 
            // lblFifthWatt
            // 
            this.lblFifthWatt.AutoSize = true;
            this.lblFifthWatt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFifthWatt.Location = new System.Drawing.Point(590, 375);
            this.lblFifthWatt.Name = "lblFifthWatt";
            this.lblFifthWatt.Size = new System.Drawing.Size(22, 13);
            this.lblFifthWatt.TabIndex = 13;
            this.lblFifthWatt.Text = "0.0";
            // 
            // lblSeventhWatt
            // 
            this.lblSeventhWatt.AutoSize = true;
            this.lblSeventhWatt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeventhWatt.Location = new System.Drawing.Point(484, 427);
            this.lblSeventhWatt.Name = "lblSeventhWatt";
            this.lblSeventhWatt.Size = new System.Drawing.Size(22, 13);
            this.lblSeventhWatt.TabIndex = 12;
            this.lblSeventhWatt.Text = "0.0";
            // 
            // lblFourthWatt
            // 
            this.lblFourthWatt.AutoSize = true;
            this.lblFourthWatt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFourthWatt.Location = new System.Drawing.Point(484, 375);
            this.lblFourthWatt.Name = "lblFourthWatt";
            this.lblFourthWatt.Size = new System.Drawing.Size(22, 13);
            this.lblFourthWatt.TabIndex = 12;
            this.lblFourthWatt.Text = "0.0";
            // 
            // lblThirdWatt
            // 
            this.lblThirdWatt.AutoSize = true;
            this.lblThirdWatt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThirdWatt.Location = new System.Drawing.Point(696, 322);
            this.lblThirdWatt.Name = "lblThirdWatt";
            this.lblThirdWatt.Size = new System.Drawing.Size(22, 13);
            this.lblThirdWatt.TabIndex = 11;
            this.lblThirdWatt.Text = "0.0";
            // 
            // lblSecondWatt
            // 
            this.lblSecondWatt.AutoSize = true;
            this.lblSecondWatt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecondWatt.Location = new System.Drawing.Point(590, 322);
            this.lblSecondWatt.Name = "lblSecondWatt";
            this.lblSecondWatt.Size = new System.Drawing.Size(22, 13);
            this.lblSecondWatt.TabIndex = 10;
            this.lblSecondWatt.Text = "0.0";
            // 
            // lblFirstWatt
            // 
            this.lblFirstWatt.AutoSize = true;
            this.lblFirstWatt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirstWatt.Location = new System.Drawing.Point(484, 322);
            this.lblFirstWatt.Name = "lblFirstWatt";
            this.lblFirstWatt.Size = new System.Drawing.Size(22, 13);
            this.lblFirstWatt.TabIndex = 9;
            this.lblFirstWatt.Text = "0.0";
            // 
            // textBox340UT
            // 
            this.textBox340UT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox340UT.Location = new System.Drawing.Point(696, 443);
            this.textBox340UT.Name = "textBox340UT";
            this.textBox340UT.ReadOnly = true;
            this.textBox340UT.Size = new System.Drawing.Size(100, 20);
            this.textBox340UT.TabIndex = 0;
            this.textBox340UT.TabStop = false;
            // 
            // textBox355UT
            // 
            this.textBox355UT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox355UT.Location = new System.Drawing.Point(696, 391);
            this.textBox355UT.Name = "textBox355UT";
            this.textBox355UT.ReadOnly = true;
            this.textBox355UT.Size = new System.Drawing.Size(100, 20);
            this.textBox355UT.TabIndex = 0;
            this.textBox355UT.TabStop = false;
            // 
            // textBox350UT
            // 
            this.textBox350UT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox350UT.Location = new System.Drawing.Point(484, 443);
            this.textBox350UT.Name = "textBox350UT";
            this.textBox350UT.ReadOnly = true;
            this.textBox350UT.Size = new System.Drawing.Size(100, 20);
            this.textBox350UT.TabIndex = 0;
            this.textBox350UT.TabStop = false;
            // 
            // textBox380UT
            // 
            this.textBox380UT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox380UT.Location = new System.Drawing.Point(484, 338);
            this.textBox380UT.Name = "textBox380UT";
            this.textBox380UT.ReadOnly = true;
            this.textBox380UT.Size = new System.Drawing.Size(100, 20);
            this.textBox380UT.TabIndex = 0;
            this.textBox380UT.TabStop = false;
            // 
            // textBox345UT
            // 
            this.textBox345UT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox345UT.Location = new System.Drawing.Point(590, 443);
            this.textBox345UT.Name = "textBox345UT";
            this.textBox345UT.ReadOnly = true;
            this.textBox345UT.Size = new System.Drawing.Size(100, 20);
            this.textBox345UT.TabIndex = 0;
            this.textBox345UT.TabStop = false;
            // 
            // textBox365UT
            // 
            this.textBox365UT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox365UT.Location = new System.Drawing.Point(484, 391);
            this.textBox365UT.Name = "textBox365UT";
            this.textBox365UT.ReadOnly = true;
            this.textBox365UT.Size = new System.Drawing.Size(100, 20);
            this.textBox365UT.TabIndex = 0;
            this.textBox365UT.TabStop = false;
            // 
            // textBox360UT
            // 
            this.textBox360UT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox360UT.Location = new System.Drawing.Point(590, 391);
            this.textBox360UT.Name = "textBox360UT";
            this.textBox360UT.ReadOnly = true;
            this.textBox360UT.Size = new System.Drawing.Size(100, 20);
            this.textBox360UT.TabIndex = 0;
            this.textBox360UT.TabStop = false;
            // 
            // textBox375UT
            // 
            this.textBox375UT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox375UT.Location = new System.Drawing.Point(590, 338);
            this.textBox375UT.Name = "textBox375UT";
            this.textBox375UT.ReadOnly = true;
            this.textBox375UT.Size = new System.Drawing.Size(100, 20);
            this.textBox375UT.TabIndex = 0;
            this.textBox375UT.TabStop = false;
            // 
            // textBox370UT
            // 
            this.textBox370UT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox370UT.Location = new System.Drawing.Point(696, 338);
            this.textBox370UT.Name = "textBox370UT";
            this.textBox370UT.ReadOnly = true;
            this.textBox370UT.Size = new System.Drawing.Size(100, 20);
            this.textBox370UT.TabIndex = 0;
            this.textBox370UT.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(590, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Per Watt Cost";
            // 
            // textBoxPerWattCostUT
            // 
            this.textBoxPerWattCostUT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPerWattCostUT.Location = new System.Drawing.Point(484, 112);
            this.textBoxPerWattCostUT.Name = "textBoxPerWattCostUT";
            this.textBoxPerWattCostUT.ReadOnly = true;
            this.textBoxPerWattCostUT.Size = new System.Drawing.Size(100, 20);
            this.textBoxPerWattCostUT.TabIndex = 0;
            this.textBoxPerWattCostUT.TabStop = false;
            this.textBoxPerWattCostUT.Text = "3.80";
            // 
            // btnCalculateUtah
            // 
            this.btnCalculateUtah.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalculateUtah.Location = new System.Drawing.Point(484, 228);
            this.btnCalculateUtah.Name = "btnCalculateUtah";
            this.btnCalculateUtah.Size = new System.Drawing.Size(100, 23);
            this.btnCalculateUtah.TabIndex = 5;
            this.btnCalculateUtah.Text = "Calculate";
            this.btnCalculateUtah.UseVisualStyleBackColor = true;
            this.btnCalculateUtah.Click += new System.EventHandler(this.btnCalculateUtah_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(590, 205);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Total System Cost";
            // 
            // textBoxTotalUtah
            // 
            this.textBoxTotalUtah.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTotalUtah.Location = new System.Drawing.Point(484, 202);
            this.textBoxTotalUtah.Name = "textBoxTotalUtah";
            this.textBoxTotalUtah.ReadOnly = true;
            this.textBoxTotalUtah.Size = new System.Drawing.Size(100, 20);
            this.textBoxTotalUtah.TabIndex = 0;
            this.textBoxTotalUtah.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(590, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Array 4 (North)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(590, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Array 3 (East)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(590, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Array 2 (West)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(590, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Array 1 (South)";
            // 
            // textBoxArray1Utah
            // 
            this.textBoxArray1Utah.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArray1Utah.Location = new System.Drawing.Point(484, 8);
            this.textBoxArray1Utah.Name = "textBoxArray1Utah";
            this.textBoxArray1Utah.Size = new System.Drawing.Size(100, 20);
            this.textBoxArray1Utah.TabIndex = 0;
            this.textBoxArray1Utah.Text = "0";
            // 
            // textBoxArray2Utah
            // 
            this.textBoxArray2Utah.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArray2Utah.Location = new System.Drawing.Point(484, 34);
            this.textBoxArray2Utah.Name = "textBoxArray2Utah";
            this.textBoxArray2Utah.Size = new System.Drawing.Size(100, 20);
            this.textBoxArray2Utah.TabIndex = 1;
            this.textBoxArray2Utah.Text = "0";
            // 
            // textBoxArray3Utah
            // 
            this.textBoxArray3Utah.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArray3Utah.Location = new System.Drawing.Point(484, 60);
            this.textBoxArray3Utah.Name = "textBoxArray3Utah";
            this.textBoxArray3Utah.Size = new System.Drawing.Size(100, 20);
            this.textBoxArray3Utah.TabIndex = 2;
            this.textBoxArray3Utah.Text = "0";
            // 
            // textBoxArray4Utah
            // 
            this.textBoxArray4Utah.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArray4Utah.Location = new System.Drawing.Point(484, 86);
            this.textBoxArray4Utah.Name = "textBoxArray4Utah";
            this.textBoxArray4Utah.Size = new System.Drawing.Size(100, 20);
            this.textBoxArray4Utah.TabIndex = 3;
            this.textBoxArray4Utah.Text = "0";
            // 
            // tabSC
            // 
            this.tabSC.Controls.Add(this.webBrowser2);
            this.tabSC.Controls.Add(this.southCarolinaChkCashBtn);
            this.tabSC.Controls.Add(this.btnClearSC);
            this.tabSC.Controls.Add(this.textBoxEmailTemplateSC);
            this.tabSC.Controls.Add(this.textBoxTotalKWSC);
            this.tabSC.Controls.Add(this.textBoxArrayKWSC4);
            this.tabSC.Controls.Add(this.textBoxArrayKWSC1);
            this.tabSC.Controls.Add(this.textBoxArrayKWSC2);
            this.tabSC.Controls.Add(this.textBoxArrayKWSC3);
            this.tabSC.Controls.Add(this.lblPWCSC9);
            this.tabSC.Controls.Add(this.lblPWCSC8);
            this.tabSC.Controls.Add(this.lblPWCSC7);
            this.tabSC.Controls.Add(this.lblPWCSC6);
            this.tabSC.Controls.Add(this.lblPWCSC5);
            this.tabSC.Controls.Add(this.lblPWCSC4);
            this.tabSC.Controls.Add(this.lblPWCSC3);
            this.tabSC.Controls.Add(this.lblPWCSC2);
            this.tabSC.Controls.Add(this.lblPWCSC1);
            this.tabSC.Controls.Add(this.textBoxPWCDOWNSC9);
            this.tabSC.Controls.Add(this.textBoxPWCDOWNSC8);
            this.tabSC.Controls.Add(this.textBoxPWCDOWNSC7);
            this.tabSC.Controls.Add(this.textBoxPWCDOWNSC6);
            this.tabSC.Controls.Add(this.textBoxPWCDOWNSC1);
            this.tabSC.Controls.Add(this.textBoxPWCDOWNSC4);
            this.tabSC.Controls.Add(this.textBoxPWCDOWNSC5);
            this.tabSC.Controls.Add(this.textBoxPWCDOWNSC2);
            this.tabSC.Controls.Add(this.textBoxPWCDOWNSC3);
            this.tabSC.Controls.Add(this.label14);
            this.tabSC.Controls.Add(this.textBoxPWCSC);
            this.tabSC.Controls.Add(this.btnCalculateSC);
            this.tabSC.Controls.Add(this.label15);
            this.tabSC.Controls.Add(this.textBoxTotalPriceSC);
            this.tabSC.Controls.Add(this.label16);
            this.tabSC.Controls.Add(this.label17);
            this.tabSC.Controls.Add(this.label18);
            this.tabSC.Controls.Add(this.label19);
            this.tabSC.Controls.Add(this.textBoxArraySC1);
            this.tabSC.Controls.Add(this.textBoxArraySC2);
            this.tabSC.Controls.Add(this.textBoxArraySC3);
            this.tabSC.Controls.Add(this.textBoxArraySC4);
            this.tabSC.Location = new System.Drawing.Point(4, 22);
            this.tabSC.Name = "tabSC";
            this.tabSC.Size = new System.Drawing.Size(1010, 472);
            this.tabSC.TabIndex = 1;
            this.tabSC.Text = "South Carolina";
            this.tabSC.UseVisualStyleBackColor = true;
            // 
            // webBrowser2
            // 
            this.webBrowser2.Location = new System.Drawing.Point(3, 3);
            this.webBrowser2.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser2.Name = "webBrowser2";
            this.webBrowser2.Size = new System.Drawing.Size(475, 460);
            this.webBrowser2.TabIndex = 54;
            this.webBrowser2.Url = new System.Uri(resources.GetString("webBrowser2.Url"), System.UriKind.Absolute);
            // 
            // southCarolinaChkCashBtn
            // 
            this.southCarolinaChkCashBtn.AutoSize = true;
            this.southCarolinaChkCashBtn.Location = new System.Drawing.Point(484, 138);
            this.southCarolinaChkCashBtn.Name = "southCarolinaChkCashBtn";
            this.southCarolinaChkCashBtn.Size = new System.Drawing.Size(50, 17);
            this.southCarolinaChkCashBtn.TabIndex = 53;
            this.southCarolinaChkCashBtn.Text = "Cash";
            this.southCarolinaChkCashBtn.UseVisualStyleBackColor = true;
            // 
            // btnClearSC
            // 
            this.btnClearSC.Location = new System.Drawing.Point(590, 228);
            this.btnClearSC.Name = "btnClearSC";
            this.btnClearSC.Size = new System.Drawing.Size(75, 23);
            this.btnClearSC.TabIndex = 43;
            this.btnClearSC.Text = "Clear";
            this.btnClearSC.UseVisualStyleBackColor = true;
            this.btnClearSC.Click += new System.EventHandler(this.btnClearSC_Click);
            // 
            // textBoxEmailTemplateSC
            // 
            this.textBoxEmailTemplateSC.Font = new System.Drawing.Font("Arial Narrow", 11.25F);
            this.textBoxEmailTemplateSC.Location = new System.Drawing.Point(802, 3);
            this.textBoxEmailTemplateSC.Multiline = true;
            this.textBoxEmailTemplateSC.Name = "textBoxEmailTemplateSC";
            this.textBoxEmailTemplateSC.ReadOnly = true;
            this.textBoxEmailTemplateSC.Size = new System.Drawing.Size(205, 460);
            this.textBoxEmailTemplateSC.TabIndex = 52;
            this.textBoxEmailTemplateSC.TabStop = false;
            this.textBoxEmailTemplateSC.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBoxEmailTemplateSC_MouseClick);
            // 
            // textBoxTotalKWSC
            // 
            this.textBoxTotalKWSC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTotalKWSC.Location = new System.Drawing.Point(696, 200);
            this.textBoxTotalKWSC.Name = "textBoxTotalKWSC";
            this.textBoxTotalKWSC.ReadOnly = true;
            this.textBoxTotalKWSC.Size = new System.Drawing.Size(100, 20);
            this.textBoxTotalKWSC.TabIndex = 51;
            this.textBoxTotalKWSC.TabStop = false;
            // 
            // textBoxArrayKWSC4
            // 
            this.textBoxArrayKWSC4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArrayKWSC4.Location = new System.Drawing.Point(696, 86);
            this.textBoxArrayKWSC4.Name = "textBoxArrayKWSC4";
            this.textBoxArrayKWSC4.ReadOnly = true;
            this.textBoxArrayKWSC4.Size = new System.Drawing.Size(100, 20);
            this.textBoxArrayKWSC4.TabIndex = 35;
            this.textBoxArrayKWSC4.TabStop = false;
            // 
            // textBoxArrayKWSC1
            // 
            this.textBoxArrayKWSC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArrayKWSC1.Location = new System.Drawing.Point(696, 8);
            this.textBoxArrayKWSC1.Name = "textBoxArrayKWSC1";
            this.textBoxArrayKWSC1.ReadOnly = true;
            this.textBoxArrayKWSC1.Size = new System.Drawing.Size(100, 20);
            this.textBoxArrayKWSC1.TabIndex = 36;
            this.textBoxArrayKWSC1.TabStop = false;
            // 
            // textBoxArrayKWSC2
            // 
            this.textBoxArrayKWSC2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArrayKWSC2.Location = new System.Drawing.Point(696, 34);
            this.textBoxArrayKWSC2.Name = "textBoxArrayKWSC2";
            this.textBoxArrayKWSC2.ReadOnly = true;
            this.textBoxArrayKWSC2.Size = new System.Drawing.Size(100, 20);
            this.textBoxArrayKWSC2.TabIndex = 37;
            this.textBoxArrayKWSC2.TabStop = false;
            // 
            // textBoxArrayKWSC3
            // 
            this.textBoxArrayKWSC3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArrayKWSC3.Location = new System.Drawing.Point(696, 60);
            this.textBoxArrayKWSC3.Name = "textBoxArrayKWSC3";
            this.textBoxArrayKWSC3.ReadOnly = true;
            this.textBoxArrayKWSC3.Size = new System.Drawing.Size(100, 20);
            this.textBoxArrayKWSC3.TabIndex = 34;
            this.textBoxArrayKWSC3.TabStop = false;
            // 
            // lblPWCSC9
            // 
            this.lblPWCSC9.AutoSize = true;
            this.lblPWCSC9.Location = new System.Drawing.Point(696, 427);
            this.lblPWCSC9.Name = "lblPWCSC9";
            this.lblPWCSC9.Size = new System.Drawing.Size(22, 13);
            this.lblPWCSC9.TabIndex = 50;
            this.lblPWCSC9.Text = "0.0";
            // 
            // lblPWCSC8
            // 
            this.lblPWCSC8.AutoSize = true;
            this.lblPWCSC8.Location = new System.Drawing.Point(590, 427);
            this.lblPWCSC8.Name = "lblPWCSC8";
            this.lblPWCSC8.Size = new System.Drawing.Size(22, 13);
            this.lblPWCSC8.TabIndex = 50;
            this.lblPWCSC8.Text = "0.0";
            // 
            // lblPWCSC7
            // 
            this.lblPWCSC7.AutoSize = true;
            this.lblPWCSC7.Location = new System.Drawing.Point(484, 427);
            this.lblPWCSC7.Name = "lblPWCSC7";
            this.lblPWCSC7.Size = new System.Drawing.Size(22, 13);
            this.lblPWCSC7.TabIndex = 50;
            this.lblPWCSC7.Text = "0.0";
            // 
            // lblPWCSC6
            // 
            this.lblPWCSC6.AutoSize = true;
            this.lblPWCSC6.Location = new System.Drawing.Point(696, 375);
            this.lblPWCSC6.Name = "lblPWCSC6";
            this.lblPWCSC6.Size = new System.Drawing.Size(22, 13);
            this.lblPWCSC6.TabIndex = 49;
            this.lblPWCSC6.Text = "0.0";
            // 
            // lblPWCSC5
            // 
            this.lblPWCSC5.AutoSize = true;
            this.lblPWCSC5.Location = new System.Drawing.Point(590, 375);
            this.lblPWCSC5.Name = "lblPWCSC5";
            this.lblPWCSC5.Size = new System.Drawing.Size(22, 13);
            this.lblPWCSC5.TabIndex = 48;
            this.lblPWCSC5.Text = "0.0";
            // 
            // lblPWCSC4
            // 
            this.lblPWCSC4.AutoSize = true;
            this.lblPWCSC4.Location = new System.Drawing.Point(484, 375);
            this.lblPWCSC4.Name = "lblPWCSC4";
            this.lblPWCSC4.Size = new System.Drawing.Size(22, 13);
            this.lblPWCSC4.TabIndex = 47;
            this.lblPWCSC4.Text = "0.0";
            // 
            // lblPWCSC3
            // 
            this.lblPWCSC3.AutoSize = true;
            this.lblPWCSC3.Location = new System.Drawing.Point(696, 322);
            this.lblPWCSC3.Name = "lblPWCSC3";
            this.lblPWCSC3.Size = new System.Drawing.Size(22, 13);
            this.lblPWCSC3.TabIndex = 46;
            this.lblPWCSC3.Text = "0.0";
            // 
            // lblPWCSC2
            // 
            this.lblPWCSC2.AutoSize = true;
            this.lblPWCSC2.Location = new System.Drawing.Point(590, 322);
            this.lblPWCSC2.Name = "lblPWCSC2";
            this.lblPWCSC2.Size = new System.Drawing.Size(22, 13);
            this.lblPWCSC2.TabIndex = 45;
            this.lblPWCSC2.Text = "0.0";
            // 
            // lblPWCSC1
            // 
            this.lblPWCSC1.AutoSize = true;
            this.lblPWCSC1.Location = new System.Drawing.Point(484, 322);
            this.lblPWCSC1.Name = "lblPWCSC1";
            this.lblPWCSC1.Size = new System.Drawing.Size(22, 13);
            this.lblPWCSC1.TabIndex = 44;
            this.lblPWCSC1.Text = "0.0";
            // 
            // textBoxPWCDOWNSC9
            // 
            this.textBoxPWCDOWNSC9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPWCDOWNSC9.Location = new System.Drawing.Point(696, 443);
            this.textBoxPWCDOWNSC9.Name = "textBoxPWCDOWNSC9";
            this.textBoxPWCDOWNSC9.ReadOnly = true;
            this.textBoxPWCDOWNSC9.Size = new System.Drawing.Size(100, 20);
            this.textBoxPWCDOWNSC9.TabIndex = 38;
            this.textBoxPWCDOWNSC9.TabStop = false;
            // 
            // textBoxPWCDOWNSC8
            // 
            this.textBoxPWCDOWNSC8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPWCDOWNSC8.Location = new System.Drawing.Point(590, 443);
            this.textBoxPWCDOWNSC8.Name = "textBoxPWCDOWNSC8";
            this.textBoxPWCDOWNSC8.ReadOnly = true;
            this.textBoxPWCDOWNSC8.Size = new System.Drawing.Size(100, 20);
            this.textBoxPWCDOWNSC8.TabIndex = 38;
            this.textBoxPWCDOWNSC8.TabStop = false;
            // 
            // textBoxPWCDOWNSC7
            // 
            this.textBoxPWCDOWNSC7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPWCDOWNSC7.Location = new System.Drawing.Point(484, 443);
            this.textBoxPWCDOWNSC7.Name = "textBoxPWCDOWNSC7";
            this.textBoxPWCDOWNSC7.ReadOnly = true;
            this.textBoxPWCDOWNSC7.Size = new System.Drawing.Size(100, 20);
            this.textBoxPWCDOWNSC7.TabIndex = 38;
            this.textBoxPWCDOWNSC7.TabStop = false;
            // 
            // textBoxPWCDOWNSC6
            // 
            this.textBoxPWCDOWNSC6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPWCDOWNSC6.Location = new System.Drawing.Point(696, 391);
            this.textBoxPWCDOWNSC6.Name = "textBoxPWCDOWNSC6";
            this.textBoxPWCDOWNSC6.ReadOnly = true;
            this.textBoxPWCDOWNSC6.Size = new System.Drawing.Size(100, 20);
            this.textBoxPWCDOWNSC6.TabIndex = 32;
            this.textBoxPWCDOWNSC6.TabStop = false;
            // 
            // textBoxPWCDOWNSC1
            // 
            this.textBoxPWCDOWNSC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPWCDOWNSC1.Location = new System.Drawing.Point(484, 338);
            this.textBoxPWCDOWNSC1.Name = "textBoxPWCDOWNSC1";
            this.textBoxPWCDOWNSC1.ReadOnly = true;
            this.textBoxPWCDOWNSC1.Size = new System.Drawing.Size(100, 20);
            this.textBoxPWCDOWNSC1.TabIndex = 31;
            this.textBoxPWCDOWNSC1.TabStop = false;
            // 
            // textBoxPWCDOWNSC4
            // 
            this.textBoxPWCDOWNSC4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPWCDOWNSC4.Location = new System.Drawing.Point(484, 391);
            this.textBoxPWCDOWNSC4.Name = "textBoxPWCDOWNSC4";
            this.textBoxPWCDOWNSC4.ReadOnly = true;
            this.textBoxPWCDOWNSC4.Size = new System.Drawing.Size(100, 20);
            this.textBoxPWCDOWNSC4.TabIndex = 18;
            this.textBoxPWCDOWNSC4.TabStop = false;
            // 
            // textBoxPWCDOWNSC5
            // 
            this.textBoxPWCDOWNSC5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPWCDOWNSC5.Location = new System.Drawing.Point(590, 391);
            this.textBoxPWCDOWNSC5.Name = "textBoxPWCDOWNSC5";
            this.textBoxPWCDOWNSC5.ReadOnly = true;
            this.textBoxPWCDOWNSC5.Size = new System.Drawing.Size(100, 20);
            this.textBoxPWCDOWNSC5.TabIndex = 30;
            this.textBoxPWCDOWNSC5.TabStop = false;
            // 
            // textBoxPWCDOWNSC2
            // 
            this.textBoxPWCDOWNSC2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPWCDOWNSC2.Location = new System.Drawing.Point(590, 338);
            this.textBoxPWCDOWNSC2.Name = "textBoxPWCDOWNSC2";
            this.textBoxPWCDOWNSC2.ReadOnly = true;
            this.textBoxPWCDOWNSC2.Size = new System.Drawing.Size(100, 20);
            this.textBoxPWCDOWNSC2.TabIndex = 29;
            this.textBoxPWCDOWNSC2.TabStop = false;
            // 
            // textBoxPWCDOWNSC3
            // 
            this.textBoxPWCDOWNSC3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPWCDOWNSC3.Location = new System.Drawing.Point(696, 338);
            this.textBoxPWCDOWNSC3.Name = "textBoxPWCDOWNSC3";
            this.textBoxPWCDOWNSC3.ReadOnly = true;
            this.textBoxPWCDOWNSC3.Size = new System.Drawing.Size(100, 20);
            this.textBoxPWCDOWNSC3.TabIndex = 28;
            this.textBoxPWCDOWNSC3.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(590, 116);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Per Watt Cost";
            // 
            // textBoxPWCSC
            // 
            this.textBoxPWCSC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPWCSC.Location = new System.Drawing.Point(484, 112);
            this.textBoxPWCSC.Name = "textBoxPWCSC";
            this.textBoxPWCSC.ReadOnly = true;
            this.textBoxPWCSC.Size = new System.Drawing.Size(100, 20);
            this.textBoxPWCSC.TabIndex = 26;
            this.textBoxPWCSC.TabStop = false;
            this.textBoxPWCSC.Text = "3.80";
            // 
            // btnCalculateSC
            // 
            this.btnCalculateSC.Location = new System.Drawing.Point(484, 228);
            this.btnCalculateSC.Name = "btnCalculateSC";
            this.btnCalculateSC.Size = new System.Drawing.Size(100, 23);
            this.btnCalculateSC.TabIndex = 42;
            this.btnCalculateSC.Text = "Calculate";
            this.btnCalculateSC.UseVisualStyleBackColor = true;
            this.btnCalculateSC.Click += new System.EventHandler(this.button2_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(590, 205);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(92, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "Total System Cost";
            // 
            // textBoxTotalPriceSC
            // 
            this.textBoxTotalPriceSC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTotalPriceSC.Location = new System.Drawing.Point(484, 202);
            this.textBoxTotalPriceSC.Name = "textBoxTotalPriceSC";
            this.textBoxTotalPriceSC.ReadOnly = true;
            this.textBoxTotalPriceSC.Size = new System.Drawing.Size(100, 20);
            this.textBoxTotalPriceSC.TabIndex = 24;
            this.textBoxTotalPriceSC.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(590, 89);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 23;
            this.label16.Text = "Array 4 (North)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(590, 63);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "Array 3 (East)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(590, 37);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 13);
            this.label18.TabIndex = 21;
            this.label18.Text = "Array 2 (West)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(590, 11);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 13);
            this.label19.TabIndex = 20;
            this.label19.Text = "Array 1 (South)";
            // 
            // textBoxArraySC1
            // 
            this.textBoxArraySC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArraySC1.Location = new System.Drawing.Point(484, 8);
            this.textBoxArraySC1.Name = "textBoxArraySC1";
            this.textBoxArraySC1.Size = new System.Drawing.Size(100, 20);
            this.textBoxArraySC1.TabIndex = 19;
            this.textBoxArraySC1.Text = "0";
            // 
            // textBoxArraySC2
            // 
            this.textBoxArraySC2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArraySC2.Location = new System.Drawing.Point(484, 34);
            this.textBoxArraySC2.Name = "textBoxArraySC2";
            this.textBoxArraySC2.Size = new System.Drawing.Size(100, 20);
            this.textBoxArraySC2.TabIndex = 39;
            this.textBoxArraySC2.Text = "0";
            // 
            // textBoxArraySC3
            // 
            this.textBoxArraySC3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArraySC3.Location = new System.Drawing.Point(484, 60);
            this.textBoxArraySC3.Name = "textBoxArraySC3";
            this.textBoxArraySC3.Size = new System.Drawing.Size(100, 20);
            this.textBoxArraySC3.TabIndex = 40;
            this.textBoxArraySC3.Text = "0";
            // 
            // textBoxArraySC4
            // 
            this.textBoxArraySC4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArraySC4.Location = new System.Drawing.Point(484, 86);
            this.textBoxArraySC4.Name = "textBoxArraySC4";
            this.textBoxArraySC4.Size = new System.Drawing.Size(100, 20);
            this.textBoxArraySC4.TabIndex = 41;
            this.textBoxArraySC4.Text = "0";
            // 
            // tabCalifornia
            // 
            this.tabCalifornia.Controls.Add(this.webBrowser3);
            this.tabCalifornia.Controls.Add(this.californiaChkCashBtn);
            this.tabCalifornia.Controls.Add(this.btnCAClear);
            this.tabCalifornia.Controls.Add(this.txtCAEmailTemplate);
            this.tabCalifornia.Controls.Add(this.txtCAKWTotal);
            this.tabCalifornia.Controls.Add(this.txtCAKWArray4);
            this.tabCalifornia.Controls.Add(this.txtCAKWArray1);
            this.tabCalifornia.Controls.Add(this.txtCAKWArray2);
            this.tabCalifornia.Controls.Add(this.txtCAKWArray3);
            this.tabCalifornia.Controls.Add(this.lblCAPWCDown9);
            this.tabCalifornia.Controls.Add(this.lblCAPWCDown8);
            this.tabCalifornia.Controls.Add(this.lblCAPWCDown7);
            this.tabCalifornia.Controls.Add(this.lblCAPWCDown6);
            this.tabCalifornia.Controls.Add(this.lblCAPWCDown5);
            this.tabCalifornia.Controls.Add(this.lblCAPWCDown4);
            this.tabCalifornia.Controls.Add(this.lblCAPWCDown3);
            this.tabCalifornia.Controls.Add(this.lblCAPWCDown2);
            this.tabCalifornia.Controls.Add(this.lblCAPWCDown1);
            this.tabCalifornia.Controls.Add(this.txtCATotalSystemCost10);
            this.tabCalifornia.Controls.Add(this.txtCATotalSystemCost9);
            this.tabCalifornia.Controls.Add(this.txtCATotalSystemCost8);
            this.tabCalifornia.Controls.Add(this.txtCATotalSystemCost7);
            this.tabCalifornia.Controls.Add(this.txtCATotalSystemCost2);
            this.tabCalifornia.Controls.Add(this.txtCATotalSystemCost5);
            this.tabCalifornia.Controls.Add(this.txtCATotalSystemCost6);
            this.tabCalifornia.Controls.Add(this.txtCATotalSystemCost3);
            this.tabCalifornia.Controls.Add(this.txtCATotalSystemCost4);
            this.tabCalifornia.Controls.Add(this.lblCAPWC);
            this.tabCalifornia.Controls.Add(this.txtCAPWC);
            this.tabCalifornia.Controls.Add(this.btnCACalculate);
            this.tabCalifornia.Controls.Add(this.lblCATotalSystemCost);
            this.tabCalifornia.Controls.Add(this.txtCATotalSystemCost1);
            this.tabCalifornia.Controls.Add(this.lblCAArray4);
            this.tabCalifornia.Controls.Add(this.lblCAArray3);
            this.tabCalifornia.Controls.Add(this.lblCAArray2);
            this.tabCalifornia.Controls.Add(this.lblCAArray1);
            this.tabCalifornia.Controls.Add(this.txtCAArray1);
            this.tabCalifornia.Controls.Add(this.txtCAArray2);
            this.tabCalifornia.Controls.Add(this.txtCAArray3);
            this.tabCalifornia.Controls.Add(this.txtCAArray4);
            this.tabCalifornia.Location = new System.Drawing.Point(4, 22);
            this.tabCalifornia.Name = "tabCalifornia";
            this.tabCalifornia.Size = new System.Drawing.Size(1010, 472);
            this.tabCalifornia.TabIndex = 3;
            this.tabCalifornia.Text = "California";
            this.tabCalifornia.UseVisualStyleBackColor = true;
            // 
            // webBrowser3
            // 
            this.webBrowser3.Location = new System.Drawing.Point(3, 3);
            this.webBrowser3.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser3.Name = "webBrowser3";
            this.webBrowser3.Size = new System.Drawing.Size(475, 460);
            this.webBrowser3.TabIndex = 93;
            this.webBrowser3.Url = new System.Uri(resources.GetString("webBrowser3.Url"), System.UriKind.Absolute);
            // 
            // californiaChkCashBtn
            // 
            this.californiaChkCashBtn.AutoSize = true;
            this.californiaChkCashBtn.Location = new System.Drawing.Point(484, 138);
            this.californiaChkCashBtn.Name = "californiaChkCashBtn";
            this.californiaChkCashBtn.Size = new System.Drawing.Size(50, 17);
            this.californiaChkCashBtn.TabIndex = 92;
            this.californiaChkCashBtn.Text = "Cash";
            this.californiaChkCashBtn.UseVisualStyleBackColor = true;
            // 
            // btnCAClear
            // 
            this.btnCAClear.Location = new System.Drawing.Point(590, 228);
            this.btnCAClear.Name = "btnCAClear";
            this.btnCAClear.Size = new System.Drawing.Size(75, 23);
            this.btnCAClear.TabIndex = 80;
            this.btnCAClear.Text = "Clear";
            this.btnCAClear.UseVisualStyleBackColor = true;
            this.btnCAClear.Click += new System.EventHandler(this.btnCAClear_Click);
            // 
            // txtCAEmailTemplate
            // 
            this.txtCAEmailTemplate.Font = new System.Drawing.Font("Arial Narrow", 11.25F);
            this.txtCAEmailTemplate.Location = new System.Drawing.Point(802, 3);
            this.txtCAEmailTemplate.Multiline = true;
            this.txtCAEmailTemplate.Name = "txtCAEmailTemplate";
            this.txtCAEmailTemplate.ReadOnly = true;
            this.txtCAEmailTemplate.Size = new System.Drawing.Size(205, 460);
            this.txtCAEmailTemplate.TabIndex = 91;
            this.txtCAEmailTemplate.TabStop = false;
            this.txtCAEmailTemplate.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtCAEmailTemplate_MouseClick);
            // 
            // txtCAKWTotal
            // 
            this.txtCAKWTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCAKWTotal.Location = new System.Drawing.Point(696, 200);
            this.txtCAKWTotal.Name = "txtCAKWTotal";
            this.txtCAKWTotal.ReadOnly = true;
            this.txtCAKWTotal.Size = new System.Drawing.Size(100, 20);
            this.txtCAKWTotal.TabIndex = 90;
            this.txtCAKWTotal.TabStop = false;
            // 
            // txtCAKWArray4
            // 
            this.txtCAKWArray4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCAKWArray4.Location = new System.Drawing.Point(696, 86);
            this.txtCAKWArray4.Name = "txtCAKWArray4";
            this.txtCAKWArray4.ReadOnly = true;
            this.txtCAKWArray4.Size = new System.Drawing.Size(100, 20);
            this.txtCAKWArray4.TabIndex = 70;
            this.txtCAKWArray4.TabStop = false;
            // 
            // txtCAKWArray1
            // 
            this.txtCAKWArray1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCAKWArray1.Location = new System.Drawing.Point(696, 8);
            this.txtCAKWArray1.Name = "txtCAKWArray1";
            this.txtCAKWArray1.ReadOnly = true;
            this.txtCAKWArray1.Size = new System.Drawing.Size(100, 20);
            this.txtCAKWArray1.TabIndex = 71;
            this.txtCAKWArray1.TabStop = false;
            // 
            // txtCAKWArray2
            // 
            this.txtCAKWArray2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCAKWArray2.Location = new System.Drawing.Point(696, 34);
            this.txtCAKWArray2.Name = "txtCAKWArray2";
            this.txtCAKWArray2.ReadOnly = true;
            this.txtCAKWArray2.Size = new System.Drawing.Size(100, 20);
            this.txtCAKWArray2.TabIndex = 72;
            this.txtCAKWArray2.TabStop = false;
            // 
            // txtCAKWArray3
            // 
            this.txtCAKWArray3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCAKWArray3.Location = new System.Drawing.Point(696, 60);
            this.txtCAKWArray3.Name = "txtCAKWArray3";
            this.txtCAKWArray3.ReadOnly = true;
            this.txtCAKWArray3.Size = new System.Drawing.Size(100, 20);
            this.txtCAKWArray3.TabIndex = 69;
            this.txtCAKWArray3.TabStop = false;
            // 
            // lblCAPWCDown9
            // 
            this.lblCAPWCDown9.AutoSize = true;
            this.lblCAPWCDown9.Location = new System.Drawing.Point(696, 427);
            this.lblCAPWCDown9.Name = "lblCAPWCDown9";
            this.lblCAPWCDown9.Size = new System.Drawing.Size(22, 13);
            this.lblCAPWCDown9.TabIndex = 87;
            this.lblCAPWCDown9.Text = "0.0";
            // 
            // lblCAPWCDown8
            // 
            this.lblCAPWCDown8.AutoSize = true;
            this.lblCAPWCDown8.Location = new System.Drawing.Point(590, 427);
            this.lblCAPWCDown8.Name = "lblCAPWCDown8";
            this.lblCAPWCDown8.Size = new System.Drawing.Size(22, 13);
            this.lblCAPWCDown8.TabIndex = 89;
            this.lblCAPWCDown8.Text = "0.0";
            // 
            // lblCAPWCDown7
            // 
            this.lblCAPWCDown7.AutoSize = true;
            this.lblCAPWCDown7.Location = new System.Drawing.Point(484, 427);
            this.lblCAPWCDown7.Name = "lblCAPWCDown7";
            this.lblCAPWCDown7.Size = new System.Drawing.Size(22, 13);
            this.lblCAPWCDown7.TabIndex = 88;
            this.lblCAPWCDown7.Text = "0.0";
            // 
            // lblCAPWCDown6
            // 
            this.lblCAPWCDown6.AutoSize = true;
            this.lblCAPWCDown6.Location = new System.Drawing.Point(696, 375);
            this.lblCAPWCDown6.Name = "lblCAPWCDown6";
            this.lblCAPWCDown6.Size = new System.Drawing.Size(22, 13);
            this.lblCAPWCDown6.TabIndex = 86;
            this.lblCAPWCDown6.Text = "0.0";
            // 
            // lblCAPWCDown5
            // 
            this.lblCAPWCDown5.AutoSize = true;
            this.lblCAPWCDown5.Location = new System.Drawing.Point(590, 375);
            this.lblCAPWCDown5.Name = "lblCAPWCDown5";
            this.lblCAPWCDown5.Size = new System.Drawing.Size(22, 13);
            this.lblCAPWCDown5.TabIndex = 85;
            this.lblCAPWCDown5.Text = "0.0";
            // 
            // lblCAPWCDown4
            // 
            this.lblCAPWCDown4.AutoSize = true;
            this.lblCAPWCDown4.Location = new System.Drawing.Point(484, 375);
            this.lblCAPWCDown4.Name = "lblCAPWCDown4";
            this.lblCAPWCDown4.Size = new System.Drawing.Size(22, 13);
            this.lblCAPWCDown4.TabIndex = 84;
            this.lblCAPWCDown4.Text = "0.0";
            // 
            // lblCAPWCDown3
            // 
            this.lblCAPWCDown3.AutoSize = true;
            this.lblCAPWCDown3.Location = new System.Drawing.Point(696, 322);
            this.lblCAPWCDown3.Name = "lblCAPWCDown3";
            this.lblCAPWCDown3.Size = new System.Drawing.Size(22, 13);
            this.lblCAPWCDown3.TabIndex = 83;
            this.lblCAPWCDown3.Text = "0.0";
            // 
            // lblCAPWCDown2
            // 
            this.lblCAPWCDown2.AutoSize = true;
            this.lblCAPWCDown2.Location = new System.Drawing.Point(590, 322);
            this.lblCAPWCDown2.Name = "lblCAPWCDown2";
            this.lblCAPWCDown2.Size = new System.Drawing.Size(22, 13);
            this.lblCAPWCDown2.TabIndex = 82;
            this.lblCAPWCDown2.Text = "0.0";
            // 
            // lblCAPWCDown1
            // 
            this.lblCAPWCDown1.AutoSize = true;
            this.lblCAPWCDown1.Location = new System.Drawing.Point(484, 322);
            this.lblCAPWCDown1.Name = "lblCAPWCDown1";
            this.lblCAPWCDown1.Size = new System.Drawing.Size(22, 13);
            this.lblCAPWCDown1.TabIndex = 81;
            this.lblCAPWCDown1.Text = "0.0";
            // 
            // txtCATotalSystemCost10
            // 
            this.txtCATotalSystemCost10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCATotalSystemCost10.Location = new System.Drawing.Point(696, 443);
            this.txtCATotalSystemCost10.Name = "txtCATotalSystemCost10";
            this.txtCATotalSystemCost10.ReadOnly = true;
            this.txtCATotalSystemCost10.Size = new System.Drawing.Size(100, 20);
            this.txtCATotalSystemCost10.TabIndex = 73;
            this.txtCATotalSystemCost10.TabStop = false;
            // 
            // txtCATotalSystemCost9
            // 
            this.txtCATotalSystemCost9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCATotalSystemCost9.Location = new System.Drawing.Point(590, 443);
            this.txtCATotalSystemCost9.Name = "txtCATotalSystemCost9";
            this.txtCATotalSystemCost9.ReadOnly = true;
            this.txtCATotalSystemCost9.Size = new System.Drawing.Size(100, 20);
            this.txtCATotalSystemCost9.TabIndex = 74;
            this.txtCATotalSystemCost9.TabStop = false;
            // 
            // txtCATotalSystemCost8
            // 
            this.txtCATotalSystemCost8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCATotalSystemCost8.Location = new System.Drawing.Point(484, 443);
            this.txtCATotalSystemCost8.Name = "txtCATotalSystemCost8";
            this.txtCATotalSystemCost8.ReadOnly = true;
            this.txtCATotalSystemCost8.Size = new System.Drawing.Size(100, 20);
            this.txtCATotalSystemCost8.TabIndex = 75;
            this.txtCATotalSystemCost8.TabStop = false;
            // 
            // txtCATotalSystemCost7
            // 
            this.txtCATotalSystemCost7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCATotalSystemCost7.Location = new System.Drawing.Point(696, 391);
            this.txtCATotalSystemCost7.Name = "txtCATotalSystemCost7";
            this.txtCATotalSystemCost7.ReadOnly = true;
            this.txtCATotalSystemCost7.Size = new System.Drawing.Size(100, 20);
            this.txtCATotalSystemCost7.TabIndex = 67;
            this.txtCATotalSystemCost7.TabStop = false;
            // 
            // txtCATotalSystemCost2
            // 
            this.txtCATotalSystemCost2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCATotalSystemCost2.Location = new System.Drawing.Point(484, 338);
            this.txtCATotalSystemCost2.Name = "txtCATotalSystemCost2";
            this.txtCATotalSystemCost2.ReadOnly = true;
            this.txtCATotalSystemCost2.Size = new System.Drawing.Size(100, 20);
            this.txtCATotalSystemCost2.TabIndex = 66;
            this.txtCATotalSystemCost2.TabStop = false;
            // 
            // txtCATotalSystemCost5
            // 
            this.txtCATotalSystemCost5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCATotalSystemCost5.Location = new System.Drawing.Point(484, 391);
            this.txtCATotalSystemCost5.Name = "txtCATotalSystemCost5";
            this.txtCATotalSystemCost5.ReadOnly = true;
            this.txtCATotalSystemCost5.Size = new System.Drawing.Size(100, 20);
            this.txtCATotalSystemCost5.TabIndex = 53;
            this.txtCATotalSystemCost5.TabStop = false;
            // 
            // txtCATotalSystemCost6
            // 
            this.txtCATotalSystemCost6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCATotalSystemCost6.Location = new System.Drawing.Point(590, 391);
            this.txtCATotalSystemCost6.Name = "txtCATotalSystemCost6";
            this.txtCATotalSystemCost6.ReadOnly = true;
            this.txtCATotalSystemCost6.Size = new System.Drawing.Size(100, 20);
            this.txtCATotalSystemCost6.TabIndex = 65;
            this.txtCATotalSystemCost6.TabStop = false;
            // 
            // txtCATotalSystemCost3
            // 
            this.txtCATotalSystemCost3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCATotalSystemCost3.Location = new System.Drawing.Point(590, 338);
            this.txtCATotalSystemCost3.Name = "txtCATotalSystemCost3";
            this.txtCATotalSystemCost3.ReadOnly = true;
            this.txtCATotalSystemCost3.Size = new System.Drawing.Size(100, 20);
            this.txtCATotalSystemCost3.TabIndex = 64;
            this.txtCATotalSystemCost3.TabStop = false;
            // 
            // txtCATotalSystemCost4
            // 
            this.txtCATotalSystemCost4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCATotalSystemCost4.Location = new System.Drawing.Point(696, 338);
            this.txtCATotalSystemCost4.Name = "txtCATotalSystemCost4";
            this.txtCATotalSystemCost4.ReadOnly = true;
            this.txtCATotalSystemCost4.Size = new System.Drawing.Size(100, 20);
            this.txtCATotalSystemCost4.TabIndex = 63;
            this.txtCATotalSystemCost4.TabStop = false;
            // 
            // lblCAPWC
            // 
            this.lblCAPWC.AutoSize = true;
            this.lblCAPWC.Location = new System.Drawing.Point(590, 116);
            this.lblCAPWC.Name = "lblCAPWC";
            this.lblCAPWC.Size = new System.Drawing.Size(73, 13);
            this.lblCAPWC.TabIndex = 62;
            this.lblCAPWC.Text = "Per Watt Cost";
            // 
            // txtCAPWC
            // 
            this.txtCAPWC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCAPWC.Location = new System.Drawing.Point(484, 112);
            this.txtCAPWC.Name = "txtCAPWC";
            this.txtCAPWC.ReadOnly = true;
            this.txtCAPWC.Size = new System.Drawing.Size(100, 20);
            this.txtCAPWC.TabIndex = 61;
            this.txtCAPWC.TabStop = false;
            this.txtCAPWC.Text = "4.25";
            // 
            // btnCACalculate
            // 
            this.btnCACalculate.Location = new System.Drawing.Point(484, 228);
            this.btnCACalculate.Name = "btnCACalculate";
            this.btnCACalculate.Size = new System.Drawing.Size(100, 23);
            this.btnCACalculate.TabIndex = 79;
            this.btnCACalculate.Text = "Calculate";
            this.btnCACalculate.UseVisualStyleBackColor = true;
            this.btnCACalculate.Click += new System.EventHandler(this.btnCACalculate_Click);
            // 
            // lblCATotalSystemCost
            // 
            this.lblCATotalSystemCost.AutoSize = true;
            this.lblCATotalSystemCost.Location = new System.Drawing.Point(590, 205);
            this.lblCATotalSystemCost.Name = "lblCATotalSystemCost";
            this.lblCATotalSystemCost.Size = new System.Drawing.Size(92, 13);
            this.lblCATotalSystemCost.TabIndex = 60;
            this.lblCATotalSystemCost.Text = "Total System Cost";
            // 
            // txtCATotalSystemCost1
            // 
            this.txtCATotalSystemCost1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCATotalSystemCost1.Location = new System.Drawing.Point(484, 202);
            this.txtCATotalSystemCost1.Name = "txtCATotalSystemCost1";
            this.txtCATotalSystemCost1.ReadOnly = true;
            this.txtCATotalSystemCost1.Size = new System.Drawing.Size(100, 20);
            this.txtCATotalSystemCost1.TabIndex = 59;
            this.txtCATotalSystemCost1.TabStop = false;
            // 
            // lblCAArray4
            // 
            this.lblCAArray4.AutoSize = true;
            this.lblCAArray4.Location = new System.Drawing.Point(590, 89);
            this.lblCAArray4.Name = "lblCAArray4";
            this.lblCAArray4.Size = new System.Drawing.Size(75, 13);
            this.lblCAArray4.TabIndex = 58;
            this.lblCAArray4.Text = "Array 4 (North)";
            // 
            // lblCAArray3
            // 
            this.lblCAArray3.AutoSize = true;
            this.lblCAArray3.Location = new System.Drawing.Point(590, 63);
            this.lblCAArray3.Name = "lblCAArray3";
            this.lblCAArray3.Size = new System.Drawing.Size(70, 13);
            this.lblCAArray3.TabIndex = 57;
            this.lblCAArray3.Text = "Array 3 (East)";
            // 
            // lblCAArray2
            // 
            this.lblCAArray2.AutoSize = true;
            this.lblCAArray2.Location = new System.Drawing.Point(590, 37);
            this.lblCAArray2.Name = "lblCAArray2";
            this.lblCAArray2.Size = new System.Drawing.Size(74, 13);
            this.lblCAArray2.TabIndex = 56;
            this.lblCAArray2.Text = "Array 2 (West)";
            // 
            // lblCAArray1
            // 
            this.lblCAArray1.AutoSize = true;
            this.lblCAArray1.Location = new System.Drawing.Point(590, 11);
            this.lblCAArray1.Name = "lblCAArray1";
            this.lblCAArray1.Size = new System.Drawing.Size(77, 13);
            this.lblCAArray1.TabIndex = 55;
            this.lblCAArray1.Text = "Array 1 (South)";
            // 
            // txtCAArray1
            // 
            this.txtCAArray1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCAArray1.Location = new System.Drawing.Point(484, 8);
            this.txtCAArray1.Name = "txtCAArray1";
            this.txtCAArray1.Size = new System.Drawing.Size(100, 20);
            this.txtCAArray1.TabIndex = 54;
            this.txtCAArray1.Text = "0";
            // 
            // txtCAArray2
            // 
            this.txtCAArray2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCAArray2.Location = new System.Drawing.Point(484, 34);
            this.txtCAArray2.Name = "txtCAArray2";
            this.txtCAArray2.Size = new System.Drawing.Size(100, 20);
            this.txtCAArray2.TabIndex = 76;
            this.txtCAArray2.Text = "0";
            // 
            // txtCAArray3
            // 
            this.txtCAArray3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCAArray3.Location = new System.Drawing.Point(484, 60);
            this.txtCAArray3.Name = "txtCAArray3";
            this.txtCAArray3.Size = new System.Drawing.Size(100, 20);
            this.txtCAArray3.TabIndex = 77;
            this.txtCAArray3.Text = "0";
            // 
            // txtCAArray4
            // 
            this.txtCAArray4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCAArray4.Location = new System.Drawing.Point(484, 86);
            this.txtCAArray4.Name = "txtCAArray4";
            this.txtCAArray4.Size = new System.Drawing.Size(100, 20);
            this.txtCAArray4.TabIndex = 78;
            this.txtCAArray4.Text = "0";
            // 
            // tabTexasAustin
            // 
            this.tabTexasAustin.Controls.Add(this.webBrowser4);
            this.tabTexasAustin.Controls.Add(this.austinChkCashBtn);
            this.tabTexasAustin.Controls.Add(this.btnAustinClear);
            this.tabTexasAustin.Controls.Add(this.austinTxtEmailTemplate);
            this.tabTexasAustin.Controls.Add(this.austinTxtKwTotal);
            this.tabTexasAustin.Controls.Add(this.austinTxtKwArray4);
            this.tabTexasAustin.Controls.Add(this.austinTxtKwArray1);
            this.tabTexasAustin.Controls.Add(this.austinTxtKwArray2);
            this.tabTexasAustin.Controls.Add(this.austinTxtKwArray3);
            this.tabTexasAustin.Controls.Add(this.lblAustinPWCDown9);
            this.tabTexasAustin.Controls.Add(this.lblAustinPWCDown8);
            this.tabTexasAustin.Controls.Add(this.lblAustinPWCDown7);
            this.tabTexasAustin.Controls.Add(this.lblAustinPWCDown6);
            this.tabTexasAustin.Controls.Add(this.lblAustinPWCDown5);
            this.tabTexasAustin.Controls.Add(this.lblAustinPWCDown4);
            this.tabTexasAustin.Controls.Add(this.lblAustinPWCDown3);
            this.tabTexasAustin.Controls.Add(this.lblAustinPWCDown2);
            this.tabTexasAustin.Controls.Add(this.lblAustinPWCDown1);
            this.tabTexasAustin.Controls.Add(this.austinTxtTotalSystemCost10);
            this.tabTexasAustin.Controls.Add(this.austinTxtTotalSystemCost9);
            this.tabTexasAustin.Controls.Add(this.austinTxtTotalSystemCost8);
            this.tabTexasAustin.Controls.Add(this.austinTxtTotalSystemCost7);
            this.tabTexasAustin.Controls.Add(this.austinTxtTotalSystemCost2);
            this.tabTexasAustin.Controls.Add(this.austinTxtTotalSystemCost5);
            this.tabTexasAustin.Controls.Add(this.austinTxtTotalSystemCost6);
            this.tabTexasAustin.Controls.Add(this.austinTxtTotalSystemCost3);
            this.tabTexasAustin.Controls.Add(this.austinTxtTotalSystemCost4);
            this.tabTexasAustin.Controls.Add(this.lblAustinPWC);
            this.tabTexasAustin.Controls.Add(this.austinTxtPWC1);
            this.tabTexasAustin.Controls.Add(this.btnAustinCalc);
            this.tabTexasAustin.Controls.Add(this.lblAustinTotalSystemCost);
            this.tabTexasAustin.Controls.Add(this.austinTxtTotalSystemCost1);
            this.tabTexasAustin.Controls.Add(this.lblAustinArray4);
            this.tabTexasAustin.Controls.Add(this.lblAustinArray3);
            this.tabTexasAustin.Controls.Add(this.lblAustinArray2);
            this.tabTexasAustin.Controls.Add(this.lblAustinArray1);
            this.tabTexasAustin.Controls.Add(this.austinTxtArray1);
            this.tabTexasAustin.Controls.Add(this.austinTxtArray2);
            this.tabTexasAustin.Controls.Add(this.austinTxtArray3);
            this.tabTexasAustin.Controls.Add(this.austinTxtArray4);
            this.tabTexasAustin.Location = new System.Drawing.Point(4, 22);
            this.tabTexasAustin.Name = "tabTexasAustin";
            this.tabTexasAustin.Size = new System.Drawing.Size(1010, 472);
            this.tabTexasAustin.TabIndex = 4;
            this.tabTexasAustin.Text = "Texas - Austin";
            this.tabTexasAustin.UseVisualStyleBackColor = true;
            // 
            // webBrowser4
            // 
            this.webBrowser4.Location = new System.Drawing.Point(3, 3);
            this.webBrowser4.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser4.Name = "webBrowser4";
            this.webBrowser4.Size = new System.Drawing.Size(475, 460);
            this.webBrowser4.TabIndex = 132;
            this.webBrowser4.Url = new System.Uri(resources.GetString("webBrowser4.Url"), System.UriKind.Absolute);
            // 
            // austinChkCashBtn
            // 
            this.austinChkCashBtn.AutoSize = true;
            this.austinChkCashBtn.Location = new System.Drawing.Point(484, 138);
            this.austinChkCashBtn.Name = "austinChkCashBtn";
            this.austinChkCashBtn.Size = new System.Drawing.Size(50, 17);
            this.austinChkCashBtn.TabIndex = 131;
            this.austinChkCashBtn.Text = "Cash";
            this.austinChkCashBtn.UseVisualStyleBackColor = true;
            // 
            // btnAustinClear
            // 
            this.btnAustinClear.Location = new System.Drawing.Point(590, 228);
            this.btnAustinClear.Name = "btnAustinClear";
            this.btnAustinClear.Size = new System.Drawing.Size(75, 23);
            this.btnAustinClear.TabIndex = 119;
            this.btnAustinClear.Text = "Clear";
            this.btnAustinClear.UseVisualStyleBackColor = true;
            this.btnAustinClear.Click += new System.EventHandler(this.btnAustinClear_Click);
            // 
            // austinTxtEmailTemplate
            // 
            this.austinTxtEmailTemplate.Font = new System.Drawing.Font("Arial Narrow", 11.25F);
            this.austinTxtEmailTemplate.Location = new System.Drawing.Point(802, 3);
            this.austinTxtEmailTemplate.Multiline = true;
            this.austinTxtEmailTemplate.Name = "austinTxtEmailTemplate";
            this.austinTxtEmailTemplate.ReadOnly = true;
            this.austinTxtEmailTemplate.Size = new System.Drawing.Size(205, 460);
            this.austinTxtEmailTemplate.TabIndex = 130;
            this.austinTxtEmailTemplate.TabStop = false;
            this.austinTxtEmailTemplate.Click += new System.EventHandler(this.austinTxtEmailTemplate_Click);
            // 
            // austinTxtKwTotal
            // 
            this.austinTxtKwTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtKwTotal.Location = new System.Drawing.Point(696, 200);
            this.austinTxtKwTotal.Name = "austinTxtKwTotal";
            this.austinTxtKwTotal.ReadOnly = true;
            this.austinTxtKwTotal.Size = new System.Drawing.Size(100, 20);
            this.austinTxtKwTotal.TabIndex = 129;
            this.austinTxtKwTotal.TabStop = false;
            // 
            // austinTxtKwArray4
            // 
            this.austinTxtKwArray4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtKwArray4.Location = new System.Drawing.Point(696, 86);
            this.austinTxtKwArray4.Name = "austinTxtKwArray4";
            this.austinTxtKwArray4.ReadOnly = true;
            this.austinTxtKwArray4.Size = new System.Drawing.Size(100, 20);
            this.austinTxtKwArray4.TabIndex = 109;
            this.austinTxtKwArray4.TabStop = false;
            // 
            // austinTxtKwArray1
            // 
            this.austinTxtKwArray1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtKwArray1.Location = new System.Drawing.Point(696, 8);
            this.austinTxtKwArray1.Name = "austinTxtKwArray1";
            this.austinTxtKwArray1.ReadOnly = true;
            this.austinTxtKwArray1.Size = new System.Drawing.Size(100, 20);
            this.austinTxtKwArray1.TabIndex = 110;
            this.austinTxtKwArray1.TabStop = false;
            // 
            // austinTxtKwArray2
            // 
            this.austinTxtKwArray2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtKwArray2.Location = new System.Drawing.Point(696, 34);
            this.austinTxtKwArray2.Name = "austinTxtKwArray2";
            this.austinTxtKwArray2.ReadOnly = true;
            this.austinTxtKwArray2.Size = new System.Drawing.Size(100, 20);
            this.austinTxtKwArray2.TabIndex = 111;
            this.austinTxtKwArray2.TabStop = false;
            // 
            // austinTxtKwArray3
            // 
            this.austinTxtKwArray3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtKwArray3.Location = new System.Drawing.Point(696, 60);
            this.austinTxtKwArray3.Name = "austinTxtKwArray3";
            this.austinTxtKwArray3.ReadOnly = true;
            this.austinTxtKwArray3.Size = new System.Drawing.Size(100, 20);
            this.austinTxtKwArray3.TabIndex = 108;
            this.austinTxtKwArray3.TabStop = false;
            // 
            // lblAustinPWCDown9
            // 
            this.lblAustinPWCDown9.AutoSize = true;
            this.lblAustinPWCDown9.Location = new System.Drawing.Point(696, 427);
            this.lblAustinPWCDown9.Name = "lblAustinPWCDown9";
            this.lblAustinPWCDown9.Size = new System.Drawing.Size(22, 13);
            this.lblAustinPWCDown9.TabIndex = 126;
            this.lblAustinPWCDown9.Text = "0.0";
            // 
            // lblAustinPWCDown8
            // 
            this.lblAustinPWCDown8.AutoSize = true;
            this.lblAustinPWCDown8.Location = new System.Drawing.Point(590, 427);
            this.lblAustinPWCDown8.Name = "lblAustinPWCDown8";
            this.lblAustinPWCDown8.Size = new System.Drawing.Size(22, 13);
            this.lblAustinPWCDown8.TabIndex = 128;
            this.lblAustinPWCDown8.Text = "0.0";
            // 
            // lblAustinPWCDown7
            // 
            this.lblAustinPWCDown7.AutoSize = true;
            this.lblAustinPWCDown7.Location = new System.Drawing.Point(484, 427);
            this.lblAustinPWCDown7.Name = "lblAustinPWCDown7";
            this.lblAustinPWCDown7.Size = new System.Drawing.Size(22, 13);
            this.lblAustinPWCDown7.TabIndex = 127;
            this.lblAustinPWCDown7.Text = "0.0";
            // 
            // lblAustinPWCDown6
            // 
            this.lblAustinPWCDown6.AutoSize = true;
            this.lblAustinPWCDown6.Location = new System.Drawing.Point(696, 375);
            this.lblAustinPWCDown6.Name = "lblAustinPWCDown6";
            this.lblAustinPWCDown6.Size = new System.Drawing.Size(22, 13);
            this.lblAustinPWCDown6.TabIndex = 125;
            this.lblAustinPWCDown6.Text = "0.0";
            // 
            // lblAustinPWCDown5
            // 
            this.lblAustinPWCDown5.AutoSize = true;
            this.lblAustinPWCDown5.Location = new System.Drawing.Point(590, 375);
            this.lblAustinPWCDown5.Name = "lblAustinPWCDown5";
            this.lblAustinPWCDown5.Size = new System.Drawing.Size(22, 13);
            this.lblAustinPWCDown5.TabIndex = 124;
            this.lblAustinPWCDown5.Text = "0.0";
            // 
            // lblAustinPWCDown4
            // 
            this.lblAustinPWCDown4.AutoSize = true;
            this.lblAustinPWCDown4.Location = new System.Drawing.Point(484, 375);
            this.lblAustinPWCDown4.Name = "lblAustinPWCDown4";
            this.lblAustinPWCDown4.Size = new System.Drawing.Size(22, 13);
            this.lblAustinPWCDown4.TabIndex = 123;
            this.lblAustinPWCDown4.Text = "0.0";
            // 
            // lblAustinPWCDown3
            // 
            this.lblAustinPWCDown3.AutoSize = true;
            this.lblAustinPWCDown3.Location = new System.Drawing.Point(696, 322);
            this.lblAustinPWCDown3.Name = "lblAustinPWCDown3";
            this.lblAustinPWCDown3.Size = new System.Drawing.Size(22, 13);
            this.lblAustinPWCDown3.TabIndex = 122;
            this.lblAustinPWCDown3.Text = "0.0";
            // 
            // lblAustinPWCDown2
            // 
            this.lblAustinPWCDown2.AutoSize = true;
            this.lblAustinPWCDown2.Location = new System.Drawing.Point(590, 322);
            this.lblAustinPWCDown2.Name = "lblAustinPWCDown2";
            this.lblAustinPWCDown2.Size = new System.Drawing.Size(22, 13);
            this.lblAustinPWCDown2.TabIndex = 121;
            this.lblAustinPWCDown2.Text = "0.0";
            // 
            // lblAustinPWCDown1
            // 
            this.lblAustinPWCDown1.AutoSize = true;
            this.lblAustinPWCDown1.Location = new System.Drawing.Point(484, 322);
            this.lblAustinPWCDown1.Name = "lblAustinPWCDown1";
            this.lblAustinPWCDown1.Size = new System.Drawing.Size(22, 13);
            this.lblAustinPWCDown1.TabIndex = 120;
            this.lblAustinPWCDown1.Text = "0.0";
            // 
            // austinTxtTotalSystemCost10
            // 
            this.austinTxtTotalSystemCost10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtTotalSystemCost10.Location = new System.Drawing.Point(696, 443);
            this.austinTxtTotalSystemCost10.Name = "austinTxtTotalSystemCost10";
            this.austinTxtTotalSystemCost10.ReadOnly = true;
            this.austinTxtTotalSystemCost10.Size = new System.Drawing.Size(100, 20);
            this.austinTxtTotalSystemCost10.TabIndex = 112;
            this.austinTxtTotalSystemCost10.TabStop = false;
            // 
            // austinTxtTotalSystemCost9
            // 
            this.austinTxtTotalSystemCost9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtTotalSystemCost9.Location = new System.Drawing.Point(590, 443);
            this.austinTxtTotalSystemCost9.Name = "austinTxtTotalSystemCost9";
            this.austinTxtTotalSystemCost9.ReadOnly = true;
            this.austinTxtTotalSystemCost9.Size = new System.Drawing.Size(100, 20);
            this.austinTxtTotalSystemCost9.TabIndex = 113;
            this.austinTxtTotalSystemCost9.TabStop = false;
            // 
            // austinTxtTotalSystemCost8
            // 
            this.austinTxtTotalSystemCost8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtTotalSystemCost8.Location = new System.Drawing.Point(484, 443);
            this.austinTxtTotalSystemCost8.Name = "austinTxtTotalSystemCost8";
            this.austinTxtTotalSystemCost8.ReadOnly = true;
            this.austinTxtTotalSystemCost8.Size = new System.Drawing.Size(100, 20);
            this.austinTxtTotalSystemCost8.TabIndex = 114;
            this.austinTxtTotalSystemCost8.TabStop = false;
            // 
            // austinTxtTotalSystemCost7
            // 
            this.austinTxtTotalSystemCost7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtTotalSystemCost7.Location = new System.Drawing.Point(696, 391);
            this.austinTxtTotalSystemCost7.Name = "austinTxtTotalSystemCost7";
            this.austinTxtTotalSystemCost7.ReadOnly = true;
            this.austinTxtTotalSystemCost7.Size = new System.Drawing.Size(100, 20);
            this.austinTxtTotalSystemCost7.TabIndex = 106;
            this.austinTxtTotalSystemCost7.TabStop = false;
            // 
            // austinTxtTotalSystemCost2
            // 
            this.austinTxtTotalSystemCost2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtTotalSystemCost2.Location = new System.Drawing.Point(484, 338);
            this.austinTxtTotalSystemCost2.Name = "austinTxtTotalSystemCost2";
            this.austinTxtTotalSystemCost2.ReadOnly = true;
            this.austinTxtTotalSystemCost2.Size = new System.Drawing.Size(100, 20);
            this.austinTxtTotalSystemCost2.TabIndex = 105;
            this.austinTxtTotalSystemCost2.TabStop = false;
            // 
            // austinTxtTotalSystemCost5
            // 
            this.austinTxtTotalSystemCost5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtTotalSystemCost5.Location = new System.Drawing.Point(484, 391);
            this.austinTxtTotalSystemCost5.Name = "austinTxtTotalSystemCost5";
            this.austinTxtTotalSystemCost5.ReadOnly = true;
            this.austinTxtTotalSystemCost5.Size = new System.Drawing.Size(100, 20);
            this.austinTxtTotalSystemCost5.TabIndex = 92;
            this.austinTxtTotalSystemCost5.TabStop = false;
            // 
            // austinTxtTotalSystemCost6
            // 
            this.austinTxtTotalSystemCost6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtTotalSystemCost6.Location = new System.Drawing.Point(590, 391);
            this.austinTxtTotalSystemCost6.Name = "austinTxtTotalSystemCost6";
            this.austinTxtTotalSystemCost6.ReadOnly = true;
            this.austinTxtTotalSystemCost6.Size = new System.Drawing.Size(100, 20);
            this.austinTxtTotalSystemCost6.TabIndex = 104;
            this.austinTxtTotalSystemCost6.TabStop = false;
            // 
            // austinTxtTotalSystemCost3
            // 
            this.austinTxtTotalSystemCost3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtTotalSystemCost3.Location = new System.Drawing.Point(590, 338);
            this.austinTxtTotalSystemCost3.Name = "austinTxtTotalSystemCost3";
            this.austinTxtTotalSystemCost3.ReadOnly = true;
            this.austinTxtTotalSystemCost3.Size = new System.Drawing.Size(100, 20);
            this.austinTxtTotalSystemCost3.TabIndex = 103;
            this.austinTxtTotalSystemCost3.TabStop = false;
            // 
            // austinTxtTotalSystemCost4
            // 
            this.austinTxtTotalSystemCost4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtTotalSystemCost4.Location = new System.Drawing.Point(696, 338);
            this.austinTxtTotalSystemCost4.Name = "austinTxtTotalSystemCost4";
            this.austinTxtTotalSystemCost4.ReadOnly = true;
            this.austinTxtTotalSystemCost4.Size = new System.Drawing.Size(100, 20);
            this.austinTxtTotalSystemCost4.TabIndex = 102;
            this.austinTxtTotalSystemCost4.TabStop = false;
            // 
            // lblAustinPWC
            // 
            this.lblAustinPWC.AutoSize = true;
            this.lblAustinPWC.Location = new System.Drawing.Point(590, 116);
            this.lblAustinPWC.Name = "lblAustinPWC";
            this.lblAustinPWC.Size = new System.Drawing.Size(73, 13);
            this.lblAustinPWC.TabIndex = 101;
            this.lblAustinPWC.Text = "Per Watt Cost";
            // 
            // austinTxtPWC1
            // 
            this.austinTxtPWC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtPWC1.Location = new System.Drawing.Point(484, 112);
            this.austinTxtPWC1.Name = "austinTxtPWC1";
            this.austinTxtPWC1.ReadOnly = true;
            this.austinTxtPWC1.Size = new System.Drawing.Size(100, 20);
            this.austinTxtPWC1.TabIndex = 100;
            this.austinTxtPWC1.TabStop = false;
            this.austinTxtPWC1.Text = "3.70";
            // 
            // btnAustinCalc
            // 
            this.btnAustinCalc.Location = new System.Drawing.Point(484, 228);
            this.btnAustinCalc.Name = "btnAustinCalc";
            this.btnAustinCalc.Size = new System.Drawing.Size(100, 23);
            this.btnAustinCalc.TabIndex = 118;
            this.btnAustinCalc.Text = "Calculate";
            this.btnAustinCalc.UseVisualStyleBackColor = true;
            this.btnAustinCalc.Click += new System.EventHandler(this.btnAustinCalc_Click);
            // 
            // lblAustinTotalSystemCost
            // 
            this.lblAustinTotalSystemCost.AutoSize = true;
            this.lblAustinTotalSystemCost.Location = new System.Drawing.Point(590, 205);
            this.lblAustinTotalSystemCost.Name = "lblAustinTotalSystemCost";
            this.lblAustinTotalSystemCost.Size = new System.Drawing.Size(92, 13);
            this.lblAustinTotalSystemCost.TabIndex = 99;
            this.lblAustinTotalSystemCost.Text = "Total System Cost";
            // 
            // austinTxtTotalSystemCost1
            // 
            this.austinTxtTotalSystemCost1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtTotalSystemCost1.Location = new System.Drawing.Point(484, 202);
            this.austinTxtTotalSystemCost1.Name = "austinTxtTotalSystemCost1";
            this.austinTxtTotalSystemCost1.ReadOnly = true;
            this.austinTxtTotalSystemCost1.Size = new System.Drawing.Size(100, 20);
            this.austinTxtTotalSystemCost1.TabIndex = 98;
            this.austinTxtTotalSystemCost1.TabStop = false;
            // 
            // lblAustinArray4
            // 
            this.lblAustinArray4.AutoSize = true;
            this.lblAustinArray4.Location = new System.Drawing.Point(590, 89);
            this.lblAustinArray4.Name = "lblAustinArray4";
            this.lblAustinArray4.Size = new System.Drawing.Size(75, 13);
            this.lblAustinArray4.TabIndex = 97;
            this.lblAustinArray4.Text = "Array 4 (North)";
            // 
            // lblAustinArray3
            // 
            this.lblAustinArray3.AutoSize = true;
            this.lblAustinArray3.Location = new System.Drawing.Point(590, 63);
            this.lblAustinArray3.Name = "lblAustinArray3";
            this.lblAustinArray3.Size = new System.Drawing.Size(70, 13);
            this.lblAustinArray3.TabIndex = 96;
            this.lblAustinArray3.Text = "Array 3 (East)";
            // 
            // lblAustinArray2
            // 
            this.lblAustinArray2.AutoSize = true;
            this.lblAustinArray2.Location = new System.Drawing.Point(590, 37);
            this.lblAustinArray2.Name = "lblAustinArray2";
            this.lblAustinArray2.Size = new System.Drawing.Size(74, 13);
            this.lblAustinArray2.TabIndex = 95;
            this.lblAustinArray2.Text = "Array 2 (West)";
            // 
            // lblAustinArray1
            // 
            this.lblAustinArray1.AutoSize = true;
            this.lblAustinArray1.Location = new System.Drawing.Point(590, 11);
            this.lblAustinArray1.Name = "lblAustinArray1";
            this.lblAustinArray1.Size = new System.Drawing.Size(77, 13);
            this.lblAustinArray1.TabIndex = 94;
            this.lblAustinArray1.Text = "Array 1 (South)";
            // 
            // austinTxtArray1
            // 
            this.austinTxtArray1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtArray1.Location = new System.Drawing.Point(484, 8);
            this.austinTxtArray1.Name = "austinTxtArray1";
            this.austinTxtArray1.Size = new System.Drawing.Size(100, 20);
            this.austinTxtArray1.TabIndex = 93;
            this.austinTxtArray1.Text = "0";
            // 
            // austinTxtArray2
            // 
            this.austinTxtArray2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtArray2.Location = new System.Drawing.Point(484, 34);
            this.austinTxtArray2.Name = "austinTxtArray2";
            this.austinTxtArray2.Size = new System.Drawing.Size(100, 20);
            this.austinTxtArray2.TabIndex = 115;
            this.austinTxtArray2.Text = "0";
            // 
            // austinTxtArray3
            // 
            this.austinTxtArray3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtArray3.Location = new System.Drawing.Point(484, 60);
            this.austinTxtArray3.Name = "austinTxtArray3";
            this.austinTxtArray3.Size = new System.Drawing.Size(100, 20);
            this.austinTxtArray3.TabIndex = 116;
            this.austinTxtArray3.Text = "0";
            // 
            // austinTxtArray4
            // 
            this.austinTxtArray4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.austinTxtArray4.Location = new System.Drawing.Point(484, 86);
            this.austinTxtArray4.Name = "austinTxtArray4";
            this.austinTxtArray4.Size = new System.Drawing.Size(100, 20);
            this.austinTxtArray4.TabIndex = 117;
            this.austinTxtArray4.Text = "0";
            // 
            // tabTexasSanAntonio
            // 
            this.tabTexasSanAntonio.Controls.Add(this.webBrowser5);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioChkCashBtn);
            this.tabTexasSanAntonio.Controls.Add(this.btnSanAntonioClear);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtEmailInformation);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtKWTotal);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtKWArray4);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtKWArray1);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtKWArray2);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtKWArray3);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioLblPwcDown9);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioLblPwcDown8);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioLblPwcDown7);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioLblPwcDown6);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioLblPwcDown5);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioLblPwcDown4);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioLblPwcDown3);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioLblPwcDown2);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioLblPwcDown1);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtTotalSystemCost10);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtTotalSystemCost9);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtTotalSystemCost8);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtTotalSystemCost7);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtTotalSystemCost2);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtTotalSystemCost5);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtTotalSystemCost6);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtTotalSystemCost3);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtTotalSystemCost4);
            this.tabTexasSanAntonio.Controls.Add(this.label22);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtPWC);
            this.tabTexasSanAntonio.Controls.Add(this.btnSanAntonioCalc);
            this.tabTexasSanAntonio.Controls.Add(this.label23);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtTotalSystemCost1);
            this.tabTexasSanAntonio.Controls.Add(this.label24);
            this.tabTexasSanAntonio.Controls.Add(this.label25);
            this.tabTexasSanAntonio.Controls.Add(this.label26);
            this.tabTexasSanAntonio.Controls.Add(this.label27);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtArray1);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtArray2);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtArray3);
            this.tabTexasSanAntonio.Controls.Add(this.sanAntonioTxtArray4);
            this.tabTexasSanAntonio.Location = new System.Drawing.Point(4, 22);
            this.tabTexasSanAntonio.Name = "tabTexasSanAntonio";
            this.tabTexasSanAntonio.Size = new System.Drawing.Size(1010, 472);
            this.tabTexasSanAntonio.TabIndex = 5;
            this.tabTexasSanAntonio.Text = "Texas - San Antonio";
            this.tabTexasSanAntonio.UseVisualStyleBackColor = true;
            // 
            // webBrowser5
            // 
            this.webBrowser5.Location = new System.Drawing.Point(3, 3);
            this.webBrowser5.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser5.Name = "webBrowser5";
            this.webBrowser5.Size = new System.Drawing.Size(475, 460);
            this.webBrowser5.TabIndex = 170;
            this.webBrowser5.Url = new System.Uri(resources.GetString("webBrowser5.Url"), System.UriKind.Absolute);
            // 
            // sanAntonioChkCashBtn
            // 
            this.sanAntonioChkCashBtn.AutoSize = true;
            this.sanAntonioChkCashBtn.Location = new System.Drawing.Point(484, 138);
            this.sanAntonioChkCashBtn.Name = "sanAntonioChkCashBtn";
            this.sanAntonioChkCashBtn.Size = new System.Drawing.Size(50, 17);
            this.sanAntonioChkCashBtn.TabIndex = 3;
            this.sanAntonioChkCashBtn.Text = "Cash";
            this.sanAntonioChkCashBtn.UseVisualStyleBackColor = true;
            // 
            // btnSanAntonioClear
            // 
            this.btnSanAntonioClear.Location = new System.Drawing.Point(590, 228);
            this.btnSanAntonioClear.Name = "btnSanAntonioClear";
            this.btnSanAntonioClear.Size = new System.Drawing.Size(75, 23);
            this.btnSanAntonioClear.TabIndex = 158;
            this.btnSanAntonioClear.Text = "Clear";
            this.btnSanAntonioClear.UseVisualStyleBackColor = true;
            this.btnSanAntonioClear.Click += new System.EventHandler(this.btnSanAntonioClear_Click);
            // 
            // sanAntonioTxtEmailInformation
            // 
            this.sanAntonioTxtEmailInformation.Font = new System.Drawing.Font("Arial Narrow", 11.25F);
            this.sanAntonioTxtEmailInformation.Location = new System.Drawing.Point(802, 3);
            this.sanAntonioTxtEmailInformation.Multiline = true;
            this.sanAntonioTxtEmailInformation.Name = "sanAntonioTxtEmailInformation";
            this.sanAntonioTxtEmailInformation.ReadOnly = true;
            this.sanAntonioTxtEmailInformation.Size = new System.Drawing.Size(205, 460);
            this.sanAntonioTxtEmailInformation.TabIndex = 169;
            this.sanAntonioTxtEmailInformation.TabStop = false;
            this.sanAntonioTxtEmailInformation.Click += new System.EventHandler(this.sanAntonioTxtEmailInformation_Click);
            // 
            // sanAntonioTxtKWTotal
            // 
            this.sanAntonioTxtKWTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtKWTotal.Location = new System.Drawing.Point(696, 200);
            this.sanAntonioTxtKWTotal.Name = "sanAntonioTxtKWTotal";
            this.sanAntonioTxtKWTotal.ReadOnly = true;
            this.sanAntonioTxtKWTotal.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtKWTotal.TabIndex = 168;
            this.sanAntonioTxtKWTotal.TabStop = false;
            // 
            // sanAntonioTxtKWArray4
            // 
            this.sanAntonioTxtKWArray4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtKWArray4.Location = new System.Drawing.Point(696, 86);
            this.sanAntonioTxtKWArray4.Name = "sanAntonioTxtKWArray4";
            this.sanAntonioTxtKWArray4.ReadOnly = true;
            this.sanAntonioTxtKWArray4.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtKWArray4.TabIndex = 148;
            this.sanAntonioTxtKWArray4.TabStop = false;
            // 
            // sanAntonioTxtKWArray1
            // 
            this.sanAntonioTxtKWArray1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtKWArray1.Location = new System.Drawing.Point(696, 8);
            this.sanAntonioTxtKWArray1.Name = "sanAntonioTxtKWArray1";
            this.sanAntonioTxtKWArray1.ReadOnly = true;
            this.sanAntonioTxtKWArray1.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtKWArray1.TabIndex = 149;
            this.sanAntonioTxtKWArray1.TabStop = false;
            // 
            // sanAntonioTxtKWArray2
            // 
            this.sanAntonioTxtKWArray2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtKWArray2.Location = new System.Drawing.Point(696, 34);
            this.sanAntonioTxtKWArray2.Name = "sanAntonioTxtKWArray2";
            this.sanAntonioTxtKWArray2.ReadOnly = true;
            this.sanAntonioTxtKWArray2.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtKWArray2.TabIndex = 150;
            this.sanAntonioTxtKWArray2.TabStop = false;
            // 
            // sanAntonioTxtKWArray3
            // 
            this.sanAntonioTxtKWArray3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtKWArray3.Location = new System.Drawing.Point(696, 60);
            this.sanAntonioTxtKWArray3.Name = "sanAntonioTxtKWArray3";
            this.sanAntonioTxtKWArray3.ReadOnly = true;
            this.sanAntonioTxtKWArray3.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtKWArray3.TabIndex = 147;
            this.sanAntonioTxtKWArray3.TabStop = false;
            // 
            // sanAntonioLblPwcDown9
            // 
            this.sanAntonioLblPwcDown9.AutoSize = true;
            this.sanAntonioLblPwcDown9.Location = new System.Drawing.Point(696, 427);
            this.sanAntonioLblPwcDown9.Name = "sanAntonioLblPwcDown9";
            this.sanAntonioLblPwcDown9.Size = new System.Drawing.Size(22, 13);
            this.sanAntonioLblPwcDown9.TabIndex = 165;
            this.sanAntonioLblPwcDown9.Text = "0.0";
            // 
            // sanAntonioLblPwcDown8
            // 
            this.sanAntonioLblPwcDown8.AutoSize = true;
            this.sanAntonioLblPwcDown8.Location = new System.Drawing.Point(590, 427);
            this.sanAntonioLblPwcDown8.Name = "sanAntonioLblPwcDown8";
            this.sanAntonioLblPwcDown8.Size = new System.Drawing.Size(22, 13);
            this.sanAntonioLblPwcDown8.TabIndex = 167;
            this.sanAntonioLblPwcDown8.Text = "0.0";
            // 
            // sanAntonioLblPwcDown7
            // 
            this.sanAntonioLblPwcDown7.AutoSize = true;
            this.sanAntonioLblPwcDown7.Location = new System.Drawing.Point(484, 427);
            this.sanAntonioLblPwcDown7.Name = "sanAntonioLblPwcDown7";
            this.sanAntonioLblPwcDown7.Size = new System.Drawing.Size(22, 13);
            this.sanAntonioLblPwcDown7.TabIndex = 166;
            this.sanAntonioLblPwcDown7.Text = "0.0";
            // 
            // sanAntonioLblPwcDown6
            // 
            this.sanAntonioLblPwcDown6.AutoSize = true;
            this.sanAntonioLblPwcDown6.Location = new System.Drawing.Point(696, 375);
            this.sanAntonioLblPwcDown6.Name = "sanAntonioLblPwcDown6";
            this.sanAntonioLblPwcDown6.Size = new System.Drawing.Size(22, 13);
            this.sanAntonioLblPwcDown6.TabIndex = 164;
            this.sanAntonioLblPwcDown6.Text = "0.0";
            // 
            // sanAntonioLblPwcDown5
            // 
            this.sanAntonioLblPwcDown5.AutoSize = true;
            this.sanAntonioLblPwcDown5.Location = new System.Drawing.Point(590, 375);
            this.sanAntonioLblPwcDown5.Name = "sanAntonioLblPwcDown5";
            this.sanAntonioLblPwcDown5.Size = new System.Drawing.Size(22, 13);
            this.sanAntonioLblPwcDown5.TabIndex = 163;
            this.sanAntonioLblPwcDown5.Text = "0.0";
            // 
            // sanAntonioLblPwcDown4
            // 
            this.sanAntonioLblPwcDown4.AutoSize = true;
            this.sanAntonioLblPwcDown4.Location = new System.Drawing.Point(484, 375);
            this.sanAntonioLblPwcDown4.Name = "sanAntonioLblPwcDown4";
            this.sanAntonioLblPwcDown4.Size = new System.Drawing.Size(22, 13);
            this.sanAntonioLblPwcDown4.TabIndex = 162;
            this.sanAntonioLblPwcDown4.Text = "0.0";
            // 
            // sanAntonioLblPwcDown3
            // 
            this.sanAntonioLblPwcDown3.AutoSize = true;
            this.sanAntonioLblPwcDown3.Location = new System.Drawing.Point(696, 322);
            this.sanAntonioLblPwcDown3.Name = "sanAntonioLblPwcDown3";
            this.sanAntonioLblPwcDown3.Size = new System.Drawing.Size(22, 13);
            this.sanAntonioLblPwcDown3.TabIndex = 161;
            this.sanAntonioLblPwcDown3.Text = "0.0";
            // 
            // sanAntonioLblPwcDown2
            // 
            this.sanAntonioLblPwcDown2.AutoSize = true;
            this.sanAntonioLblPwcDown2.Location = new System.Drawing.Point(590, 322);
            this.sanAntonioLblPwcDown2.Name = "sanAntonioLblPwcDown2";
            this.sanAntonioLblPwcDown2.Size = new System.Drawing.Size(22, 13);
            this.sanAntonioLblPwcDown2.TabIndex = 160;
            this.sanAntonioLblPwcDown2.Text = "0.0";
            // 
            // sanAntonioLblPwcDown1
            // 
            this.sanAntonioLblPwcDown1.AutoSize = true;
            this.sanAntonioLblPwcDown1.Location = new System.Drawing.Point(484, 322);
            this.sanAntonioLblPwcDown1.Name = "sanAntonioLblPwcDown1";
            this.sanAntonioLblPwcDown1.Size = new System.Drawing.Size(22, 13);
            this.sanAntonioLblPwcDown1.TabIndex = 159;
            this.sanAntonioLblPwcDown1.Text = "0.0";
            // 
            // sanAntonioTxtTotalSystemCost10
            // 
            this.sanAntonioTxtTotalSystemCost10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtTotalSystemCost10.Location = new System.Drawing.Point(696, 443);
            this.sanAntonioTxtTotalSystemCost10.Name = "sanAntonioTxtTotalSystemCost10";
            this.sanAntonioTxtTotalSystemCost10.ReadOnly = true;
            this.sanAntonioTxtTotalSystemCost10.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtTotalSystemCost10.TabIndex = 151;
            this.sanAntonioTxtTotalSystemCost10.TabStop = false;
            // 
            // sanAntonioTxtTotalSystemCost9
            // 
            this.sanAntonioTxtTotalSystemCost9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtTotalSystemCost9.Location = new System.Drawing.Point(590, 443);
            this.sanAntonioTxtTotalSystemCost9.Name = "sanAntonioTxtTotalSystemCost9";
            this.sanAntonioTxtTotalSystemCost9.ReadOnly = true;
            this.sanAntonioTxtTotalSystemCost9.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtTotalSystemCost9.TabIndex = 152;
            this.sanAntonioTxtTotalSystemCost9.TabStop = false;
            // 
            // sanAntonioTxtTotalSystemCost8
            // 
            this.sanAntonioTxtTotalSystemCost8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtTotalSystemCost8.Location = new System.Drawing.Point(484, 443);
            this.sanAntonioTxtTotalSystemCost8.Name = "sanAntonioTxtTotalSystemCost8";
            this.sanAntonioTxtTotalSystemCost8.ReadOnly = true;
            this.sanAntonioTxtTotalSystemCost8.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtTotalSystemCost8.TabIndex = 153;
            this.sanAntonioTxtTotalSystemCost8.TabStop = false;
            // 
            // sanAntonioTxtTotalSystemCost7
            // 
            this.sanAntonioTxtTotalSystemCost7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtTotalSystemCost7.Location = new System.Drawing.Point(696, 391);
            this.sanAntonioTxtTotalSystemCost7.Name = "sanAntonioTxtTotalSystemCost7";
            this.sanAntonioTxtTotalSystemCost7.ReadOnly = true;
            this.sanAntonioTxtTotalSystemCost7.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtTotalSystemCost7.TabIndex = 145;
            this.sanAntonioTxtTotalSystemCost7.TabStop = false;
            // 
            // sanAntonioTxtTotalSystemCost2
            // 
            this.sanAntonioTxtTotalSystemCost2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtTotalSystemCost2.Location = new System.Drawing.Point(484, 338);
            this.sanAntonioTxtTotalSystemCost2.Name = "sanAntonioTxtTotalSystemCost2";
            this.sanAntonioTxtTotalSystemCost2.ReadOnly = true;
            this.sanAntonioTxtTotalSystemCost2.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtTotalSystemCost2.TabIndex = 144;
            this.sanAntonioTxtTotalSystemCost2.TabStop = false;
            // 
            // sanAntonioTxtTotalSystemCost5
            // 
            this.sanAntonioTxtTotalSystemCost5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtTotalSystemCost5.Location = new System.Drawing.Point(484, 391);
            this.sanAntonioTxtTotalSystemCost5.Name = "sanAntonioTxtTotalSystemCost5";
            this.sanAntonioTxtTotalSystemCost5.ReadOnly = true;
            this.sanAntonioTxtTotalSystemCost5.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtTotalSystemCost5.TabIndex = 131;
            this.sanAntonioTxtTotalSystemCost5.TabStop = false;
            // 
            // sanAntonioTxtTotalSystemCost6
            // 
            this.sanAntonioTxtTotalSystemCost6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtTotalSystemCost6.Location = new System.Drawing.Point(590, 391);
            this.sanAntonioTxtTotalSystemCost6.Name = "sanAntonioTxtTotalSystemCost6";
            this.sanAntonioTxtTotalSystemCost6.ReadOnly = true;
            this.sanAntonioTxtTotalSystemCost6.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtTotalSystemCost6.TabIndex = 143;
            this.sanAntonioTxtTotalSystemCost6.TabStop = false;
            // 
            // sanAntonioTxtTotalSystemCost3
            // 
            this.sanAntonioTxtTotalSystemCost3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtTotalSystemCost3.Location = new System.Drawing.Point(590, 338);
            this.sanAntonioTxtTotalSystemCost3.Name = "sanAntonioTxtTotalSystemCost3";
            this.sanAntonioTxtTotalSystemCost3.ReadOnly = true;
            this.sanAntonioTxtTotalSystemCost3.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtTotalSystemCost3.TabIndex = 142;
            this.sanAntonioTxtTotalSystemCost3.TabStop = false;
            // 
            // sanAntonioTxtTotalSystemCost4
            // 
            this.sanAntonioTxtTotalSystemCost4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtTotalSystemCost4.Location = new System.Drawing.Point(696, 338);
            this.sanAntonioTxtTotalSystemCost4.Name = "sanAntonioTxtTotalSystemCost4";
            this.sanAntonioTxtTotalSystemCost4.ReadOnly = true;
            this.sanAntonioTxtTotalSystemCost4.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtTotalSystemCost4.TabIndex = 141;
            this.sanAntonioTxtTotalSystemCost4.TabStop = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(590, 116);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(73, 13);
            this.label22.TabIndex = 140;
            this.label22.Text = "Per Watt Cost";
            // 
            // sanAntonioTxtPWC
            // 
            this.sanAntonioTxtPWC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtPWC.Location = new System.Drawing.Point(484, 112);
            this.sanAntonioTxtPWC.Name = "sanAntonioTxtPWC";
            this.sanAntonioTxtPWC.ReadOnly = true;
            this.sanAntonioTxtPWC.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtPWC.TabIndex = 139;
            this.sanAntonioTxtPWC.TabStop = false;
            this.sanAntonioTxtPWC.Text = "3.70";
            // 
            // btnSanAntonioCalc
            // 
            this.btnSanAntonioCalc.Location = new System.Drawing.Point(484, 228);
            this.btnSanAntonioCalc.Name = "btnSanAntonioCalc";
            this.btnSanAntonioCalc.Size = new System.Drawing.Size(100, 23);
            this.btnSanAntonioCalc.TabIndex = 157;
            this.btnSanAntonioCalc.Text = "Calculate";
            this.btnSanAntonioCalc.UseVisualStyleBackColor = true;
            this.btnSanAntonioCalc.Click += new System.EventHandler(this.btnSanAntonioCalc_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(590, 205);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(92, 13);
            this.label23.TabIndex = 138;
            this.label23.Text = "Total System Cost";
            // 
            // sanAntonioTxtTotalSystemCost1
            // 
            this.sanAntonioTxtTotalSystemCost1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtTotalSystemCost1.Location = new System.Drawing.Point(484, 202);
            this.sanAntonioTxtTotalSystemCost1.Name = "sanAntonioTxtTotalSystemCost1";
            this.sanAntonioTxtTotalSystemCost1.ReadOnly = true;
            this.sanAntonioTxtTotalSystemCost1.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtTotalSystemCost1.TabIndex = 137;
            this.sanAntonioTxtTotalSystemCost1.TabStop = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(590, 89);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(75, 13);
            this.label24.TabIndex = 136;
            this.label24.Text = "Array 4 (North)";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(590, 63);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(70, 13);
            this.label25.TabIndex = 135;
            this.label25.Text = "Array 3 (East)";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(590, 37);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(74, 13);
            this.label26.TabIndex = 134;
            this.label26.Text = "Array 2 (West)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(590, 11);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(77, 13);
            this.label27.TabIndex = 133;
            this.label27.Text = "Array 1 (South)";
            // 
            // sanAntonioTxtArray1
            // 
            this.sanAntonioTxtArray1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtArray1.Location = new System.Drawing.Point(484, 8);
            this.sanAntonioTxtArray1.Name = "sanAntonioTxtArray1";
            this.sanAntonioTxtArray1.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtArray1.TabIndex = 132;
            this.sanAntonioTxtArray1.Text = "0";
            // 
            // sanAntonioTxtArray2
            // 
            this.sanAntonioTxtArray2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtArray2.Location = new System.Drawing.Point(484, 34);
            this.sanAntonioTxtArray2.Name = "sanAntonioTxtArray2";
            this.sanAntonioTxtArray2.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtArray2.TabIndex = 154;
            this.sanAntonioTxtArray2.Text = "0";
            // 
            // sanAntonioTxtArray3
            // 
            this.sanAntonioTxtArray3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtArray3.Location = new System.Drawing.Point(484, 60);
            this.sanAntonioTxtArray3.Name = "sanAntonioTxtArray3";
            this.sanAntonioTxtArray3.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtArray3.TabIndex = 155;
            this.sanAntonioTxtArray3.Text = "0";
            // 
            // sanAntonioTxtArray4
            // 
            this.sanAntonioTxtArray4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sanAntonioTxtArray4.Location = new System.Drawing.Point(484, 86);
            this.sanAntonioTxtArray4.Name = "sanAntonioTxtArray4";
            this.sanAntonioTxtArray4.Size = new System.Drawing.Size(100, 20);
            this.sanAntonioTxtArray4.TabIndex = 156;
            this.sanAntonioTxtArray4.Text = "0";
            // 
            // tabVegas
            // 
            this.tabVegas.Controls.Add(this.webBrowser6);
            this.tabVegas.Controls.Add(this.nvChkCash);
            this.tabVegas.Controls.Add(this.btnClearNV);
            this.tabVegas.Controls.Add(this.nvEmailTemplateTxt);
            this.tabVegas.Controls.Add(this.nvTotalKwTxt);
            this.tabVegas.Controls.Add(this.nvkWArray4Txt);
            this.tabVegas.Controls.Add(this.nvkWArray1Txt);
            this.tabVegas.Controls.Add(this.nvkWArray2Txt);
            this.tabVegas.Controls.Add(this.nvkWArray3Txt);
            this.tabVegas.Controls.Add(this.nvLblDown9);
            this.tabVegas.Controls.Add(this.nvLblDown8);
            this.tabVegas.Controls.Add(this.nvLblDown7);
            this.tabVegas.Controls.Add(this.nvLblDown6);
            this.tabVegas.Controls.Add(this.nvLblDown5);
            this.tabVegas.Controls.Add(this.nvLblDown4);
            this.tabVegas.Controls.Add(this.nvLblDown3);
            this.tabVegas.Controls.Add(this.nvLblDown2);
            this.tabVegas.Controls.Add(this.nvLblDown1);
            this.tabVegas.Controls.Add(this.nvTxtDown9);
            this.tabVegas.Controls.Add(this.nvTxtDown8);
            this.tabVegas.Controls.Add(this.nvTxtDown7);
            this.tabVegas.Controls.Add(this.nvTxtDown6);
            this.tabVegas.Controls.Add(this.nvTxtDown1);
            this.tabVegas.Controls.Add(this.nvTxtDown4);
            this.tabVegas.Controls.Add(this.nvTxtDown5);
            this.tabVegas.Controls.Add(this.nvTxtDown2);
            this.tabVegas.Controls.Add(this.nvTxtDown3);
            this.tabVegas.Controls.Add(this.label28);
            this.tabVegas.Controls.Add(this.nvPwcTxt);
            this.tabVegas.Controls.Add(this.btnCalcNV);
            this.tabVegas.Controls.Add(this.label29);
            this.tabVegas.Controls.Add(this.nvTotalSystemCostTxt);
            this.tabVegas.Controls.Add(this.label30);
            this.tabVegas.Controls.Add(this.label31);
            this.tabVegas.Controls.Add(this.label32);
            this.tabVegas.Controls.Add(this.label33);
            this.tabVegas.Controls.Add(this.nvArray1Txt);
            this.tabVegas.Controls.Add(this.nvArray2Txt);
            this.tabVegas.Controls.Add(this.nvArray3Txt);
            this.tabVegas.Controls.Add(this.nvArray4Txt);
            this.tabVegas.Location = new System.Drawing.Point(4, 22);
            this.tabVegas.Name = "tabVegas";
            this.tabVegas.Size = new System.Drawing.Size(1010, 472);
            this.tabVegas.TabIndex = 6;
            this.tabVegas.Text = "Nevada";
            this.tabVegas.UseVisualStyleBackColor = true;
            // 
            // webBrowser6
            // 
            this.webBrowser6.Location = new System.Drawing.Point(3, 3);
            this.webBrowser6.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser6.Name = "webBrowser6";
            this.webBrowser6.Size = new System.Drawing.Size(475, 460);
            this.webBrowser6.TabIndex = 210;
            this.webBrowser6.Url = new System.Uri(resources.GetString("webBrowser6.Url"), System.UriKind.Absolute);
            // 
            // nvChkCash
            // 
            this.nvChkCash.AutoSize = true;
            this.nvChkCash.Location = new System.Drawing.Point(484, 138);
            this.nvChkCash.Name = "nvChkCash";
            this.nvChkCash.Size = new System.Drawing.Size(50, 17);
            this.nvChkCash.TabIndex = 171;
            this.nvChkCash.Text = "Cash";
            this.nvChkCash.UseVisualStyleBackColor = true;
            // 
            // btnClearNV
            // 
            this.btnClearNV.Location = new System.Drawing.Point(590, 228);
            this.btnClearNV.Name = "btnClearNV";
            this.btnClearNV.Size = new System.Drawing.Size(75, 23);
            this.btnClearNV.TabIndex = 198;
            this.btnClearNV.Text = "Clear";
            this.btnClearNV.UseVisualStyleBackColor = true;
            // 
            // nvEmailTemplateTxt
            // 
            this.nvEmailTemplateTxt.Font = new System.Drawing.Font("Arial Narrow", 11.25F);
            this.nvEmailTemplateTxt.Location = new System.Drawing.Point(802, 3);
            this.nvEmailTemplateTxt.Multiline = true;
            this.nvEmailTemplateTxt.Name = "nvEmailTemplateTxt";
            this.nvEmailTemplateTxt.ReadOnly = true;
            this.nvEmailTemplateTxt.Size = new System.Drawing.Size(205, 460);
            this.nvEmailTemplateTxt.TabIndex = 209;
            this.nvEmailTemplateTxt.TabStop = false;
            // 
            // nvTotalKwTxt
            // 
            this.nvTotalKwTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvTotalKwTxt.Location = new System.Drawing.Point(696, 200);
            this.nvTotalKwTxt.Name = "nvTotalKwTxt";
            this.nvTotalKwTxt.ReadOnly = true;
            this.nvTotalKwTxt.Size = new System.Drawing.Size(100, 20);
            this.nvTotalKwTxt.TabIndex = 208;
            this.nvTotalKwTxt.TabStop = false;
            // 
            // nvkWArray4Txt
            // 
            this.nvkWArray4Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvkWArray4Txt.Location = new System.Drawing.Point(696, 86);
            this.nvkWArray4Txt.Name = "nvkWArray4Txt";
            this.nvkWArray4Txt.ReadOnly = true;
            this.nvkWArray4Txt.Size = new System.Drawing.Size(100, 20);
            this.nvkWArray4Txt.TabIndex = 188;
            this.nvkWArray4Txt.TabStop = false;
            // 
            // nvkWArray1Txt
            // 
            this.nvkWArray1Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvkWArray1Txt.Location = new System.Drawing.Point(696, 8);
            this.nvkWArray1Txt.Name = "nvkWArray1Txt";
            this.nvkWArray1Txt.ReadOnly = true;
            this.nvkWArray1Txt.Size = new System.Drawing.Size(100, 20);
            this.nvkWArray1Txt.TabIndex = 189;
            this.nvkWArray1Txt.TabStop = false;
            // 
            // nvkWArray2Txt
            // 
            this.nvkWArray2Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvkWArray2Txt.Location = new System.Drawing.Point(696, 34);
            this.nvkWArray2Txt.Name = "nvkWArray2Txt";
            this.nvkWArray2Txt.ReadOnly = true;
            this.nvkWArray2Txt.Size = new System.Drawing.Size(100, 20);
            this.nvkWArray2Txt.TabIndex = 190;
            this.nvkWArray2Txt.TabStop = false;
            // 
            // nvkWArray3Txt
            // 
            this.nvkWArray3Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvkWArray3Txt.Location = new System.Drawing.Point(696, 60);
            this.nvkWArray3Txt.Name = "nvkWArray3Txt";
            this.nvkWArray3Txt.ReadOnly = true;
            this.nvkWArray3Txt.Size = new System.Drawing.Size(100, 20);
            this.nvkWArray3Txt.TabIndex = 187;
            this.nvkWArray3Txt.TabStop = false;
            // 
            // nvLblDown9
            // 
            this.nvLblDown9.AutoSize = true;
            this.nvLblDown9.Location = new System.Drawing.Point(696, 427);
            this.nvLblDown9.Name = "nvLblDown9";
            this.nvLblDown9.Size = new System.Drawing.Size(22, 13);
            this.nvLblDown9.TabIndex = 205;
            this.nvLblDown9.Text = "0.0";
            // 
            // nvLblDown8
            // 
            this.nvLblDown8.AutoSize = true;
            this.nvLblDown8.Location = new System.Drawing.Point(590, 427);
            this.nvLblDown8.Name = "nvLblDown8";
            this.nvLblDown8.Size = new System.Drawing.Size(22, 13);
            this.nvLblDown8.TabIndex = 207;
            this.nvLblDown8.Text = "0.0";
            // 
            // nvLblDown7
            // 
            this.nvLblDown7.AutoSize = true;
            this.nvLblDown7.Location = new System.Drawing.Point(484, 427);
            this.nvLblDown7.Name = "nvLblDown7";
            this.nvLblDown7.Size = new System.Drawing.Size(22, 13);
            this.nvLblDown7.TabIndex = 206;
            this.nvLblDown7.Text = "0.0";
            // 
            // nvLblDown6
            // 
            this.nvLblDown6.AutoSize = true;
            this.nvLblDown6.Location = new System.Drawing.Point(696, 375);
            this.nvLblDown6.Name = "nvLblDown6";
            this.nvLblDown6.Size = new System.Drawing.Size(22, 13);
            this.nvLblDown6.TabIndex = 204;
            this.nvLblDown6.Text = "0.0";
            // 
            // nvLblDown5
            // 
            this.nvLblDown5.AutoSize = true;
            this.nvLblDown5.Location = new System.Drawing.Point(590, 375);
            this.nvLblDown5.Name = "nvLblDown5";
            this.nvLblDown5.Size = new System.Drawing.Size(22, 13);
            this.nvLblDown5.TabIndex = 203;
            this.nvLblDown5.Text = "0.0";
            // 
            // nvLblDown4
            // 
            this.nvLblDown4.AutoSize = true;
            this.nvLblDown4.Location = new System.Drawing.Point(484, 375);
            this.nvLblDown4.Name = "nvLblDown4";
            this.nvLblDown4.Size = new System.Drawing.Size(22, 13);
            this.nvLblDown4.TabIndex = 202;
            this.nvLblDown4.Text = "0.0";
            // 
            // nvLblDown3
            // 
            this.nvLblDown3.AutoSize = true;
            this.nvLblDown3.Location = new System.Drawing.Point(696, 322);
            this.nvLblDown3.Name = "nvLblDown3";
            this.nvLblDown3.Size = new System.Drawing.Size(22, 13);
            this.nvLblDown3.TabIndex = 201;
            this.nvLblDown3.Text = "0.0";
            // 
            // nvLblDown2
            // 
            this.nvLblDown2.AutoSize = true;
            this.nvLblDown2.Location = new System.Drawing.Point(590, 322);
            this.nvLblDown2.Name = "nvLblDown2";
            this.nvLblDown2.Size = new System.Drawing.Size(22, 13);
            this.nvLblDown2.TabIndex = 200;
            this.nvLblDown2.Text = "0.0";
            // 
            // nvLblDown1
            // 
            this.nvLblDown1.AutoSize = true;
            this.nvLblDown1.Location = new System.Drawing.Point(484, 322);
            this.nvLblDown1.Name = "nvLblDown1";
            this.nvLblDown1.Size = new System.Drawing.Size(22, 13);
            this.nvLblDown1.TabIndex = 199;
            this.nvLblDown1.Text = "0.0";
            // 
            // nvTxtDown9
            // 
            this.nvTxtDown9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvTxtDown9.Location = new System.Drawing.Point(696, 443);
            this.nvTxtDown9.Name = "nvTxtDown9";
            this.nvTxtDown9.ReadOnly = true;
            this.nvTxtDown9.Size = new System.Drawing.Size(100, 20);
            this.nvTxtDown9.TabIndex = 191;
            this.nvTxtDown9.TabStop = false;
            // 
            // nvTxtDown8
            // 
            this.nvTxtDown8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvTxtDown8.Location = new System.Drawing.Point(590, 443);
            this.nvTxtDown8.Name = "nvTxtDown8";
            this.nvTxtDown8.ReadOnly = true;
            this.nvTxtDown8.Size = new System.Drawing.Size(100, 20);
            this.nvTxtDown8.TabIndex = 192;
            this.nvTxtDown8.TabStop = false;
            // 
            // nvTxtDown7
            // 
            this.nvTxtDown7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvTxtDown7.Location = new System.Drawing.Point(484, 443);
            this.nvTxtDown7.Name = "nvTxtDown7";
            this.nvTxtDown7.ReadOnly = true;
            this.nvTxtDown7.Size = new System.Drawing.Size(100, 20);
            this.nvTxtDown7.TabIndex = 193;
            this.nvTxtDown7.TabStop = false;
            // 
            // nvTxtDown6
            // 
            this.nvTxtDown6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvTxtDown6.Location = new System.Drawing.Point(696, 391);
            this.nvTxtDown6.Name = "nvTxtDown6";
            this.nvTxtDown6.ReadOnly = true;
            this.nvTxtDown6.Size = new System.Drawing.Size(100, 20);
            this.nvTxtDown6.TabIndex = 186;
            this.nvTxtDown6.TabStop = false;
            // 
            // nvTxtDown1
            // 
            this.nvTxtDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvTxtDown1.Location = new System.Drawing.Point(484, 338);
            this.nvTxtDown1.Name = "nvTxtDown1";
            this.nvTxtDown1.ReadOnly = true;
            this.nvTxtDown1.Size = new System.Drawing.Size(100, 20);
            this.nvTxtDown1.TabIndex = 185;
            this.nvTxtDown1.TabStop = false;
            // 
            // nvTxtDown4
            // 
            this.nvTxtDown4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvTxtDown4.Location = new System.Drawing.Point(484, 391);
            this.nvTxtDown4.Name = "nvTxtDown4";
            this.nvTxtDown4.ReadOnly = true;
            this.nvTxtDown4.Size = new System.Drawing.Size(100, 20);
            this.nvTxtDown4.TabIndex = 172;
            this.nvTxtDown4.TabStop = false;
            // 
            // nvTxtDown5
            // 
            this.nvTxtDown5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvTxtDown5.Location = new System.Drawing.Point(590, 391);
            this.nvTxtDown5.Name = "nvTxtDown5";
            this.nvTxtDown5.ReadOnly = true;
            this.nvTxtDown5.Size = new System.Drawing.Size(100, 20);
            this.nvTxtDown5.TabIndex = 184;
            this.nvTxtDown5.TabStop = false;
            // 
            // nvTxtDown2
            // 
            this.nvTxtDown2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvTxtDown2.Location = new System.Drawing.Point(590, 338);
            this.nvTxtDown2.Name = "nvTxtDown2";
            this.nvTxtDown2.ReadOnly = true;
            this.nvTxtDown2.Size = new System.Drawing.Size(100, 20);
            this.nvTxtDown2.TabIndex = 183;
            this.nvTxtDown2.TabStop = false;
            // 
            // nvTxtDown3
            // 
            this.nvTxtDown3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvTxtDown3.Location = new System.Drawing.Point(696, 338);
            this.nvTxtDown3.Name = "nvTxtDown3";
            this.nvTxtDown3.ReadOnly = true;
            this.nvTxtDown3.Size = new System.Drawing.Size(100, 20);
            this.nvTxtDown3.TabIndex = 182;
            this.nvTxtDown3.TabStop = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(590, 116);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(73, 13);
            this.label28.TabIndex = 181;
            this.label28.Text = "Per Watt Cost";
            // 
            // nvPwcTxt
            // 
            this.nvPwcTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvPwcTxt.Location = new System.Drawing.Point(484, 112);
            this.nvPwcTxt.Name = "nvPwcTxt";
            this.nvPwcTxt.ReadOnly = true;
            this.nvPwcTxt.Size = new System.Drawing.Size(100, 20);
            this.nvPwcTxt.TabIndex = 180;
            this.nvPwcTxt.TabStop = false;
            this.nvPwcTxt.Text = "3.70";
            // 
            // btnCalcNV
            // 
            this.btnCalcNV.Location = new System.Drawing.Point(484, 228);
            this.btnCalcNV.Name = "btnCalcNV";
            this.btnCalcNV.Size = new System.Drawing.Size(100, 23);
            this.btnCalcNV.TabIndex = 197;
            this.btnCalcNV.Text = "Calculate";
            this.btnCalcNV.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(590, 205);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(92, 13);
            this.label29.TabIndex = 179;
            this.label29.Text = "Total System Cost";
            // 
            // nvTotalSystemCostTxt
            // 
            this.nvTotalSystemCostTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvTotalSystemCostTxt.Location = new System.Drawing.Point(484, 202);
            this.nvTotalSystemCostTxt.Name = "nvTotalSystemCostTxt";
            this.nvTotalSystemCostTxt.ReadOnly = true;
            this.nvTotalSystemCostTxt.Size = new System.Drawing.Size(100, 20);
            this.nvTotalSystemCostTxt.TabIndex = 178;
            this.nvTotalSystemCostTxt.TabStop = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(590, 89);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(75, 13);
            this.label30.TabIndex = 177;
            this.label30.Text = "Array 4 (North)";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(590, 63);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(70, 13);
            this.label31.TabIndex = 176;
            this.label31.Text = "Array 3 (East)";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(590, 37);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(74, 13);
            this.label32.TabIndex = 175;
            this.label32.Text = "Array 2 (West)";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(590, 11);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(77, 13);
            this.label33.TabIndex = 174;
            this.label33.Text = "Array 1 (South)";
            // 
            // nvArray1Txt
            // 
            this.nvArray1Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvArray1Txt.Location = new System.Drawing.Point(484, 8);
            this.nvArray1Txt.Name = "nvArray1Txt";
            this.nvArray1Txt.Size = new System.Drawing.Size(100, 20);
            this.nvArray1Txt.TabIndex = 173;
            this.nvArray1Txt.Text = "0";
            // 
            // nvArray2Txt
            // 
            this.nvArray2Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvArray2Txt.Location = new System.Drawing.Point(484, 34);
            this.nvArray2Txt.Name = "nvArray2Txt";
            this.nvArray2Txt.Size = new System.Drawing.Size(100, 20);
            this.nvArray2Txt.TabIndex = 194;
            this.nvArray2Txt.Text = "0";
            // 
            // nvArray3Txt
            // 
            this.nvArray3Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvArray3Txt.Location = new System.Drawing.Point(484, 60);
            this.nvArray3Txt.Name = "nvArray3Txt";
            this.nvArray3Txt.Size = new System.Drawing.Size(100, 20);
            this.nvArray3Txt.TabIndex = 195;
            this.nvArray3Txt.Text = "0";
            // 
            // nvArray4Txt
            // 
            this.nvArray4Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nvArray4Txt.Location = new System.Drawing.Point(484, 86);
            this.nvArray4Txt.Name = "nvArray4Txt";
            this.nvArray4Txt.Size = new System.Drawing.Size(100, 20);
            this.nvArray4Txt.TabIndex = 196;
            this.nvArray4Txt.Text = "0";
            // 
            // tabAnnoucements
            // 
            this.tabAnnoucements.Controls.Add(this.webBrowser8);
            this.tabAnnoucements.Controls.Add(this.webBrowser7);
            this.tabAnnoucements.Location = new System.Drawing.Point(4, 22);
            this.tabAnnoucements.Name = "tabAnnoucements";
            this.tabAnnoucements.Size = new System.Drawing.Size(1010, 472);
            this.tabAnnoucements.TabIndex = 2;
            this.tabAnnoucements.Text = "Annoucements";
            this.tabAnnoucements.UseVisualStyleBackColor = true;
            // 
            // webBrowser8
            // 
            this.webBrowser8.Location = new System.Drawing.Point(671, 3);
            this.webBrowser8.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser8.Name = "webBrowser8";
            this.webBrowser8.Size = new System.Drawing.Size(330, 466);
            this.webBrowser8.TabIndex = 2;
            this.webBrowser8.Url = new System.Uri(resources.GetString("webBrowser8.Url"), System.UriKind.Absolute);
            // 
            // webBrowser7
            // 
            this.webBrowser7.Location = new System.Drawing.Point(3, 3);
            this.webBrowser7.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser7.Name = "webBrowser7";
            this.webBrowser7.Size = new System.Drawing.Size(662, 466);
            this.webBrowser7.TabIndex = 1;
            this.webBrowser7.Url = new System.Uri(resources.GetString("webBrowser7.Url"), System.UriKind.Absolute);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1042, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aboutToolStripMenuItem1
            // 
            this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
            this.aboutToolStripMenuItem1.Size = new System.Drawing.Size(126, 20);
            this.aboutToolStripMenuItem1.Text = "Version/Release Info";
            this.aboutToolStripMenuItem1.Click += new System.EventHandler(this.aboutToolStripMenuItem1_Click);
            // 
            // VisionSolarCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1042, 534);
            this.Controls.Add(this.tabAnnouncements);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "VisionSolarCalculator";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Proposal Companion";
            this.tabAnnouncements.ResumeLayout(false);
            this.tabUtah.ResumeLayout(false);
            this.tabUtah.PerformLayout();
            this.tabSC.ResumeLayout(false);
            this.tabSC.PerformLayout();
            this.tabCalifornia.ResumeLayout(false);
            this.tabCalifornia.PerformLayout();
            this.tabTexasAustin.ResumeLayout(false);
            this.tabTexasAustin.PerformLayout();
            this.tabTexasSanAntonio.ResumeLayout(false);
            this.tabTexasSanAntonio.PerformLayout();
            this.tabVegas.ResumeLayout(false);
            this.tabVegas.PerformLayout();
            this.tabAnnoucements.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabAnnouncements;
        private System.Windows.Forms.TabPage tabUtah;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxArray1Utah;
        private System.Windows.Forms.TextBox textBoxArray2Utah;
        private System.Windows.Forms.TextBox textBoxArray3Utah;
        private System.Windows.Forms.TextBox textBoxArray4Utah;
        private System.Windows.Forms.Button btnCalculateUtah;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxTotalUtah;
        private System.Windows.Forms.TextBox textBoxPerWattCostUT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox380UT;
        private System.Windows.Forms.TextBox textBox365UT;
        private System.Windows.Forms.TextBox textBox360UT;
        private System.Windows.Forms.TextBox textBox375UT;
        private System.Windows.Forms.TextBox textBox370UT;
        private System.Windows.Forms.Label lblSixthWatt;
        private System.Windows.Forms.Label lblFifthWatt;
        private System.Windows.Forms.Label lblFourthWatt;
        private System.Windows.Forms.Label lblThirdWatt;
        private System.Windows.Forms.Label lblSecondWatt;
        private System.Windows.Forms.Label lblFirstWatt;
        private System.Windows.Forms.TextBox textBox355UT;
        private System.Windows.Forms.TextBox textBoxArray4KwUT;
        private System.Windows.Forms.TextBox textBoxArray1KwUT;
        private System.Windows.Forms.TextBox textBoxArray2KwUT;
        private System.Windows.Forms.TextBox textBoxArray3KwUT;
        private System.Windows.Forms.TextBox textBoxKwTotalUT;
        private System.Windows.Forms.TextBox textBoxEmailTemplateUT;
        private System.Windows.Forms.Button btnClearUT;
        private System.Windows.Forms.TabPage tabSC;
        private System.Windows.Forms.Button btnClearSC;
        private System.Windows.Forms.TextBox textBoxEmailTemplateSC;
        private System.Windows.Forms.TextBox textBoxTotalKWSC;
        private System.Windows.Forms.TextBox textBoxArrayKWSC4;
        private System.Windows.Forms.TextBox textBoxArrayKWSC1;
        private System.Windows.Forms.TextBox textBoxArrayKWSC2;
        private System.Windows.Forms.TextBox textBoxArrayKWSC3;
        private System.Windows.Forms.Label lblPWCSC7;
        private System.Windows.Forms.Label lblPWCSC6;
        private System.Windows.Forms.Label lblPWCSC5;
        private System.Windows.Forms.Label lblPWCSC4;
        private System.Windows.Forms.Label lblPWCSC3;
        private System.Windows.Forms.Label lblPWCSC2;
        private System.Windows.Forms.Label lblPWCSC1;
        private System.Windows.Forms.TextBox textBoxPWCDOWNSC7;
        private System.Windows.Forms.TextBox textBoxPWCDOWNSC6;
        private System.Windows.Forms.TextBox textBoxPWCDOWNSC1;
        private System.Windows.Forms.TextBox textBoxPWCDOWNSC4;
        private System.Windows.Forms.TextBox textBoxPWCDOWNSC5;
        private System.Windows.Forms.TextBox textBoxPWCDOWNSC2;
        private System.Windows.Forms.TextBox textBoxPWCDOWNSC3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxPWCSC;
        private System.Windows.Forms.Button btnCalculateSC;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxTotalPriceSC;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxArraySC1;
        private System.Windows.Forms.TextBox textBoxArraySC2;
        private System.Windows.Forms.TextBox textBoxArraySC3;
        private System.Windows.Forms.TextBox textBoxArraySC4;
        private System.Windows.Forms.TabPage tabAnnoucements;
        private System.Windows.Forms.Label lblPWCSC9;
        private System.Windows.Forms.Label lblPWCSC8;
        private System.Windows.Forms.TextBox textBoxPWCDOWNSC9;
        private System.Windows.Forms.TextBox textBoxPWCDOWNSC8;
        private System.Windows.Forms.TabPage tabCalifornia;
        private System.Windows.Forms.Button btnCAClear;
        private System.Windows.Forms.TextBox txtCAEmailTemplate;
        private System.Windows.Forms.TextBox txtCAKWTotal;
        private System.Windows.Forms.TextBox txtCAKWArray4;
        private System.Windows.Forms.TextBox txtCAKWArray1;
        private System.Windows.Forms.TextBox txtCAKWArray2;
        private System.Windows.Forms.TextBox txtCAKWArray3;
        private System.Windows.Forms.Label lblCAPWCDown9;
        private System.Windows.Forms.Label lblCAPWCDown8;
        private System.Windows.Forms.Label lblCAPWCDown7;
        private System.Windows.Forms.Label lblCAPWCDown6;
        private System.Windows.Forms.Label lblCAPWCDown5;
        private System.Windows.Forms.Label lblCAPWCDown4;
        private System.Windows.Forms.Label lblCAPWCDown3;
        private System.Windows.Forms.Label lblCAPWCDown2;
        private System.Windows.Forms.Label lblCAPWCDown1;
        private System.Windows.Forms.TextBox txtCATotalSystemCost10;
        private System.Windows.Forms.TextBox txtCATotalSystemCost9;
        private System.Windows.Forms.TextBox txtCATotalSystemCost8;
        private System.Windows.Forms.TextBox txtCATotalSystemCost7;
        private System.Windows.Forms.TextBox txtCATotalSystemCost2;
        private System.Windows.Forms.TextBox txtCATotalSystemCost5;
        private System.Windows.Forms.TextBox txtCATotalSystemCost6;
        private System.Windows.Forms.TextBox txtCATotalSystemCost3;
        private System.Windows.Forms.TextBox txtCATotalSystemCost4;
        private System.Windows.Forms.Label lblCAPWC;
        private System.Windows.Forms.TextBox txtCAPWC;
        private System.Windows.Forms.Button btnCACalculate;
        private System.Windows.Forms.Label lblCATotalSystemCost;
        private System.Windows.Forms.TextBox txtCATotalSystemCost1;
        private System.Windows.Forms.Label lblCAArray4;
        private System.Windows.Forms.Label lblCAArray3;
        private System.Windows.Forms.Label lblCAArray2;
        private System.Windows.Forms.Label lblCAArray1;
        private System.Windows.Forms.TextBox txtCAArray1;
        private System.Windows.Forms.TextBox txtCAArray2;
        private System.Windows.Forms.TextBox txtCAArray3;
        private System.Windows.Forms.TextBox txtCAArray4;
        private System.Windows.Forms.TabPage tabTexasAustin;
        private System.Windows.Forms.TabPage tabTexasSanAntonio;
        private System.Windows.Forms.TabPage tabVegas;
        private System.Windows.Forms.Button btnAustinClear;
        private System.Windows.Forms.TextBox austinTxtEmailTemplate;
        private System.Windows.Forms.TextBox austinTxtKwTotal;
        private System.Windows.Forms.TextBox austinTxtKwArray4;
        private System.Windows.Forms.TextBox austinTxtKwArray1;
        private System.Windows.Forms.TextBox austinTxtKwArray2;
        private System.Windows.Forms.TextBox austinTxtKwArray3;
        private System.Windows.Forms.Label lblAustinPWCDown9;
        private System.Windows.Forms.Label lblAustinPWCDown8;
        private System.Windows.Forms.Label lblAustinPWCDown7;
        private System.Windows.Forms.Label lblAustinPWCDown6;
        private System.Windows.Forms.Label lblAustinPWCDown5;
        private System.Windows.Forms.Label lblAustinPWCDown4;
        private System.Windows.Forms.Label lblAustinPWCDown3;
        private System.Windows.Forms.Label lblAustinPWCDown2;
        private System.Windows.Forms.Label lblAustinPWCDown1;
        private System.Windows.Forms.TextBox austinTxtTotalSystemCost10;
        private System.Windows.Forms.TextBox austinTxtTotalSystemCost9;
        private System.Windows.Forms.TextBox austinTxtTotalSystemCost8;
        private System.Windows.Forms.TextBox austinTxtTotalSystemCost7;
        private System.Windows.Forms.TextBox austinTxtTotalSystemCost2;
        private System.Windows.Forms.TextBox austinTxtTotalSystemCost5;
        private System.Windows.Forms.TextBox austinTxtTotalSystemCost6;
        private System.Windows.Forms.TextBox austinTxtTotalSystemCost3;
        private System.Windows.Forms.TextBox austinTxtTotalSystemCost4;
        private System.Windows.Forms.Label lblAustinPWC;
        private System.Windows.Forms.TextBox austinTxtPWC1;
        private System.Windows.Forms.Button btnAustinCalc;
        private System.Windows.Forms.Label lblAustinTotalSystemCost;
        private System.Windows.Forms.TextBox austinTxtTotalSystemCost1;
        private System.Windows.Forms.Label lblAustinArray4;
        private System.Windows.Forms.Label lblAustinArray3;
        private System.Windows.Forms.Label lblAustinArray2;
        private System.Windows.Forms.Label lblAustinArray1;
        private System.Windows.Forms.TextBox austinTxtArray1;
        private System.Windows.Forms.TextBox austinTxtArray2;
        private System.Windows.Forms.TextBox austinTxtArray3;
        private System.Windows.Forms.TextBox austinTxtArray4;
        private System.Windows.Forms.Button btnSanAntonioClear;
        private System.Windows.Forms.TextBox sanAntonioTxtEmailInformation;
        private System.Windows.Forms.TextBox sanAntonioTxtKWTotal;
        private System.Windows.Forms.TextBox sanAntonioTxtKWArray4;
        private System.Windows.Forms.TextBox sanAntonioTxtKWArray1;
        private System.Windows.Forms.TextBox sanAntonioTxtKWArray2;
        private System.Windows.Forms.TextBox sanAntonioTxtKWArray3;
        private System.Windows.Forms.Label sanAntonioLblPwcDown9;
        private System.Windows.Forms.Label sanAntonioLblPwcDown8;
        private System.Windows.Forms.Label sanAntonioLblPwcDown7;
        private System.Windows.Forms.Label sanAntonioLblPwcDown6;
        private System.Windows.Forms.Label sanAntonioLblPwcDown5;
        private System.Windows.Forms.Label sanAntonioLblPwcDown4;
        private System.Windows.Forms.Label sanAntonioLblPwcDown3;
        private System.Windows.Forms.Label sanAntonioLblPwcDown2;
        private System.Windows.Forms.Label sanAntonioLblPwcDown1;
        private System.Windows.Forms.TextBox sanAntonioTxtTotalSystemCost10;
        private System.Windows.Forms.TextBox sanAntonioTxtTotalSystemCost9;
        private System.Windows.Forms.TextBox sanAntonioTxtTotalSystemCost8;
        private System.Windows.Forms.TextBox sanAntonioTxtTotalSystemCost7;
        private System.Windows.Forms.TextBox sanAntonioTxtTotalSystemCost2;
        private System.Windows.Forms.TextBox sanAntonioTxtTotalSystemCost5;
        private System.Windows.Forms.TextBox sanAntonioTxtTotalSystemCost6;
        private System.Windows.Forms.TextBox sanAntonioTxtTotalSystemCost3;
        private System.Windows.Forms.TextBox sanAntonioTxtTotalSystemCost4;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox sanAntonioTxtPWC;
        private System.Windows.Forms.Button btnSanAntonioCalc;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox sanAntonioTxtTotalSystemCost1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox sanAntonioTxtArray1;
        private System.Windows.Forms.TextBox sanAntonioTxtArray2;
        private System.Windows.Forms.TextBox sanAntonioTxtArray3;
        private System.Windows.Forms.TextBox sanAntonioTxtArray4;
        private System.Windows.Forms.Label lblNinthWatt;
        private System.Windows.Forms.Label lblEighthWatt;
        private System.Windows.Forms.Label lblSeventhWatt;
        private System.Windows.Forms.TextBox textBox340UT;
        private System.Windows.Forms.TextBox textBox350UT;
        private System.Windows.Forms.TextBox textBox345UT;
        private System.Windows.Forms.CheckBox utahChkBtnCash;
        private System.Windows.Forms.CheckBox southCarolinaChkCashBtn;
        private System.Windows.Forms.CheckBox californiaChkCashBtn;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
        private System.Windows.Forms.CheckBox austinChkCashBtn;
        private System.Windows.Forms.CheckBox sanAntonioChkCashBtn;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.WebBrowser webBrowser2;
        private System.Windows.Forms.WebBrowser webBrowser3;
        private System.Windows.Forms.WebBrowser webBrowser4;
        private System.Windows.Forms.WebBrowser webBrowser5;
        private System.Windows.Forms.WebBrowser webBrowser6;
        private System.Windows.Forms.CheckBox nvChkCash;
        private System.Windows.Forms.Button btnClearNV;
        private System.Windows.Forms.TextBox nvEmailTemplateTxt;
        private System.Windows.Forms.TextBox nvTotalKwTxt;
        private System.Windows.Forms.TextBox nvkWArray4Txt;
        private System.Windows.Forms.TextBox nvkWArray1Txt;
        private System.Windows.Forms.TextBox nvkWArray2Txt;
        private System.Windows.Forms.TextBox nvkWArray3Txt;
        private System.Windows.Forms.Label nvLblDown9;
        private System.Windows.Forms.Label nvLblDown8;
        private System.Windows.Forms.Label nvLblDown7;
        private System.Windows.Forms.Label nvLblDown6;
        private System.Windows.Forms.Label nvLblDown5;
        private System.Windows.Forms.Label nvLblDown4;
        private System.Windows.Forms.Label nvLblDown3;
        private System.Windows.Forms.Label nvLblDown2;
        private System.Windows.Forms.Label nvLblDown1;
        private System.Windows.Forms.TextBox nvTxtDown9;
        private System.Windows.Forms.TextBox nvTxtDown8;
        private System.Windows.Forms.TextBox nvTxtDown7;
        private System.Windows.Forms.TextBox nvTxtDown6;
        private System.Windows.Forms.TextBox nvTxtDown1;
        private System.Windows.Forms.TextBox nvTxtDown4;
        private System.Windows.Forms.TextBox nvTxtDown5;
        private System.Windows.Forms.TextBox nvTxtDown2;
        private System.Windows.Forms.TextBox nvTxtDown3;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox nvPwcTxt;
        private System.Windows.Forms.Button btnCalcNV;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox nvTotalSystemCostTxt;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox nvArray1Txt;
        private System.Windows.Forms.TextBox nvArray2Txt;
        private System.Windows.Forms.TextBox nvArray3Txt;
        private System.Windows.Forms.TextBox nvArray4Txt;
        private System.Windows.Forms.WebBrowser webBrowser8;
        private System.Windows.Forms.WebBrowser webBrowser7;
    }
}

