﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace visionsolarcalculator
{
    public partial class VisionSolarCalculator : Form
    {
        // Made a class variable for PerWattCost
        double PerWattCost;
        const double INCENTIVE = 0;


        public VisionSolarCalculator()
        {
            InitializeComponent();
        }


        // This section begins Utah.


        /// <summary>
        /// Takes the Arrays and calulates finances Utah
        /// UTAH
        /// UTAH
        /// UTAH
        /// UTAH
        /// UTAH
        /// UTAH
        /// UTAH
        /// UTAH
        /// UTAH
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCalculateUtah_Click(object sender, EventArgs e)
        {
            try
            {
                // Define Variables
                double Array1UT, Array2UT, Array3UT, Array4UT, TotalPanelCountUtah, TotalkWUtah, TotalSystemCostUT, DIVIDENDFEEUT, TotalSystemCostWithFeesUT;

                // If Cash button is checked
                if (utahChkBtnCash.Checked)
                {
                    // Changed Per Watt Cost text box in utah to cash text box.
                    textBoxPerWattCostUT.Text = "4.25";

                    // Recieve input from textBoxArray1-4Utah and put into variable
                    Array1UT = double.Parse(textBoxArray1Utah.Text);
                    Array2UT = double.Parse(textBoxArray2Utah.Text);
                    Array3UT = double.Parse(textBoxArray3Utah.Text);
                    Array4UT = double.Parse(textBoxArray4Utah.Text);

                    // Receive per watt cost from TextBox
                    PerWattCost = double.Parse(textBoxPerWattCostUT.Text);

                    // Calculate with "SumOfArrays = TotalPanelCountUtah * 260 = TotalkWUtah * PerWattCost + 2000 = TotalSystemCostUtah"
                    TotalPanelCountUtah = Array1UT + Array2UT + Array3UT + Array4UT;
                    TotalkWUtah = TotalPanelCountUtah * 260;
                    TotalSystemCostUT = ((TotalkWUtah * PerWattCost));

                    // To calculate the Dividend fee then add it into the Utah system cost.
                    DIVIDENDFEEUT = 0;
                    TotalSystemCostWithFeesUT = DIVIDENDFEEUT + TotalSystemCostUT;

                    // Show results in text box
                    textBoxTotalUtah.Text = TotalSystemCostWithFeesUT.ToString("c");

                    // Text boxes show degredation of per watt cost 3.85 to per watt cost minimum of 3.5 for Utah
                    lblFirstWatt.Text = (PerWattCost - .05).ToString("n2");
                    lblSecondWatt.Text = (double.Parse(lblFirstWatt.Text) - .05).ToString("n2");
                    lblThirdWatt.Text = (double.Parse(lblSecondWatt.Text) - .05).ToString("n2");
                    lblFourthWatt.Text = (double.Parse(lblThirdWatt.Text) - .05).ToString("n2");
                    lblFifthWatt.Text = (double.Parse(lblFourthWatt.Text) - .05).ToString("n2");
                    lblSixthWatt.Text = (double.Parse(lblFifthWatt.Text) - .05).ToString("n2");
                    lblSeventhWatt.Text = (double.Parse(lblSixthWatt.Text) - .05).ToString("n2");
                    lblEighthWatt.Text = (double.Parse(lblSeventhWatt.Text) - .05).ToString("n2");
                    lblNinthWatt.Text = (double.Parse(lblEighthWatt.Text) - .05).ToString("n2");


                    // Diplay the system cost of each degraded per watt cost to minimum of 3.5
                    textBox380UT.Text = ((TotalkWUtah * double.Parse(lblFirstWatt.Text)).ToString("c"));
                    textBox375UT.Text = ((TotalkWUtah * double.Parse(lblSecondWatt.Text)).ToString("c"));
                    textBox370UT.Text = ((TotalkWUtah * double.Parse(lblThirdWatt.Text)).ToString("c"));
                    textBox365UT.Text = ((TotalkWUtah * double.Parse(lblFourthWatt.Text)).ToString("c"));
                    textBox360UT.Text = ((TotalkWUtah * double.Parse(lblFifthWatt.Text)).ToString("c"));
                    textBox355UT.Text = ((TotalkWUtah * double.Parse(lblSixthWatt.Text)).ToString("c"));
                    textBox350UT.Text = ((TotalkWUtah * double.Parse(lblSeventhWatt.Text)).ToString("c"));
                    textBox345UT.Text = ((TotalkWUtah * double.Parse(lblEighthWatt.Text)).ToString("c"));
                    textBox340UT.Text = ((TotalkWUtah * double.Parse(lblNinthWatt.Text)).ToString("c"));

                    // Display kW size of each array in Text Boxes
                    textBoxArray1KwUT.Text = (((double.Parse(textBoxArray1Utah.Text) * 260) / 1000).ToString("n2"));
                    textBoxArray2KwUT.Text = (((double.Parse(textBoxArray2Utah.Text) * 260) / 1000).ToString("n2"));
                    textBoxArray3KwUT.Text = (((double.Parse(textBoxArray3Utah.Text) * 260) / 1000).ToString("n2"));
                    textBoxArray4KwUT.Text = (((double.Parse(textBoxArray4Utah.Text) * 260) / 1000).ToString("n2"));

                    // Display kW total in total box
                    double Array1KWUT, Array2KWUT, Array3KWUT, Array4KWUT;
                    Array1KWUT = double.Parse(textBoxArray1KwUT.Text);
                    Array2KWUT = double.Parse(textBoxArray2KwUT.Text);
                    Array3KWUT = double.Parse(textBoxArray3KwUT.Text);
                    Array4KWUT = double.Parse(textBoxArray4KwUT.Text);
                    textBoxKwTotalUT.Text = (Array1KWUT + Array2KWUT + Array3KWUT + Array4KWUT).ToString("n2");

                    // Make text box email template display an email template with variables filled in.
                    textBoxEmailTemplateUT.Text = $"The documents are as follows:\r\n\r\nFileNameHere\r\n\r\n► {TotalPanelCountUtah} Panel System\r\n► {TotalkWUtah / 1000} kW System Size\r\n► ElectricalUsageOffsetHere\r\n\r\nInformationAboutTheSystemHere\r\n\r\nIf there are any questions or concerns, please reply to this email.\r\n\r\n\r\n\r\nRegards,\r\n\r\nYourNameHere";
                }
                else
                {
                    // Keep the Per Watt in Utah as 3.80
                    textBoxPerWattCostUT.Text = "3.80";

                    // Recieve input from textBoxArray1-4Utah and put into variable
                    Array1UT = double.Parse(textBoxArray1Utah.Text);
                    Array2UT = double.Parse(textBoxArray2Utah.Text);
                    Array3UT = double.Parse(textBoxArray3Utah.Text);
                    Array4UT = double.Parse(textBoxArray4Utah.Text);

                    // Receive per watt cost from TextBox
                    PerWattCost = double.Parse(textBoxPerWattCostUT.Text);

                    // Calculate with "SumOfArrays = TotalPanelCountUtah * 260 = TotalkWUtah * PerWattCost + 2000 = TotalSystemCostUtah"
                    TotalPanelCountUtah = Array1UT + Array2UT + Array3UT + Array4UT;
                    TotalkWUtah = TotalPanelCountUtah * 260;
                    TotalSystemCostUT = ((TotalkWUtah * PerWattCost) + ((TotalkWUtah * PerWattCost) * 0.07));

                    // To calculate the Dividend fee then add it into the Utah system cost.
                    DIVIDENDFEEUT = 0;
                    TotalSystemCostWithFeesUT = DIVIDENDFEEUT + TotalSystemCostUT;

                    // Show results in text box
                    textBoxTotalUtah.Text = TotalSystemCostWithFeesUT.ToString("c");

                    // Text boxes show degredation of per watt cost 3.85 to per watt cost minimum of 3.5 for Utah
                    lblFirstWatt.Text = (PerWattCost - .05).ToString("n2");
                    lblSecondWatt.Text = (double.Parse(lblFirstWatt.Text) - .05).ToString("n2");
                    lblThirdWatt.Text = (double.Parse(lblSecondWatt.Text) - .05).ToString("n2");
                    lblFourthWatt.Text = (double.Parse(lblThirdWatt.Text) - .05).ToString("n2");
                    lblFifthWatt.Text = (double.Parse(lblFourthWatt.Text) - .05).ToString("n2");
                    lblSixthWatt.Text = (double.Parse(lblFifthWatt.Text) - .05).ToString("n2");
                    lblSeventhWatt.Text = (double.Parse(lblSixthWatt.Text) - .05).ToString("n2");
                    lblEighthWatt.Text = (double.Parse(lblSeventhWatt.Text) - .05).ToString("n2");
                    lblNinthWatt.Text = (double.Parse(lblEighthWatt.Text) - .05).ToString("n2");


                    // Diplay the system cost of each degraded per watt cost to minimum of 3.5
                    textBox380UT.Text = ((TotalkWUtah * double.Parse(lblFirstWatt.Text) + ((TotalkWUtah * double.Parse(lblFirstWatt.Text)) * 0.07)).ToString("c"));
                    textBox375UT.Text = ((TotalkWUtah * double.Parse(lblSecondWatt.Text) + ((TotalkWUtah * double.Parse(lblSecondWatt.Text)) * 0.07)).ToString("c"));
                    textBox370UT.Text = ((TotalkWUtah * double.Parse(lblThirdWatt.Text) + ((TotalkWUtah * double.Parse(lblThirdWatt.Text)) * 0.07)).ToString("c"));
                    textBox365UT.Text = ((TotalkWUtah * double.Parse(lblFourthWatt.Text) + ((TotalkWUtah * double.Parse(lblFourthWatt.Text)) * 0.07)).ToString("c"));
                    textBox360UT.Text = ((TotalkWUtah * double.Parse(lblFifthWatt.Text) + ((TotalkWUtah * double.Parse(lblFifthWatt.Text)) * 0.07)).ToString("c"));
                    textBox355UT.Text = ((TotalkWUtah * double.Parse(lblSixthWatt.Text) + ((TotalkWUtah * double.Parse(lblSixthWatt.Text)) * 0.07)).ToString("c"));
                    textBox350UT.Text = ((TotalkWUtah * double.Parse(lblSeventhWatt.Text) + ((TotalkWUtah * double.Parse(lblSeventhWatt.Text)) * 0.07)).ToString("c"));
                    textBox345UT.Text = ((TotalkWUtah * double.Parse(lblEighthWatt.Text) + ((TotalkWUtah * double.Parse(lblEighthWatt.Text)) * 0.07)).ToString("c"));
                    textBox340UT.Text = ((TotalkWUtah * double.Parse(lblNinthWatt.Text) + ((TotalkWUtah * double.Parse(lblNinthWatt.Text)) * 0.07)).ToString("c"));

                    // Display kW size of each array in Text Boxes
                    textBoxArray1KwUT.Text = (((double.Parse(textBoxArray1Utah.Text) * 260) / 1000).ToString("n2"));
                    textBoxArray2KwUT.Text = (((double.Parse(textBoxArray2Utah.Text) * 260) / 1000).ToString("n2"));
                    textBoxArray3KwUT.Text = (((double.Parse(textBoxArray3Utah.Text) * 260) / 1000).ToString("n2"));
                    textBoxArray4KwUT.Text = (((double.Parse(textBoxArray4Utah.Text) * 260) / 1000).ToString("n2"));

                    // Display kW total in total box
                    double Array1KWUT, Array2KWUT, Array3KWUT, Array4KWUT;
                    Array1KWUT = double.Parse(textBoxArray1KwUT.Text);
                    Array2KWUT = double.Parse(textBoxArray2KwUT.Text);
                    Array3KWUT = double.Parse(textBoxArray3KwUT.Text);
                    Array4KWUT = double.Parse(textBoxArray4KwUT.Text);
                    textBoxKwTotalUT.Text = (Array1KWUT + Array2KWUT + Array3KWUT + Array4KWUT).ToString("n2");

                    // Make text box email template display an email template with variables filled in.
                    textBoxEmailTemplateUT.Text = $"The documents are as follows:\r\n\r\nFileNameHere\r\n\r\n► {TotalPanelCountUtah} Panel System\r\n► {TotalkWUtah / 1000} kW System Size\r\n► ElectricalUsageOffsetHere\r\n\r\nInformationAboutTheSystemHere\r\n\r\nIf there are any questions or concerns, please reply to this email.\r\n\r\n\r\n\r\nRegards,\r\n\r\nYourNameHere";
                }

                // Set Array 1 as focus
                textBoxArray1Utah.Focus();
            }
            catch
            {

            }
        }


        /// <summary>
        /// Clears each array filling it with 0. Clears all totals. Refocuses to array 1. Clears Email Template.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClearUT_Click(object sender, EventArgs e)
        {
            // Clears all the arrays and fills them with 0. Clears all totals. Refocuses to array 1. Clears email template.
            textBoxArray1Utah.Text = ("0");
            textBoxArray2Utah.Text = ("0");
            textBoxArray3Utah.Text = ("0");
            textBoxArray4Utah.Text = ("0");
            textBoxArray1KwUT.Text = ("");
            textBoxArray2KwUT.Text = ("");
            textBoxArray3KwUT.Text = ("");
            textBoxArray4KwUT.Text = ("");
            textBoxKwTotalUT.Text = ("");
            textBoxTotalUtah.Text = ("");
            textBox380UT.Text = ("");
            textBox375UT.Text = ("");
            textBox370UT.Text = ("");
            textBox365UT.Text = ("");
            textBox360UT.Text = ("");
            textBox355UT.Text = ("");
            textBox350UT.Text = ("");
            textBox345UT.Text = ("");
            textBox340UT.Text = ("");
            textBoxArray1Utah.Focus();
            textBoxEmailTemplateUT.Text = ("");
            utahChkBtnCash.Checked = false;
        }


        /// <summary>
        /// Copies email text to clipboard.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxEmailTemplateUT_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                Clipboard.Clear();    //Clear if any old value is there in Clipboard        
                Clipboard.SetText(textBoxEmailTemplateUT.Text); //Copy text to Clipboard
                string strClip = Clipboard.GetText(); //Get text from Clipboard
                MessageBox.Show("Text Copied.");
            }
            catch
            {

            }
        }



        /// <summary>
        /// This section begins South Carolina.
        /// SOUTH CAROLINA
        /// SOUTH CAROLINA
        /// SOUTH CAROLINA
        /// SOUTH CAROLINA
        /// SOUTH CAROLINA
        /// SOUTH CAROLINA
        /// SOUTH CAROLINA
        /// SOUTH CAROLINA
        /// SOUTH CAROLINA
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 


        /// <summary>
        /// To calculate the South Carolina financing and generate email template.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                // If check box is checked, run cash numbers.
                if (southCarolinaChkCashBtn.Checked)
                {
                    textBoxPWCSC.Text = "4.25";
                }
                else
                {
                    textBoxPWCSC.Text = "3.80";
                }

                // Set Array 1 as focus
                textBoxArraySC1.Focus();

                // Define Variables
                double Array1SC, Array2SC, Array3SC, Array4SC, TotalPanelCountSC, TotalkWSC, TotalSystemCostSC;

                // Recieve input from textBoxArray1-4Utah and put into variable
                Array1SC = double.Parse(textBoxArraySC1.Text);
                Array2SC = double.Parse(textBoxArraySC2.Text);
                Array3SC = double.Parse(textBoxArraySC3.Text);
                Array4SC = double.Parse(textBoxArraySC4.Text);

                // Receive per watt cost from TextBox
                PerWattCost = double.Parse(textBoxPWCSC.Text);

                // Calculate with "SumOfArrays = TotalPanelCountSC * 260 = TotalkWSC * PerWattCost + 2000 = TotalSystemCostSC"
                TotalPanelCountSC = Array1SC + Array2SC + Array3SC + Array4SC;
                TotalkWSC = TotalPanelCountSC * 260;
                TotalSystemCostSC = TotalkWSC * PerWattCost + INCENTIVE;

                // Show results in text box
                textBoxTotalPriceSC.Text = TotalSystemCostSC.ToString("c");

                // Text boxes show degredation of per watt cost 3.80 to per watt cost minimum of 3.45 for SC
                lblPWCSC1.Text = (PerWattCost - .05).ToString("n2");
                lblPWCSC2.Text = (double.Parse(lblPWCSC1.Text) - .05).ToString("n2");
                lblPWCSC3.Text = (double.Parse(lblPWCSC2.Text) - .05).ToString("n2");
                lblPWCSC4.Text = (double.Parse(lblPWCSC3.Text) - .05).ToString("n2");
                lblPWCSC5.Text = (double.Parse(lblPWCSC4.Text) - .05).ToString("n2");
                lblPWCSC6.Text = (double.Parse(lblPWCSC5.Text) - .05).ToString("n2");
                lblPWCSC7.Text = (double.Parse(lblPWCSC6.Text) - .05).ToString("n2");
                lblPWCSC8.Text = (double.Parse(lblPWCSC7.Text) - .05).ToString("n2");
                lblPWCSC9.Text = (double.Parse(lblPWCSC8.Text) - .05).ToString("n2");

                // Diplay the system cost of each degraded per watt cost to minimum of 3.45
                textBoxPWCDOWNSC1.Text = (TotalkWSC * double.Parse(lblPWCSC1.Text) + INCENTIVE).ToString("c");
                textBoxPWCDOWNSC2.Text = (TotalkWSC * double.Parse(lblPWCSC2.Text) + INCENTIVE).ToString("c");
                textBoxPWCDOWNSC3.Text = (TotalkWSC * double.Parse(lblPWCSC3.Text) + INCENTIVE).ToString("c");
                textBoxPWCDOWNSC4.Text = (TotalkWSC * double.Parse(lblPWCSC4.Text) + INCENTIVE).ToString("c");
                textBoxPWCDOWNSC5.Text = (TotalkWSC * double.Parse(lblPWCSC5.Text) + INCENTIVE).ToString("c");
                textBoxPWCDOWNSC6.Text = (TotalkWSC * double.Parse(lblPWCSC6.Text) + INCENTIVE).ToString("c");
                textBoxPWCDOWNSC7.Text = (TotalkWSC * double.Parse(lblPWCSC7.Text) + INCENTIVE).ToString("c");
                textBoxPWCDOWNSC8.Text = (TotalkWSC * double.Parse(lblPWCSC8.Text) + INCENTIVE).ToString("c");
                textBoxPWCDOWNSC9.Text = (TotalkWSC * double.Parse(lblPWCSC9.Text) + INCENTIVE).ToString("c");

                // Display kW size of each array in Text Boxes
                textBoxArrayKWSC1.Text = (((double.Parse(textBoxArraySC1.Text) * 260) / 1000).ToString("n2"));
                textBoxArrayKWSC2.Text = (((double.Parse(textBoxArraySC2.Text) * 260) / 1000).ToString("n2"));
                textBoxArrayKWSC3.Text = (((double.Parse(textBoxArraySC3.Text) * 260) / 1000).ToString("n2"));
                textBoxArrayKWSC4.Text = (((double.Parse(textBoxArraySC4.Text) * 260) / 1000).ToString("n2"));

                // Display kW total in total box
                double Array1KWSC, Array2KWSC, Array3KWSC, Array4KWSC;
                Array1KWSC = double.Parse(textBoxArrayKWSC1.Text);
                Array2KWSC = double.Parse(textBoxArrayKWSC2.Text);
                Array3KWSC = double.Parse(textBoxArrayKWSC3.Text);
                Array4KWSC = double.Parse(textBoxArrayKWSC4.Text);
                textBoxTotalKWSC.Text = (Array1KWSC + Array2KWSC + Array3KWSC + Array4KWSC).ToString("n2");

                // Make text box email template display an email template with variables filled in.
                textBoxEmailTemplateSC.Text = $"The documents are as follows:\r\n\r\nFileNameHere\r\n\r\n► {TotalPanelCountSC} Panel System\r\n► {TotalkWSC / 1000} kW System Size\r\n► ElectricalUsageOffsetHere\r\n\r\nInformationAboutTheSystemHere\r\n\r\nIf there are any questions or concerns, please reply to this email.\r\n\r\n\r\n\r\nRegards,\r\n\r\nYourNameHere";

                // Show messagebox

            }
            catch
            {
                MessageBox.Show("Type only intergers");
            }
        }



        /// <summary>
        /// To clear the text boxes in the South Carolina Tab.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClearSC_Click(object sender, EventArgs e)
        {
            textBoxArrayKWSC1.Text = ("");
            textBoxArrayKWSC2.Text = ("");
            textBoxArrayKWSC3.Text = ("");
            textBoxArrayKWSC4.Text = ("");
            textBoxArraySC1.Text = ("0");
            textBoxArraySC2.Text = ("0");
            textBoxArraySC3.Text = ("0");
            textBoxArraySC4.Text = ("0");
            textBoxEmailTemplateSC.Text = ("");
            textBoxPWCDOWNSC1.Text = ("");
            textBoxPWCDOWNSC2.Text = ("");
            textBoxPWCDOWNSC3.Text = ("");
            textBoxPWCDOWNSC4.Text = ("");
            textBoxPWCDOWNSC5.Text = ("");
            textBoxPWCDOWNSC6.Text = ("");
            textBoxPWCDOWNSC7.Text = ("");
            textBoxTotalKWSC.Text = ("");
            textBoxTotalPriceSC.Text = ("");
            textBoxArraySC1.Focus();
            southCarolinaChkCashBtn.Checked = false;
        }

        private void textBoxEmailTemplateSC_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                Clipboard.Clear();    //Clear if any old value is there in Clipboard        
                Clipboard.SetText(textBoxEmailTemplateSC.Text); //Copy text to Clipboard
                string strClip = Clipboard.GetText(); //Get text from Clipboard
                MessageBox.Show("Text Copied.");
            }
            catch
            {

            }
        }







        /// <summary>
        /// CALIFORNIA
        /// CALIFORNIA
        /// CALIFORNIA
        /// CALIFORNIA
        /// CALIFORNIA
        /// CALIFORNIA
        /// CALIFORNIA
        /// CALIFORNIA
        /// CALIFORNIA
        /// CALIFORNIA
        /// CALIFORNIA
        /// CALIFORNIA
        /// CALIFORNIA
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCACalculate_Click(object sender, EventArgs e)
        {
            try
            {
                // If California check box is checked, generate Cash rate.
                if (californiaChkCashBtn.Checked)
                {
                    txtCAPWC.Text = "4.25";
                }
                else
                {
                    txtCAPWC.Text = "4.25";
                }

                // Set Array 1 as focus
                txtCAArray1.Focus();

                // Define Variables
                double Array1CA, Array2CA, Array3CA, Array4CA, TotalPanelCountCA, TotalkWCA, TotalSystemCostCA;

                // Recieve input from textBoxArray1-4Utah and put into variable
                Array1CA = double.Parse(txtCAArray1.Text);
                Array2CA = double.Parse(txtCAArray2.Text);
                Array3CA = double.Parse(txtCAArray3.Text);
                Array4CA = double.Parse(txtCAArray4.Text);

                // Receive per watt cost from TextBox
                PerWattCost = double.Parse(txtCAPWC.Text);

                // Calculate with "SumOfArrays = TotalPanelCountSC * 260 = TotalkWSC * PerWattCost + 2000 = TotalSystemCostSC"
                TotalPanelCountCA = Array1CA + Array2CA + Array3CA + Array4CA;
                TotalkWCA = TotalPanelCountCA * 260;
                TotalSystemCostCA = TotalkWCA * PerWattCost + INCENTIVE;

                // Show results in text box
                txtCATotalSystemCost1.Text = TotalSystemCostCA.ToString("c");

                // Text boxes show degredation of per watt cost 3.80 to per watt cost minimum of 3.45 for SC
                lblCAPWCDown1.Text = (PerWattCost - .05).ToString("n2");
                lblCAPWCDown2.Text = (double.Parse(lblCAPWCDown1.Text) - .05).ToString("n2");
                lblCAPWCDown3.Text = (double.Parse(lblCAPWCDown2.Text) - .05).ToString("n2");
                lblCAPWCDown4.Text = (double.Parse(lblCAPWCDown3.Text) - .05).ToString("n2");
                lblCAPWCDown5.Text = (double.Parse(lblCAPWCDown4.Text) - .05).ToString("n2");
                lblCAPWCDown6.Text = (double.Parse(lblCAPWCDown5.Text) - .05).ToString("n2");
                lblCAPWCDown7.Text = (double.Parse(lblCAPWCDown6.Text) - .05).ToString("n2");
                lblCAPWCDown8.Text = (double.Parse(lblCAPWCDown7.Text) - .05).ToString("n2");
                lblCAPWCDown9.Text = (double.Parse(lblCAPWCDown8.Text) - .05).ToString("n2");

                // Diplay the system cost of each degraded per watt cost to minimum of 3.45
                txtCATotalSystemCost2.Text = (TotalkWCA * double.Parse(lblCAPWCDown1.Text) + INCENTIVE).ToString("c");
                txtCATotalSystemCost3.Text = (TotalkWCA * double.Parse(lblCAPWCDown2.Text) + INCENTIVE).ToString("c");
                txtCATotalSystemCost4.Text = (TotalkWCA * double.Parse(lblCAPWCDown3.Text) + INCENTIVE).ToString("c");
                txtCATotalSystemCost5.Text = (TotalkWCA * double.Parse(lblCAPWCDown4.Text) + INCENTIVE).ToString("c");
                txtCATotalSystemCost6.Text = (TotalkWCA * double.Parse(lblCAPWCDown5.Text) + INCENTIVE).ToString("c");
                txtCATotalSystemCost7.Text = (TotalkWCA * double.Parse(lblCAPWCDown6.Text) + INCENTIVE).ToString("c");
                txtCATotalSystemCost8.Text = (TotalkWCA * double.Parse(lblCAPWCDown7.Text) + INCENTIVE).ToString("c");
                txtCATotalSystemCost9.Text = (TotalkWCA * double.Parse(lblCAPWCDown8.Text) + INCENTIVE).ToString("c");
                txtCATotalSystemCost10.Text = (TotalkWCA * double.Parse(lblCAPWCDown9.Text) + INCENTIVE).ToString("c");

                // Display kW size of each array in Text Boxes
                txtCAKWArray1.Text = (((double.Parse(txtCAArray1.Text) * 260) / 1000).ToString("n2"));
                txtCAKWArray2.Text = (((double.Parse(txtCAArray2.Text) * 260) / 1000).ToString("n2"));
                txtCAKWArray3.Text = (((double.Parse(txtCAArray3.Text) * 260) / 1000).ToString("n2"));
                txtCAKWArray4.Text = (((double.Parse(txtCAArray4.Text) * 260) / 1000).ToString("n2"));

                // Display kW total in total box
                double Array1KWCA, Array2KWCA, Array3KWCA, Array4KWCA;
                Array1KWCA = double.Parse(txtCAKWArray1.Text);
                Array2KWCA = double.Parse(txtCAKWArray2.Text);
                Array3KWCA = double.Parse(txtCAKWArray3.Text);
                Array4KWCA = double.Parse(txtCAKWArray4.Text);
                txtCAKWTotal.Text = (Array1KWCA + Array2KWCA + Array3KWCA + Array4KWCA).ToString("n2");

                // Make text box email template display an email template with variables filled in.
                txtCAEmailTemplate.Text = $"The documents are as follows:\r\n\r\nFileNameHere\r\n\r\n► {TotalPanelCountCA} Panel System\r\n► {TotalkWCA / 1000} kW System Size\r\n► ElectricalUsageOffsetHere\r\n\r\nInformationAboutTheSystemHere\r\n\r\nIf there are any questions or concerns, please reply to this email.\r\n\r\n\r\n\r\nRegards,\r\n\r\nYourNameHere";

                // Refocus on array1
                txtCAArray1.Focus();

            }
            catch
            {
                MessageBox.Show("Type only intergers");
            }
        }


        /// <summary>
        /// Clears all California Text Boxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCAClear_Click(object sender, EventArgs e)
        {
            txtCAArray1.Text = "0";
            txtCAArray2.Text = "0";
            txtCAArray3.Text = "0";
            txtCAArray4.Text = "0";
            txtCAEmailTemplate.Clear();
            txtCAKWArray1.Clear();
            txtCAKWArray2.Clear();
            txtCAKWArray3.Clear();
            txtCAKWArray4.Clear();
            txtCAKWTotal.Clear();
            txtCATotalSystemCost1.Clear();
            txtCATotalSystemCost2.Clear();
            txtCATotalSystemCost3.Clear();
            txtCATotalSystemCost4.Clear();
            txtCATotalSystemCost5.Clear();
            txtCATotalSystemCost6.Clear();
            txtCATotalSystemCost7.Clear();
            txtCATotalSystemCost8.Clear();
            txtCATotalSystemCost9.Clear();
            txtCATotalSystemCost10.Clear();
            californiaChkCashBtn.Checked = false;
            txtCAArray1.Focus();
        }

        private void txtCAEmailTemplate_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                Clipboard.Clear();    //Clear if any old value is there in Clipboard        
                Clipboard.SetText(txtCAEmailTemplate.Text); //Copy text to Clipboard
                string strClip = Clipboard.GetText(); //Get text from Clipboard
                MessageBox.Show("Text Copied.");
            }
            catch
            {

            }
        }     



        /// <summary>
        /// AUSTIN TEXAS
        /// AUSTIN TEXAS
        /// AUSTIN TEXAS
        /// AUSTIN TEXAS
        /// AUSTIN TEXAS
        /// AUSTIN TEXAS
        /// AUSTIN TEXAS
        /// AUSTIN TEXAS
        /// AUSTIN TEXAS
        /// AUSTIN TEXAS
        /// AUSTIN TEXAS
        /// AUSTIN TEXAS
        /// AUSTIN TEXAS
        /// AUSTIN TEXAS
        /// AUSTIN TEXAS
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAustinCalc_Click(object sender, EventArgs e)
        {
            try
            {
                // Cash button check box
                if (austinChkCashBtn.Checked)
                {
                    austinTxtPWC1.Text = "4.25";
                }
                else
                {
                    austinTxtPWC1.Text = "3.70";
                }

                // Set Array 1 as focus
                austinTxtArray1.Focus();

                // Define Variables
                double Array1AUTX, Array2AUTX, Array3AUTX, Array4AUTX, TotalPanelCountAUTX, TotalkWAUTX, TotalSystemCostAUTX;

                // Recieve input from textBoxArray1-4Utah and put into variable
                Array1AUTX = double.Parse(austinTxtArray1.Text);
                Array2AUTX = double.Parse(austinTxtArray2.Text);
                Array3AUTX = double.Parse(austinTxtArray3.Text);
                Array4AUTX = double.Parse(austinTxtArray4.Text);

                // Receive per watt cost from TextBox
                PerWattCost = double.Parse(austinTxtPWC1.Text);

                // Calculate with "SumOfArrays = TotalPanelCountSC * 260 = TotalkWSC * PerWattCost + 2000 = TotalSystemCostSC"
                TotalPanelCountAUTX = Array1AUTX + Array2AUTX + Array3AUTX + Array4AUTX;
                TotalkWAUTX = TotalPanelCountAUTX * 260;
                TotalSystemCostAUTX = TotalkWAUTX * PerWattCost;

                // Show results in text box
                austinTxtTotalSystemCost1.Text = TotalSystemCostAUTX.ToString("c");

                // Text boxes show degredation of per watt cost 3.80 to per watt cost minimum of 3.45 for SC
                lblAustinPWCDown1.Text = (PerWattCost - .05).ToString("n2");
                lblAustinPWCDown2.Text = (double.Parse(lblAustinPWCDown1.Text) - .05).ToString("n2");
                lblAustinPWCDown3.Text = (double.Parse(lblAustinPWCDown2.Text) - .05).ToString("n2");
                lblAustinPWCDown4.Text = (double.Parse(lblAustinPWCDown3.Text) - .05).ToString("n2");
                lblAustinPWCDown5.Text = (double.Parse(lblAustinPWCDown4.Text) - .05).ToString("n2");
                lblAustinPWCDown6.Text = (double.Parse(lblAustinPWCDown5.Text) - .05).ToString("n2");
                lblAustinPWCDown7.Text = (double.Parse(lblAustinPWCDown6.Text) - .05).ToString("n2");
                lblAustinPWCDown8.Text = (double.Parse(lblAustinPWCDown7.Text) - .05).ToString("n2");
                lblAustinPWCDown9.Text = (double.Parse(lblAustinPWCDown8.Text) - .05).ToString("n2");

                // Diplay the system cost of each degraded per watt cost to minimum of 3.45
                austinTxtTotalSystemCost2.Text = (TotalkWAUTX * double.Parse(lblAustinPWCDown1.Text) + INCENTIVE).ToString("c");
                austinTxtTotalSystemCost3.Text = (TotalkWAUTX * double.Parse(lblAustinPWCDown2.Text) + INCENTIVE).ToString("c");
                austinTxtTotalSystemCost4.Text = (TotalkWAUTX * double.Parse(lblAustinPWCDown3.Text) + INCENTIVE).ToString("c");
                austinTxtTotalSystemCost5.Text = (TotalkWAUTX * double.Parse(lblAustinPWCDown4.Text) + INCENTIVE).ToString("c");
                austinTxtTotalSystemCost6.Text = (TotalkWAUTX * double.Parse(lblAustinPWCDown5.Text) + INCENTIVE).ToString("c");
                austinTxtTotalSystemCost7.Text = (TotalkWAUTX * double.Parse(lblAustinPWCDown6.Text) + INCENTIVE).ToString("c");
                austinTxtTotalSystemCost8.Text = (TotalkWAUTX * double.Parse(lblAustinPWCDown7.Text) + INCENTIVE).ToString("c");
                austinTxtTotalSystemCost9.Text = (TotalkWAUTX * double.Parse(lblAustinPWCDown8.Text) + INCENTIVE).ToString("c");
                austinTxtTotalSystemCost10.Text = (TotalkWAUTX * double.Parse(lblAustinPWCDown9.Text) + INCENTIVE).ToString("c");


                // Display kW size of each array in Text Boxes
                austinTxtKwArray1.Text = (((double.Parse(austinTxtArray1.Text) * 260) / 1000).ToString("n2"));
                austinTxtKwArray2.Text = (((double.Parse(austinTxtArray2.Text) * 260) / 1000).ToString("n2"));
                austinTxtKwArray3.Text = (((double.Parse(austinTxtArray3.Text) * 260) / 1000).ToString("n2"));
                austinTxtKwArray4.Text = (((double.Parse(austinTxtArray4.Text) * 260) / 1000).ToString("n2"));

                // Display kW total in total box
                double Array1KWAUTX, Array2KWAUTX, Array3KWAUTX, Array4KWAUTX;
                Array1KWAUTX = double.Parse(austinTxtKwArray1.Text);
                Array2KWAUTX = double.Parse(austinTxtKwArray2.Text);
                Array3KWAUTX = double.Parse(austinTxtKwArray3.Text);
                Array4KWAUTX = double.Parse(austinTxtKwArray4.Text);
                austinTxtKwTotal.Text = (Array1KWAUTX + Array2KWAUTX + Array3KWAUTX + Array4KWAUTX).ToString("n2");

                // Make text box email template display an email template with variables filled in.
                austinTxtEmailTemplate.Text = $"The documents are as follows:\r\n\r\nFileNameHere\r\n\r\n► {TotalPanelCountAUTX} Panel System\r\n► {TotalkWAUTX / 1000} kW System Size\r\n► ElectricalUsageOffsetHere\r\n\r\nInformationAboutTheSystemHere\r\n\r\nIf there are any questions or concerns, please reply to this email.\r\n\r\n\r\n\r\nRegards,\r\n\r\nYourNameHere";

                // Refocus on textarray1
                austinTxtArray1.Focus();
            }
            catch
            {
                MessageBox.Show("Only type numbers.");
            }
        }


        /// <summary>
        /// Clears all text boxes in the Austin tab.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAustinClear_Click(object sender, EventArgs e)
        {
            austinTxtArray1.Text = ("0");
            austinTxtArray2.Text = ("0");
            austinTxtArray3.Text = ("0");
            austinTxtArray4.Text = ("0");
            austinTxtEmailTemplate.Clear();
            austinTxtKwArray1.Clear();
            austinTxtKwArray2.Clear();
            austinTxtKwArray3.Clear();
            austinTxtKwArray4.Clear();
            austinTxtKwTotal.Clear();
            austinTxtTotalSystemCost1.Clear();
            austinTxtTotalSystemCost2.Clear();
            austinTxtTotalSystemCost3.Clear();
            austinTxtTotalSystemCost4.Clear();
            austinTxtTotalSystemCost5.Clear();
            austinTxtTotalSystemCost6.Clear();
            austinTxtTotalSystemCost7.Clear();
            austinTxtTotalSystemCost8.Clear();
            austinTxtTotalSystemCost9.Clear();
            austinTxtTotalSystemCost10.Clear();
            austinTxtArray1.Focus();
            austinChkCashBtn.Checked = false;
            austinTxtArray1.Focus();
        }


        /// <summary>
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// San Antonio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSanAntonioCalc_Click(object sender, EventArgs e)
        {
            try
            {
                // Cash numbers check box for San Antonio
                if (sanAntonioChkCashBtn.Checked)
                {
                    sanAntonioTxtPWC.Text = "4.25";
                }
                else
                {
                    sanAntonioTxtPWC.Text = "3.70";
                }

                // Set Array 1 as focus
                austinTxtArray1.Focus();

                // Define Variables
                double Array1SATX, Array2SATX, Array3SATX, Array4SATX, TotalPanelCountSATX, TotalkWSATX, TotalSystemCostSATX;

                // Recieve input from textBoxArray1-4Utah and put into variable
                Array1SATX = double.Parse(sanAntonioTxtArray1.Text);
                Array2SATX = double.Parse(sanAntonioTxtArray2.Text);
                Array3SATX = double.Parse(sanAntonioTxtArray3.Text);
                Array4SATX = double.Parse(sanAntonioTxtArray4.Text);

                // Receive per watt cost from TextBox
                PerWattCost = double.Parse(sanAntonioTxtPWC.Text);

                // Calculate with "SumOfArrays = TotalPanelCountSC * 260 = TotalkWSC * PerWattCost + 2000 = TotalSystemCostSC"
                TotalPanelCountSATX = Array1SATX + Array2SATX + Array3SATX + Array4SATX;
                TotalkWSATX = TotalPanelCountSATX * 260;
                TotalSystemCostSATX = TotalkWSATX * PerWattCost + INCENTIVE;

                // Show results in text box
                sanAntonioTxtTotalSystemCost1.Text = TotalSystemCostSATX.ToString("c");

                // Labels show degredation of per watt cost 3.80 to per watt cost minimum of 3.45 for SC
                sanAntonioLblPwcDown1.Text = (PerWattCost - .05).ToString("n2");
                sanAntonioLblPwcDown2.Text = (double.Parse(sanAntonioLblPwcDown1.Text) - .05).ToString("n2");
                sanAntonioLblPwcDown3.Text = (double.Parse(sanAntonioLblPwcDown2.Text) - .05).ToString("n2");
                sanAntonioLblPwcDown4.Text = (double.Parse(sanAntonioLblPwcDown3.Text) - .05).ToString("n2");
                sanAntonioLblPwcDown5.Text = (double.Parse(sanAntonioLblPwcDown4.Text) - .05).ToString("n2");
                sanAntonioLblPwcDown6.Text = (double.Parse(sanAntonioLblPwcDown5.Text) - .05).ToString("n2");
                sanAntonioLblPwcDown7.Text = (double.Parse(sanAntonioLblPwcDown6.Text) - .05).ToString("n2");
                sanAntonioLblPwcDown8.Text = (double.Parse(sanAntonioLblPwcDown7.Text) - .05).ToString("n2");
                sanAntonioLblPwcDown9.Text = (double.Parse(sanAntonioLblPwcDown8.Text) - .05).ToString("n2");

                // Diplay the system cost of each degraded per watt cost to minimum of 3.45
                sanAntonioTxtTotalSystemCost2.Text = (TotalkWSATX * double.Parse(sanAntonioLblPwcDown1.Text) + INCENTIVE).ToString("c");
                sanAntonioTxtTotalSystemCost3.Text = (TotalkWSATX * double.Parse(sanAntonioLblPwcDown2.Text) + INCENTIVE).ToString("c");
                sanAntonioTxtTotalSystemCost4.Text = (TotalkWSATX * double.Parse(sanAntonioLblPwcDown3.Text) + INCENTIVE).ToString("c");
                sanAntonioTxtTotalSystemCost5.Text = (TotalkWSATX * double.Parse(sanAntonioLblPwcDown4.Text) + INCENTIVE).ToString("c");
                sanAntonioTxtTotalSystemCost6.Text = (TotalkWSATX * double.Parse(sanAntonioLblPwcDown5.Text) + INCENTIVE).ToString("c");
                sanAntonioTxtTotalSystemCost7.Text = (TotalkWSATX * double.Parse(sanAntonioLblPwcDown6.Text) + INCENTIVE).ToString("c");
                sanAntonioTxtTotalSystemCost8.Text = (TotalkWSATX * double.Parse(sanAntonioLblPwcDown7.Text) + INCENTIVE).ToString("c");
                sanAntonioTxtTotalSystemCost9.Text = (TotalkWSATX * double.Parse(sanAntonioLblPwcDown8.Text) + INCENTIVE).ToString("c");
                sanAntonioTxtTotalSystemCost10.Text = (TotalkWSATX * double.Parse(sanAntonioLblPwcDown9.Text) + INCENTIVE).ToString("c");

                // Display kW size of each array in Text Boxes
                sanAntonioTxtKWArray1.Text = (((double.Parse(sanAntonioTxtArray1.Text) * 260) / 1000).ToString("n2"));
                sanAntonioTxtKWArray2.Text = (((double.Parse(sanAntonioTxtArray2.Text) * 260) / 1000).ToString("n2"));
                sanAntonioTxtKWArray3.Text = (((double.Parse(sanAntonioTxtArray3.Text) * 260) / 1000).ToString("n2"));
                sanAntonioTxtKWArray4.Text = (((double.Parse(sanAntonioTxtArray4.Text) * 260) / 1000).ToString("n2"));

                // Display kW total in total box
                double Array1KWSATX, Array2KWSATX, Array3KWSATX, Array4KWSATX;
                Array1KWSATX = double.Parse(sanAntonioTxtArray1.Text);
                Array2KWSATX = double.Parse(sanAntonioTxtArray2.Text);
                Array3KWSATX = double.Parse(sanAntonioTxtArray3.Text);
                Array4KWSATX = double.Parse(sanAntonioTxtArray4.Text);
                sanAntonioTxtKWTotal.Text = (TotalkWSATX / 1000).ToString("n2");

                // Make text box email template display an email template with variables filled in.
                sanAntonioTxtEmailInformation.Text = $"The documents are as follows:\r\n\r\nFileNameHere\r\n\r\n► {TotalPanelCountSATX} Panel System\r\n► {TotalkWSATX / 1000} kW System Size\r\n► ElectricalUsageOffsetHere\r\n\r\nInformationAboutTheSystemHere\r\n\r\nIf there are any questions or concerns, please reply to this email.\r\n\r\n\r\n\r\nRegards,\r\n\r\nYourNameHere";

                // Refocus on textarray1
                sanAntonioTxtArray1.Focus();
            }
            catch
            {
                MessageBox.Show("Only type numbers.");
            }
        }


        /// <summary>
        /// Clears the san antonio tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSanAntonioClear_Click(object sender, EventArgs e)
        {
            sanAntonioTxtArray1.Text = ("0");
            sanAntonioTxtArray2.Text = ("0");
            sanAntonioTxtArray3.Text = ("0");
            sanAntonioTxtArray4.Text = ("0");
            sanAntonioTxtEmailInformation.Clear();
            sanAntonioTxtKWArray1.Clear();
            sanAntonioTxtKWArray2.Clear();
            sanAntonioTxtKWArray3.Clear();
            sanAntonioTxtKWArray4.Clear();
            sanAntonioTxtKWTotal.Clear();
            sanAntonioTxtTotalSystemCost1.Clear();
            sanAntonioTxtTotalSystemCost2.Clear();
            sanAntonioTxtTotalSystemCost3.Clear();
            sanAntonioTxtTotalSystemCost4.Clear();
            sanAntonioTxtTotalSystemCost5.Clear();
            sanAntonioTxtTotalSystemCost6.Clear();
            sanAntonioTxtTotalSystemCost7.Clear();
            sanAntonioTxtTotalSystemCost8.Clear();
            sanAntonioTxtTotalSystemCost9.Clear();
            sanAntonioTxtTotalSystemCost10.Clear();
            sanAntonioChkCashBtn.Checked = false;
            sanAntonioTxtArray1.Focus();
        }


        /// <summary>
        /// Copy and paste from austin email box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void austinTxtEmailTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.Clear();    //Clear if any old value is there in Clipboard        
                Clipboard.SetText(austinTxtEmailTemplate.Text); //Copy text to Clipboard
                string strClip = Clipboard.GetText(); //Get text from Clipboard
                MessageBox.Show("Text Copied.");
            }
            catch
            {

            }
        }


        /// <summary>
        /// copy and paste from san antonio box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sanAntonioTxtEmailInformation_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.Clear();    //Clear if any old value is there in Clipboard        
                Clipboard.SetText(sanAntonioTxtEmailInformation.Text); //Copy text to Clipboard
                string strClip = Clipboard.GetText(); //Get text from Clipboard
                MessageBox.Show("Text Copied.");
            }
            catch
            {

            }
        }


        /// <summary>
        /// For when you change tabs you'll auto focus to the first array.
        /// For when you change tabs you'll auto focus to the first array.
        /// For when you change tabs you'll auto focus to the first array.
        /// For when you change tabs you'll auto focus to the first array.
        /// For when you change tabs you'll auto focus to the first array.
        /// For when you change tabs you'll auto focus to the first array.
        /// For when you change tabs you'll auto focus to the first array.
        /// For when you change tabs you'll auto focus to the first array.
        /// For when you change tabs you'll auto focus to the first array.
        /// For when you change tabs you'll auto focus to the first array.
        /// For when you change tabs you'll auto focus to the first array.
        /// For when you change tabs you'll auto focus to the first array.
        /// For when you change tabs you'll auto focus to the first array.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabControl1_Click(object sender, EventArgs e)
        {
            textBoxArray1Utah.Focus();
            textBoxArraySC1.Focus();
            txtCAArray1.Focus();
            austinTxtArray1.Focus();
            sanAntonioTxtArray1.Focus();
        }


        // For when the user checks the version and release information.
        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This is version 5.0.0.0 released 10/12/2015");
        }
    }
}
